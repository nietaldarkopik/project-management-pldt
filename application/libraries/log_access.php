<?php

class Log_access{
	var $CI;
	
	function Log_access(){
		$this->CI =& get_instance();
		$this->init();
	}
	
	function init(){
		$this->CI->load->library('user_agent');
		$this->CI->load->library("user_access");
		
		$this->insert();
	}
	
	function current_access()
	{
		$data = 	array(	"user_id" => $this->CI->user_access->current_user_id,
							"datetime" => date("Y-m-d h:i:s"),
							"url" => str_replace('/index.php','',current_url()),
							"access" => $this->CI->router->class . '/' . $this->CI->router->method,
							"request" => (isset($_REQUEST))?json_encode($_REQUEST):json_encode(array()),
							"ip_address" => $this->CI->input->ip_address(),
							"browser" => $this->CI->agent->browser(),
							"user_agent" => $this->CI->agent->agent_string()
						);
		return $data;
	}
	
	function insert()
	{
		$skip_log = array(	'dashboard/right_menu',
							'dashboard/top_menu',
							'menu/index',
							'dashboard/is_login',
							'dashboard/rightbar',
							'dashboard/login'
							);
		$data = $this->current_access();
		if(!in_array($data['access'],$skip_log))
			$this->CI->db->insert(TABLE_SYS_LOG,$data);
	}
}
