<?php

class Gts_members{
  var $CI;

  function Gts_members()
  {
		$this->CI =& get_instance();
  }
  
  function get_members()
  {
    $q_member = $this->CI->db->get("addon_member");
    $f_member = $q_member->result_array();
    return $f_member;
  }
  
  function get_member_detail($user_id = "")
  {
    if(empty($user_id))
      return array();
      
    $this->CI->db->where(array('user_id' => $user_id));
    $q_member = $this->CI->db->get("addon_member");
    $f_member = $q_member->row_array();
    return $f_member;
  }
  
  function get_member_by_code($member_code = "")
  {
    if(empty($member_code))
      return array();
      
    $this->CI->db->where(array('plain_code' => $member_code));
    $q_member = $this->CI->db->get("addon_member");
    $f_member = $q_member->row_array();
    return $f_member;
  }
  
  function get_current_member()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    return $member;
  }
  
  function get_member_code($user_id = "")
  {
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['plain_code']))?$member['plain_code']:"";
    return $member_code;
  }
  
  function get_current_member_code()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['plain_code']))?$member['plain_code']:"";
    return $member_code;
  }
  
  function get_current_member_id()
  {
    $user_id = $this->CI->user_access->current_user_id;
    $member = $this->get_member_detail($user_id);
    $member_code = (isset($member['member_id']))?$member['member_id']:"";
    return $member_code;
  }
  
  function str_user_code($user_id = "")
  {
    if($user_id == '')
      return false;
      
    $user_id = 'GTS'.str_pad($user_id, 7, '0', STR_PAD_LEFT);;
    return $user_id;
  }
}
