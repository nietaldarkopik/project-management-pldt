<?php

class Input_checkbox{
  var $CI;
  var $form_field = "";
  function Input_checkbox()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "text";
    
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
	$options = (isset($field['options']))?$field['options']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= ' <div class="row">
                  <div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
                  
    $query = (isset($field['query']))?$field['query']:"";
    
    if(!empty($query))
    {
      $q = $this->CI->db->query($query);
      if($q->num_rows() > 0)
      {
        $t_options = $q->result_array();
        foreach($t_options as $index => $row)
        {
          if(isset($row['value']))
          {
            $t_label = (isset($row['label']))?$row['label']:$row['value'];
            $options[$row['value']] = $t_label;
          }
        }
      }
    }elseif(isset($field['table']) and !empty($field['table']))
    {
      $this->CI->db->order_by("value","ASC");
      $this->CI->db->select($field['select']);
      $q = $this->CI->db->get($field['table']);
      
      if($q->num_rows() > 0)
      {
        $t_options = $q->result_array();
        foreach($t_options as $index => $row)
        {
          if(isset($row['value']))
          {
            $t_label = (isset($row['label']))?$row['label']:$row['value'];
            $options[$row['value']] = $t_label;
          }
        }
      }
    
    }

    $form = '<br/>';
    
	$json_value = (!empty($value))?json_decode($value,true):array();
    if(is_array($options) and count($options) > 0)
    {
      foreach($options as $i => $o)
      {
        $checked = (in_array($o,$json_value))?TRUE:FALSE;
        $input =  form_checkbox($input_name.'[]', $o, $checked);
        $o = $this->CI->hook->do_action("form_fields_input_checkbox_".$i,$o);
        $form .= '
              <div class="input-group">
                <span class="input-group-addon">
                '.$input.'
                </span>
                <span class="form-control">'.$o.'</span>
              </div>';
	  }
	  $form .= '<br/>';
    }

    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      '.$form.'
                      '.$error.'
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';      
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }

}
