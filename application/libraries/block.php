<?php

class Block{
	var $CI;
	var $blocks = array();
	
	function __construct(){
		$this->CI =& get_instance();
	}
  
  function call_block($blockname = "")
  {
    $this->CI->db->where(array("block_name" => $blockname));
    $q_block = $this->CI->db->get(TABLE_SYS_BLOCKS);
    if($q_block->num_rows() == 0)
      return false;
      
    $block = $q_block->row_array();
    
    $q_block_plugin = $this->CI->db->query("SELECT * FROM ".TABLE_SYS_BLOCK_PLUGINS." bp,".TABLE_SYS_PLUGINS." p WHERE bp.block_id = '".$block['block_id']."' AND bp.plugin_id = p.plugin_id AND p.status = 'active'");
    
    $output = "";
    if($q_block_plugin->num_rows() > 0)
    {
      $block_plugins = $q_block_plugin->result_array();
      foreach($block_plugins as $i => $block_plugin)
      {
        if(isset($block_plugin['plugin_name']))
        {
          $the_plugin = $this->CI->plugins->call_plugins($block_plugin['plugin_name']);
          if(method_exists($the_plugin,'hook_'.$blockname))
          {
            $output .= $the_plugin->{'hook_'.$blockname}();
          }
        }
      }
    }
    
    return $output;
  }

  function get_plugins_list($blockname = "")
  {
    $this->CI->db->where(array("block_name" => $blockname));
    $q_block = $this->CI->db->get(TABLE_SYS_BLOCKS);
    if($q_block->num_rows() == 0)
      return array();
      
    $block = $q_block->row_array();
    
    $q_block_plugin = $this->CI->db->query("SELECT * FROM ".TABLE_SYS_BLOCK_PLUGINS." bp,".TABLE_SYS_PLUGINS." p WHERE bp.block_id = '".$block['block_id']."' AND bp.plugin_id = p.plugin_id AND p.status = 'active'");
    
    $output = "";
    if($q_block_plugin->num_rows() > 0)
    {
      return $block_plugins = $q_block_plugin->result_array();
    }
    
    return array();
  }

}
