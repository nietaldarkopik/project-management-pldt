<?php

class Filter_form_container{
  var $CI;
  function Filter_form_container()
  {
		$this->CI =& get_instance();
  }
  
  function output()
  {
    $action = '';
    $form_title = '';
    $nonce = '';
    $ajax_target = '';
    $button_action = '';
    $output = '';
    $output .= '
			<form id="form" action="'.$action.'" method="post" class="form" enctype="multipart/form-data">
				<fieldset id="edit_form_fieldset">
				<legend>'.$form_title.'</legend>
				<br class="fclear"/>
				<label>&nbsp;</label>
				<input type="hidden" name="nonce" value="'.$nonce.'"/> 
				<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
				<input type="hidden" name="is_ajax" value="1"/> 
				<span class="value_view">
					&nbsp;
					&nbsp;
					'.$button_action.'
				</span>
				<br class="fclear"/>
				<br class="fclear"/>
				Data bertanda bintang (<span style="color:red;">*</span>) wajib diisi
				<br class="fclear"/>
				</fieldset>
			</form>';
    return $output;
  }
}
