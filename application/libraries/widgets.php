<?php
class Widgets{
	var $CI;
	var $hooks = array();
	var $plugins = array();
  
	function Widgets(){
		$this->CI =& get_instance();
	}
	
  function install($plugin_name = "",$widget_data = array())
  {
    $where = array();
    $where['plugin_name'] = $plugin_name;
    $where['widget_name']  = $widget_data['widget_name'];
    
    $data = array();
    $data['plugin_name'] = $plugin_name;
    $data['widget_name']  = $widget_data['widget_name'];
    $data['widget_title'] = $widget_data['widget_title'];
    $data['description']  = $widget_data['description'];
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
    $data['author']  = $widget_data['author'];
    $data['version']  = $widget_data['version'];
    $this->CI->db->where($where);
    $q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);
    
    if($q_check_widget->num_rows() == 0)
    {
      $q = $this->CI->db->insert(TABLE_SYS_WIDGETS,$data);
      if($q)
      {
        return 1;
      }else{
        return 0;
      }
    }else{
      return -1;
    }
  }
  
  function update($plugin_name = "",$widget_data = array())
  {
    $where = array();
    $where['plugin_name'] = $plugin_name;
    $where['widget_name']  = $widget_data['widget_name'];
    
    $data = array();
    $data['plugin_name'] = $plugin_name;
    $data['widget_name']  = $widget_data['widget_name'];
    $data['widget_title'] = $widget_data['widget_title'];
    $data['description']  = $widget_data['description'];
    $data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
    $data['author']  = $widget_data['author'];
    $data['version']  = $widget_data['version'];
    $this->CI->db->where($where);
    $q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);
    
    if($q_check_widget->num_rows() == 0)
    {
      $q = $this->install($plugin_name,$widget_data);
      if($q)
      {
        return 1;
      }else{
        return 0;
      }
    }else{
      $this->CI->db->where($where);
      $q = $this->CI->db->update(TABLE_SYS_WIDGETS,$data);
      if($q)
      {
        return 1;
      }else{
        return -1;
      }
    }
  }
}
