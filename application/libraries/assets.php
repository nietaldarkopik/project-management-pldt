<?php

class assets {

  var $CI;
  var $js = array('head' => array(),'body' => array());
  var $css = array('head' => array(),'body' => array());
  var $img,$upload,$js_inline,$css_inline;
  
  function assets(){
    $this->CI =& get_instance();
  }
  
  function reset()
  {
    $this->js = array('head' => array(),'body' => array());
    $this->css = array('head' => array(),'body' => array());
    $this->img = $this->upload = $this->js_inline = $this->css_inline = "";
  }
  
  function add_js($js = "",$position = "head")
  {
    if(is_array($js) and count($js) > 0)
    {      
      if(isset($this->js[$position]) and is_array($this->js[$position]) and count($this->js[$position]) > 0)
      {
        foreach($this->js[$position] as $i => $j)
        {
          foreach($js as $i2 => $j2)
          {
            if($j == $js2)
              unset($js[$i2]);
          }
        }
      }
      foreach($js as $i => $j)
      {
        $this->js[$position][] = $j;
      }
    }else{
      if(isset($this->js[$position]) and is_array($this->js[$position]) and count($this->js[$position]) > 0)
      {
        foreach($this->js[$position] as $i => $j)
        {
          if($j == $js)
            return false;
        }
      }
      $this->js[$position][] = $js;
    }
    
    return true;
  }

  function add_js_inline($js = "",$position = "head")
  {
    $this->js_inline[$position][] = $js;
  }

  function print_js_inline($position = "head")
  {
    $output = "";
    if(isset($this->js_inline[$position]) and is_array($this->js_inline[$position]) and count($this->js_inline[$position]) > 0)
    {
      foreach($this->js_inline[$position] as $js)
      {
        $output .= $js."\n";
      }
    }
    return $output;
  }
  
  function print_js($position = "head")
  {
    $output = "";
    if(isset($this->js[$position]) and is_array($this->js[$position]) and count($this->js[$position]) > 0)
    {
      foreach($this->js[$position] as $js)
      {
        if(strpos($js,'<script') !== false)
        {
          $output .= $js."\n";
        }else{
          $output .= '<script type="text/javascript" src="'.$js.'"></script>'."\n";
        }
      }
    }
    return $output;
  }

  function add_css($css = "",$position = "head")
  {
    if(is_array($css) and count($css) > 0)
    {      
      if(isset($this->css[$position]) and is_array($this->css[$position]) and count($this->css[$position]) > 0)
      {
        foreach($this->css[$position] as $i => $j)
        {
          foreach($css as $i2 => $j2)
          {
            if($j == $css2)
              unset($css[$i2]);
          }
        }
      }
      foreach($css as $i => $j)
      {
        $this->css[$position][] = $j;
      }
    }else{
      if(isset($this->css[$position]) and is_array($this->css[$position]) and count($this->css[$position]) > 0)
      {
        foreach($this->css[$position] as $i => $j)
        {
          if($j == $css)
            return false;
        }
      }
      $this->css[$position][] = $css;
    }
    
    return true;
  }

  function add_css_inline($css = "",$position = "head")
  {
    $this->css_inline[$position][] = $css;
  }

  function print_css($position = "head")
  {
    $output = "";
    if(isset($this->css[$position]) and is_array($this->css[$position]) and count($this->css[$position]) > 0)
    {
      foreach($this->css[$position] as $css)
      {
        if(strpos($css,'<link') !== false)
        {
          $output .= $css."\n";
        }else{
          $output .= '<link rel="stylesheet" type="text/css" href="'.$css.'"/>'."\n";
        }
      }
    }
    return $output;
  }

  function print_css_inline($position = "head")
  {
    $output = "";
    if(isset($this->css_inline[$position]) and is_array($this->css_inline[$position]) and count($this->css_inline[$position]) > 0)
    {
      foreach($this->css_inline[$position] as $css)
      {
        $output .= $css."\n";
      }
    }
    return $output;
  }

}
