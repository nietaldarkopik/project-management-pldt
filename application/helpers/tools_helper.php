<?php
/**
 *
 * Safely encrypts data for POST transport
 * URL issues
 *  transforms + to spaces
 *  / are param value separators
 *  = are param value separators
 *
 *  we process the string on reverse
 * @param string $string
 */
function my_urlsafe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_','.'),$data);
    return $data;
}

/**
 *
 * The encoding string had to be specially encoded to be transported
 * over the HTTP
 *
 * The reverse function has to be executed on the client
 *
 * @param string $string
 */
function my_urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

function is_date( $str ){
    $stamp = strtotime( $str );
    if (!is_numeric($stamp) || !preg_match("^\d{1,2}[.-/]\d{2}[.-/]\d{4}^", $str))
        return FALSE;
    $month = date( 'm', $stamp );
    $day   = date( 'd', $stamp );
    $year  = date( 'Y', $stamp );
    if (checkdate($month, $day, $year))
        return TRUE;
    return FALSE;
}


function format_currency($nominal = 0,$currency = "")
{
  $nominal = number_format($nominal,0,",",".");
  return $currency.$nominal;
}

function hierarchycal_data($data = array(),$parent_key = '',$primary_key = '',$parent = 0)
{
    $output = array();
    if(is_array($data) and count($data) > 0)
    {
        foreach($data as $i => $v)
        {
            if(isset($v[$parent_key]) and $v[$parent_key] == $parent)
            {
                $output[$v[$parent_key]] = $v;
                $childs = hierarchycal_data($data,$parent_key,$primary_key,$v[$primary_key]);
                $output[$v[$parent_key]]['childs'] = $childs;
            }
        }
    }
    return $output;    
}

function hierarchycal_option($data = array(),$parent_key = '',$primary_key = '',$parent = 0,$label_key = '',$level = 0,$spaces = '--')
{
    $output = "";
    if(is_array($data) and count($data) > 0)
    {
        foreach($data as $i => $v)
        {
            if(isset($v[$parent_key]) and $v[$parent_key] == $parent)
            {
                $output .= '<option value="'.$v[$primary_key].'">' . str_repeat($spaces,$level) . $v[$label_key] . '</option>';
                $childs = hierarchycal_option($data,$parent_key,$primary_key,$v[$primary_key],$label_key,$level + 1,$spaces);
                $output .= $childs;
            }
        }
    }
    return $output;    
}

function hierarchycal_array($data = array(),$parent_key = '',$primary_key = '',$parent = 0,$label_key = '',$level = 0,$spaces = '--')
{
    $output = array();
    if(is_array($data) and count($data) > 0)
    {
        foreach($data as $i => $v)
        {
            if(isset($v[$parent_key]) and $v[$parent_key] == $parent)
            {
                $output[$v[$primary_key]] = str_repeat($spaces,$level) . $v[$label_key];
                $childs = hierarchycal_array($data,$parent_key,$primary_key,$v[$primary_key],$label_key,$level + 1,$spaces);
                if(is_array($childs) and count($childs) > 0)
                    $output += $childs;
            }
        }
    }
    return $output;    
}