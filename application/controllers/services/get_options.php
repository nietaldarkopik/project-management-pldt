<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_options extends Admin_Controller {
  
  function index()
  {
    $params = $this->input->post("params");
    $table = $this->input->post("table");
    $select = $this->input->post("select");
    $where = $this->input->post("where");
    $fk = $this->input->post("fk");
    $primary_key = $this->input->post("primary_key");
    $foreign_key = $this->input->post("foreign_key");
    
    if(!empty($params))
    {
      $params = my_urlsafe_b64decode($params);
      parse_str($params,$arr_params);
      
      if(is_array($arr_params))
        extract($arr_params);
    }
    
    $relation = array($foreign_key => $fk);
    
    $q = "";
    if(!empty($select))
      $this->db->select($select,false);
    if(!empty($where))
      $this->db->where($where);
    if(implode("",$relation) != "")
      $this->db->where($relation);
    if(!empty($table))
      $q = $this->db->get($table);
    
    #echo $this->db->last_query();
    
    $output = "";
    if(!empty($q))
    {
      if($q->num_rows() > 0)
      {
        $data = $q->result_array();
        if(is_array($data) and count($data) > 0)
        {
          foreach($data as $index => $value)
          {
            if(isset($value['value']) and isset($value['label']))
            {
              $value['value'] = isset($value[$primary_key])?$value[$primary_key]:$value['value'];
              $output .= '<option value="'.$value['value'].'">'.$value['label'].'</option>';
            }
          }
        }
      }
    }
    echo $output;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
