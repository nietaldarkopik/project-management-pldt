<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download_pdf extends CI_Controller {
  
  function __construct()
  {
      parent::__construct();
      $this->load->library("pdfwriter");
  }
  
  function index()
  {
	}
  
  function download_url($url_base64 = "")
  {
    #echo my_urlsafe_b64encode("admin/mk_beban_anggaran/index");
    $url = my_urlsafe_b64decode($url_base64);
    if(strpos($url,"http") !== false)
    {}else{
        $url = base_url().my_urlsafe_b64decode($url_base64);
    }
    
    $postdata = http_build_query(
                                                array(
                                                    'vo' => 'view'
                                                )
                                            );
                                            
    $opts = array(  'http'=>
                            array( 'method'=>"POST",
                                      'header'  => 'Content-type: application/x-www-form-urlencoded',
                                      'content' => $postdata
                            )
                        );

    $context = stream_context_create($opts);
    $html = file_get_contents($url, false, $context);
    $this->pdfwriter->set_html($html);
    #$this->pdfwriter->download();
    $this->pdfwriter->mpdf();
  }
  
  
  function download($url_base64 = "")
  {
    #echo my_urlsafe_b64encode("admin/mk_beban_anggaran/report");
    $url = my_urlsafe_b64decode($url_base64);
    if(strpos($url,"http") !== false)
    {
        $url = str_replace(base_url(),"",$url);
    }
    #exit;
    $uri_segment = explode("/",$url);
    $class_name = ucfirst($uri_segment[1]);
    require_once(APPPATH."controllers/".$uri_segment[0].'/'.$uri_segment[1].'.php');
    #$html = file_get_contents($url);
    $newController = new $class_name();
    ob_start();
    $newController->$uri_segment[2]($uri_segment[3]);
    $html = ob_get_contents();
    $this->pdfwriter->set_html($html);
    $this->pdfwriter->download();
  }
  
  function html($html = "",$save = false,$paper = "A2",$orientation = "landscape")
  {
    $this->pdfwriter->set_html($html);
    if($save === false)
    {
			#$this->pdfwriter->download();
			$this->pdfwriter->mpdf();
		}else{
			$this->pdfwriter->dompdf($save,$paper,$orientation,false);
		}
		exit;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
