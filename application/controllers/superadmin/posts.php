<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends Admin_Controller {

	var $init = array();
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Berita'));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'berita/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Edit Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Detail Pages'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$is_ajax = $this->input->post('is_ajax');
		
		if(empty($is_ajax))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/topbar');
		}
	
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Pages'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{			
		$init = array(	'table' => 'addon_sample',
						'fields' => array(	
                      array(
													'name' => 'input_button',
													'label' => 'input_button',
													'id' => 'input_button',
													'value' => '',
													'type' => 'input_button',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_checkbox',
													'label' => 'input_checkbox',
													'id' => 'input_checkbox',
													'value' => '',
													'type' => 'input_checkbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_menus',
													'select' => array('user_menu_id AS value','controller AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_menu_id',
													'rules' => 'required'
												),
											array(
													'name' => 'input_color',
													'label' => 'input_color',
													'id' => 'input_color',
													'value' => '',
													'type' => 'input_color',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_date',
													'label' => 'input_date',
													'id' => 'input_date',
													'value' => '',
													'type' => 'input_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_datetime',
													'label' => 'input_datetime',
													'id' => 'input_datetime',
													'value' => '',
													'type' => 'input_datetime',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_default',
													'label' => 'input_default',
													'id' => 'input_default',
													'value' => '',
													'type' => 'input_default',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_file',
													'label' => 'input_file',
													'id' => 'input_file',
													'value' => '',
													'type' => 'input_file',
													'use_search' => true,
													'use_listing' => true,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/layouts/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => 'required'
												),
											array(
													'name' => 'input_fixed_multiple',
													'label' => 'input_fixed_multiple',
													'id' => 'input_fixed_multiple',
													'value' => '',
													'type' => 'input_fixed_multiple',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_fixed_multiple_text',
													'label' => 'input_fixed_multiple_text',
													'id' => 'input_fixed_multiple_text',
													'value' => '',
													'type' => 'input_fixed_multiple_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_mce',
													'label' => 'input_mce',
													'id' => 'input_mce',
													'value' => '',
													'type' => 'input_mce',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_multiple',
													'label' => 'input_multiple',
													'id' => 'input_multiple',
													'value' => '',
													'type' => 'input_multiple',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_multiple_file',
													'label' => 'input_multiple_file',
													'id' => 'input_multiple_file',
													'value' => '',
													'type' => 'input_multiple_file',
													'use_search' => true,
													'use_listing' => true,
													'config_upload' => array('upload_path' => getcwd().'/uploads/media/layouts/','encrypt_name' => true,'allowed_types' =>  'gif|jpg|png'),
													'rules' => 'required'
												),
											array(
													'name' => 'input_password',
													'label' => 'input_password',
													'id' => 'input_password',
													'value' => '',
													'type' => 'input_password',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_radio',
													'label' => 'input_radio',
													'id' => 'input_radio',
													'value' => '',
													'type' => 'input_radio',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_menus',
													'select' => array('user_menu_id AS value','controller AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_menu_id',
													'rules' => 'required'
												),
											array(
													'name' => 'input_range_date',
													'label' => 'input_range_date',
													'id' => 'input_range_date',
													'value' => '',
													'type' => 'input_range_date',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_range_year',
													'label' => 'input_range_year',
													'id' => 'input_range_year',
													'value' => '',
													'type' => 'input_range_year',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_selectbox',
													'label' => 'input_selectbox',
													'id' => 'input_selectbox',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_menus',
													'select' => array('user_menu_id AS value','controller AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_menu_id',
													'rules' => 'required'
												),
											array(
													'name' => 'input_text',
													'label' => 'input_text',
													'id' => 'input_text',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'input_textarea',
													'label' => 'input_textarea',
													'id' => 'input_textarea',
													'value' => '',
													'type' => 'input_textarea',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'id'
					);
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
