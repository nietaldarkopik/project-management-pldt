<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inbox extends Admin_Controller {

  function index()
  {
    //get list of user
    $this->db->select('user_id, username, email, user_level_id');
    $this->db->order_by('user_level_id', 'ASC');
    $this->db->order_by('username', 'ASC');
    $this->db->where('active_status', 'active');
    $this->db->where('user_id <>', $this->session->userdata('user_id'));
    $user_data = $this->db->get('user_accounts')->result_array();
    $is_login = $this->user_access->is_login();

    $curr_page = $this->uri->segment(4);
    if(empty($curr_page)) $curr_page = 1;
    $message_data = $this->lib_inbox->list_message($curr_page);

    $attach_data = array(
                    'response' => '',
                    'page_title' => 'Inbox',
                    'user_data' => $user_data,
                    'current_url' => base_url(uri_string()),
                    'send_email_url' => site_url('admin/inbox/send_mail'),
                    'message_data' => $message_data,
                   );
    if($is_login)
      $this->load->view('layouts/inbox/listing', $attach_data);
    else
      $this->load->view('layouts/login');
      
  }

  function send_mail()
  {
    $redirect_url = $this->input->post('current_url');
    $input = $this->input->post('input');

    $send_mail = $this->lib_inbox->send_mail($input['penerima'], $input['judul_pesan'], $input['pesan']);
    if($send_mail['success'])
    {
      $this->session->set_flashdata('success_inbox', $send_mail['message']);
    }else{
      $this->session->set_flashdata('error_inbox', $send_mail['message']);
    }

    redirect($redirect_url);
    die;
  }

  function read_message()
  {
    $inboxdet_id = $this->uri->segment(4);
    
    $may_read_message = $this->lib_inbox->may_read_message($inboxdet_id);
    if($may_read_message)
    {
      //set message as read
      $this->lib_inbox->read_message($inboxdet_id);
      $the_message = $this->lib_inbox->get_detail_message($inboxdet_id);

      $attach_data = array(
                    'response' => '',
                    'current_url' => base_url(uri_string()),
                    'the_message' => $the_message,
                   );
      
      if($this->user_access->is_login())
        $this->load->view('layouts/inbox/read_message', $attach_data);
      else
        $this->load->view('layouts/login');
    

    }else{
      //show forbidden page
      if($this->user_access->is_login())
        $this->load->view('layouts/inbox/read_message_forbidden');
      else
        $this->load->view('layouts/login');
    
    }
  
  }

  function attachment_downloader()
  {
    $attachment_id = $this->uri->segment(4);
    
    $may_download_attachment = $this->lib_inbox->may_download_attachment($attachment_id);
    if($may_download_attachment)
    {
      $this->lib_inbox->download_attachment($attachment_id);
      die;
    }else{
      show_404('Maaf, anda tidak memiliki izin untuk membuka halaman ini.');
    }
  }
  
}

