<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_jadwal_proyek extends Admin_Controller {

	var $init = array();
	var $config_import = array();
	var $init_timeline = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->_config_timeline();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $mk_jadwal_proyek_filter = $this->session->userdata("mk_jadwal_proyek_filter");
    $master_kontrak_id = (isset($mk_jadwal_proyek_filter['master_kontrak_id']))?$mk_jadwal_proyek_filter['master_kontrak_id']:"";
    $vendor_id = (isset($mk_jadwal_proyek_filter['vendor_id']))?$mk_jadwal_proyek_filter['vendor_id']:"";
    $data_jadwal = $this->_get_jadwal($master_kontrak_id,$vendor_id);
    
    $data_jadwal_json = json_encode($data_jadwal);

    $js = '<script src="'.current_admin_theme_url().'static/js/ganttchart/codebase/dhtmlxgantt.js"></script>';
    $js .= '<script type="text/javascript">
              $(document).ready(function()
              {
                var demo_tasks = {
                                  "data":'.$data_jadwal_json.'
                                };
                gantt.config.columns = [
                    {name:"text",       label:"Nama Pekerjaan",  width:350, tree:true, resize: true },
                    {name:"start_date", label:"Tgl Mulai",  width:100, align: "center", resize: true  },
                    {name:"end_date",   label:"Tgl Berakhir",  width:100, align: "center", resize: true  },
                    {name:"due_date",   label:"Jatuh Tempo",  width:100, align: "center", resize: true  },
                    {name:"duration",   label:"Lama Pekerjaan",  width:100,   align: "center", resize: true  },
                    {name: "vol", label: "Vol",  width:50, hide:false, resize: true   }
                ];
                gantt.config.grid_resize = true;
                //gantt.config.min_grid_column_width = 100;
                gantt.config.scale_unit = "year";
                gantt.config.step = 1;
                //gantt.config.date_scale = "%F, %Y";
                gantt.config.date_scale = "%Y";
                //gantt.config.min_column_width = 100;
                gantt.config.autosize = true;
                gantt.config.autofit = false;
                gantt.config.grid_width = 800;
                gantt.config.drag_move = false;
                gantt.config.drag_progress = false;
                gantt.config.drag_resize = false;
                gantt.config.drag_links = false;
                gantt.config.fit_tasks = "y";

                gantt.config.scale_height = 90;

                var weekScaleTemplate = function(date){
                  var dateToStr = gantt.date.date_to_str("%d %M");
                  var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                  return dateToStr(date) + " - " + dateToStr(endDate);
                };

                gantt.config.subscales = [
                  /*{unit:"week", step:1, date:"#%W" ,template:weekScaleTemplate},*/
                  {unit:"month", step:1, date:"%M"},
                  {unit:"week", step:1, date:"#%W"}
                ];
                gantt.attachEvent("onGanttReady", function(){
                    $(".gantt_grid").resizable({
                      handles: "e",
                      maxWidth: 800,
                      stop: function(e,u){
                        var originalSize = u.originalSize;
                        var currentsize = u.size;
                        var diffsize = originalSize.width - (currentsize.width-7);
                        $(".gantt_task").width(function(i,w){
                          return w + diffsize;
                        });
                      }
                      //alsoResize: ".gantt_grid_head_cell,.gantt_grid_scale,.gantt_cell,.gantt_grid_data,.gantt_row"
                    });
                });
                gantt.init("gantt_here");
                gantt.parse(demo_tasks);
                gantt.setSizes();
                
                $(".navbar-toggle ").on("click",function(){
                  gantt.setSizes();
                });
              });
            </script>';
    $css = '<link rel="stylesheet" href="'.current_admin_theme_url().'static/js/ganttchart/codebase/skins/dhtmlxgantt_broadway.css"/>';
    
    $this->assets->add_js($js,"body");
    $this->assets->add_css($css,"head");
    $listing = $this->_create_listing($this->init);
		if($is_login)
			#$this->load->view('layouts/mk_jadwal_proyek/listing_hierarcy',array('listing' => $listing,'response' => '','page_title' => 'Jadwal Proyek','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
			$this->load->view('layouts/mk_jadwal_proyek/ganttchart',array('listing' => $listing,'response' => '','page_title' => 'Jadwal Proyek','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
    if(isset($this->init['where']))
    unset($this->init['where']);
    
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_listing',array($this,'_hook_show_panel_allowed'));
    
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Jadwal Proyek'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Jadwal Proyek'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Jadwal Proyek'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function _daftar_kerja($object_id = "")
	{
    $url = base_url().$this->uri->segment(1) . "/" . "mk_jadwal_proyek_timeline/daftar_kerja/".$object_id;
    echo '<script>window.location.href="'.$url.'";</script>';
    exit;
    #header("location: ".base_url().$this->uri->segment(1) . "/" . "mk_jadwal_proyek_timeline/daftar_kerja/" . $object_id);
    #exit;
	}
	
	function daftar_kerja($object_id = "")
	{
		$this->_config_timeline();
		$this->data->init($this->init_timeline);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add_timeline'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add_timeline'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_jadwal_proyek/add_timeline',array('response' => $response,'page_title' => 'Jadwal Sub Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function edit_timeline($object_id = "")
	{
		$this->_config_timeline();
    if(isset($this->init_timeline['where']))
    unset($this->init_timeline['where']);
    $this->init = $this->init_timeline;
		$this->data->init($this->init_timeline);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit_timeline'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_listing',array($this,'_hook_show_panel_allowed'));
		
		$response = $this->data->edit("",$this->init_timeline);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_jadwal_proyek/add_timeline',array('response' => $response,'page_title' => 'Jadwal Proyek'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function listing($param = "")
	{
    $this->index($param = "");
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek",
            'where' => ' AND type_jadwal="jadwal" ',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          /*
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => false,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Kota',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_autocomplete',
                            #'query' => 'SELECT chain_geography.*,data_propinsi.nama_propinsi,data_kota.dat2,data_kota.nama_kota,data_kecamatan.nama_kecamatan,data_desa.nama_desa FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_desa_id = data_kota.data_desa_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            #'query' => 'SELECT concat(data_desa.nama_desa,",data_kecamatan.nama_kecamatan,",",data_kota.dat2," ",data_kota.nama_kota,",",",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_desa_id = data_kota.data_desa_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            'service_get_json' => array(
                              'table' => 'data_kota,data_propinsi',
                              'where' => 'data_kota.propinsi_id = data_propinsi.data_propinsi_id',
                              'select' => 'concat(data_kota.dat2," ",data_kota.nama_kota,", Prop. ",data_propinsi.nama_propinsi) label,data_kota.data_desa_id value',
                              'foreign_key' => 'data_kota.data_desa_id'
                            ),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),*/
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            #'query' => 'SELECT concat(data_desa.kode_desa,"-",nama_desa) label,data_desa_id value FROM mk_desa_lokasi,data_desa WHERE mk_desa_lokasi.desa_id  = data_desa.data_desa_id LIMIT 0,100',
                            'options' => array('' => '-----Pilih Desa-----'),
                            'js_connect_to' => 
                              array(  'table' => 'mk_desa_lokasi,data_desa',
                                      'where' => 'mk_desa_lokasi.desa_id  = data_desa.data_desa_id',
                                      'select' => 'concat(data_desa.kode_desa,"-",nama_desa) label,data_desa_id value',
                                      'primary_key' => 'data_desa_id',
                                      'foreign_key' => 'mk_desa_lokasi.master_kontrak_id',
                                      'id_field_parent' => '"#master_kontrak_id"'
                              ),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'base64' => true,
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'induk_jadwal_proyek_id',
                            'label' => 'Induk Pekerjaan',
                            'id' => 'induk_jadwal_proyek_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            //'query' => 'SELECT concat(kode_jadwal_proyek,"  -  ",nama_pekerjaan," ") label,mk_jadwal_proyek_id value FROM mk_jadwal_proyek ORDER BY induk_jadwal_proyek_id DESC',
                            'options' => array('' => '-----Pilih Induk Pekerjaan-----'),
                            'js_connect_to' => 
                                array(  'table' => 'mk_jadwal_proyek',
                                        'where' => '',
                                        'select' => 'concat(kode_jadwal_proyek,\'  -  \',nama_pekerjaan) label,mk_jadwal_proyek_id value',
                                        'primary_key' => 'mk_jadwal_proyek_id',
                                        'foreign_key' => 'master_kontrak_id',
                                        'id_field_parent' => '"#master_kontrak_id"'
                                ),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Nama Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_mce',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          /*
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),*/
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          )
                    ),
                    'where' => ' induk_jadwal_proyek_id = "0" ',
                    'primary_key' => 'mk_jadwal_proyek_id',
                    'path' => "/admin/",
                    'controller' => 'mk_jadwal_proyek',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Add Sub Task','name' => 'daftar_kerja', 'class' => 'glyphicon-edit'),
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_timeline($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek",
            'where' => ' AND type_jadwal="jadwal" ',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Nama Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Kode Desa',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Desa-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),/*
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),*/
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          )
                    ),
                    'primary_key' => 'mk_jadwal_proyek_id',
                    'path' => "/admin/",
                    'controller' => 'mk_jadwal_proyek',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit_timeline', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init_timeline = $init;
	}
	
	function _config_import($id_object = "")
	{
    $fields_insert = 
                    array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Nama Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Kota-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          )
                    );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_jadwal_proyek',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mk_jadwal_proyek_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_jadwal_proyek/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		return $param;
	}
	
	function _hook_do_add_timeline($param = "")
	{
		$induk_jadwal = $this->uri->segment(4);
		$q_induk_jadwal = $this->db->query("SELECT * FROM mk_jadwal_proyek WHERE mk_jadwal_proyek_id = '".$induk_jadwal."'");
		$d_induk_jadwal = $q_induk_jadwal->row_array();
		$kontrak_id = (isset($d_induk_jadwal['master_kontrak_id']))?$d_induk_jadwal['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['master_kontrak_id'] = (isset($kontrak['master_kontrak_id']))?$kontrak['master_kontrak_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['induk_jadwal_proyek_id'] = $induk_jadwal;
		$param['kota_id'] = (isset($d_induk_jadwal['kota_id']))?$d_induk_jadwal['kota_id']:0;
		return $param;
	}
  
	function _hook_do_edit($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		return $param;
	}
	
	function _hook_do_edit_timeline($param = "")
	{
    /*
    $induk_jadwal = $this->uri->segment(4);
    $q_induk_jadwal = $this->db->query("SELECT * FROM mk_jadwal_proyek WHERE mk_jadwal_proyek_id = '".$induk_jadwal."'");
    $d_induk_jadwal = $q_induk_jadwal->row_array();
    
    $kontrak_id = (isset($d_induk_jadwal['master_kontrak_id']))?$d_induk_jadwal['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $pks = $q->row_array();
    
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
    $vendor = $q->row_array();
    $param['master_kontrak_id'] = (isset($vendor['master_kontrak_id']))?$vendor['master_kontrak_id']:0;
    $param['master_vendor_id'] = (isset($vendor['mk_master_vendor_id']))?$vendor['mk_master_vendor_id']:0;
    $param['pks_id'] = (isset($pks['pks_id']))?$pks['pks_id']:0;
    $param['induk_jadwal_proyek_id'] = $induk_jadwal;
    $param['kota_id'] = (isset($d_induk_jadwal['kota_id']))?$d_induk_jadwal['kota_id']:0;
    */
    
    if(isset($param['master_kontrak_id']))
    unset($param['master_kontrak_id']);
    
    if(isset($param['master_vendor_id']))
    unset($param['master_vendor_id']);
    
    if(isset($param['pks_id']))
    unset($param['pks_id']);
    
    if(isset($param['induk_jadwal_proyek_id']))
    unset($param['induk_jadwal_proyek_id']);
    
    if(isset($param['kota_id']))
    unset($param['kota_id']);
    
		return $param;
	}
  
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Jadwal Proyek";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Jadwal Proyek";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_ajax_target_add_timeline(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_kota_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_kota WHERE data_kota_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_kota']) and isset($d['nama_kota']))?'#'.$d['kode_kota'].' - '.$d['nama_kota']:$default_value;
  }
  
  
	function _create_listing($config = array())
	{ 
    $this->load->library("data",array(),"data_jadwal");
		$this->data_jadwal->init($this->init);
		$this->data_jadwal->set_filter();
    $config = (is_array($config) and count($config) > 0)?$config:$this->data_jadwal->config;
		if($this->data_jadwal->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->data_jadwal->init_pagination($config);
		
		$t_nonce = $this->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$path = (isset($config['path']))?$config['path']:'';
		$controller = (isset($config['controller']))?$config['controller']:'';
		$function = (isset($config['function']))?$config['function']:'';
		$action = $this->data_jadwal->show_panel_allowed("",$path,$controller,$panel_function);
    
		$col = "";				
		if(!empty($this->data_jadwal->fields) and is_array($this->data_jadwal->fields) and count($this->data_jadwal->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->data_jadwal->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->uri->segment($this->data_jadwal->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->uri->segment($this->data_jadwal->paging_config['uri_segment'],0);
		
		$start_page  = $this->hook->do_action('hook_create_listing_start_page',$start_page);
		$to_page  = $this->hook->do_action('hook_create_listing_to_page',$this->data_jadwal->paging_config['per_page']);
		
		$sql = $this->data_jadwal->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_jadwal->data_rows = $data_rows;
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->data_jadwal->fields) and is_array($this->data_jadwal->fields) and count($this->data_jadwal->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr class="info">';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->data_jadwal->primary_key]))?$data_row[$this->data_jadwal->primary_key]:"";
						$tmp_list.= '
									<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
          $number = ++$start_page;
          $tmp_list.= '
                <td align="center" style="font-weight:bold;font-size:12px;">' . $number .'</td>';
					foreach($this->data_jadwal->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
							
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->data_jadwal->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'input_file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->data_jadwal->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->data_jadwal->show_image($path,'height="50"');
									}
								}
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->data_jadwal->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}else{
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->data_jadwal->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->data_jadwal->show_image($path,'height="50"');
									}
								}
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->data_jadwal->human_date($value,false,false);
								}
								
								if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
									$decoded_value = json_decode($value, true);
									
									if(is_array($decoded_value) and count($decoded_value) > 0){
										$value = "<ul style='padding:10px;'>";
										foreach($decoded_value as $idx => $dv){
											$label = $this->data_jadwal->get_label_value($dv['label'], $field['columns']);
											$value .= "<li><span>{$label} :</span> <span class='value_data'>{$dv['value']}</span> </li>";
										}
										$value .= "</ul>";
									}
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}
						}
					}
					if(!empty($action))
					{
						$action = $this->data_jadwal->show_panel_allowed("",$path,$controller,$panel_function,$data_row[$this->data_jadwal->primary_key]);
						$list_style = (isset($this->data_jadwal->the_config['list_style']))?$this->data_jadwal->the_config['list_style']:"class='rows_action'";
						$action  = $this->hook->do_action('hook_create_listing_action',$action);
						$tmp_list.= '<td '.$list_style.'>'. $action .'</td>';
					}
					$tmp_list.= '
								</tr>';
          
          $config2 = $this->init_timeline;
          $config2['where'] = " induk_jadwal_proyek_id = '".$data_row[$this->data_jadwal->primary_key]."' ";
          $config2['parent_number'] = $number;
          $tmp_list .= $this->_create_listing_timeline($config2);
					$list  .= $this->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%" class="table table-bordered table-hover">
						<thead>
							<tr>
                ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
  
	function _create_listing_timeline($config = array())
	{
    $this->load->library("data",array(),"data_timeline");
		$this->data_timeline->init($config);
		$this->data_timeline->set_filter();
    $config = (is_array($config) and count($config) > 0)?$config:$this->data_timeline->config;
		if($this->data_timeline->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->data_timeline->init_pagination($config);
		
		$t_nonce = $this->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$path = (isset($config['path']))?$config['path']:'';
		$controller = (isset($config['controller']))?$config['controller']:'';
		$function = (isset($config['function']))?$config['function']:'';
		$action = $this->data_timeline->show_panel_allowed("",$path,$controller,$panel_function);
    
		$col = "";				
		if(!empty($this->data_timeline->fields) and is_array($this->data_timeline->fields) and count($this->data_timeline->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->data_timeline->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->uri->segment($this->data_timeline->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->uri->segment($this->data_timeline->paging_config['uri_segment'],0);
		
		$start_page  = $this->hook->do_action('hook_create_listing_start_page',$start_page);
		$to_page  = $this->hook->do_action('hook_create_listing_to_page',$this->data_timeline->paging_config['per_page']);
		
		$sql = $this->data_timeline->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_timeline->data_rows = $data_rows;
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->data_timeline->fields) and is_array($this->data_timeline->fields) and count($this->data_timeline->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr>';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->data_timeline->primary_key]))?$data_row[$this->data_timeline->primary_key]:"";
						$tmp_list.= '
									<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
          $number = ++$start_page;
          $tmp_list.= '
									<td align="center">' . $config['parent_number'] .'.' . $number .'</td>';
					foreach($this->data_timeline->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
							
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->data_timeline->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'input_file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->data_timeline->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->data_timeline->show_image($path,'height="50"');
									}
								}
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->data_timeline->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}else{
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->data_timeline->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->data_timeline->show_image($path,'height="50"');
									}
								}
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->data_timeline->human_date($value,false,false);
								}
								
								if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
									$decoded_value = json_decode($value, true);
									
									if(is_array($decoded_value) and count($decoded_value) > 0){
										$value = "<ul style='padding:10px;'>";
										foreach($decoded_value as $idx => $dv){
											$label = $this->data_timeline->get_label_value($dv['label'], $field['columns']);
											$value .= "<li><span>{$label} :</span> <span class='value_data'>{$dv['value']}</span> </li>";
										}
										$value .= "</ul>";
									}
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}
						}
					}
					if(!empty($action))
					{
						$action = $this->data_timeline->show_panel_allowed("",$path,$controller,$panel_function,$data_row[$this->data_timeline->primary_key]);
						$list_style = (isset($this->data_timeline->the_config['list_style']))?$this->data_timeline->the_config['list_style']:"class='rows_action'";
						$action  = $this->hook->do_action('hook_create_listing_action',$action);
						$tmp_list.= '<td '.$list_style.'>'. $action .'</td>';
					}
					$tmp_list.= '
								</tr>';
					$list  .= $this->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
    
    return $list;
        $output = '<table width="100%" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
                ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _get_jadwal_proyek_heararchy($jadwal_proyek_id = "")
  {
    $output = array();
    $this->db->where(array("mk_jadwal_proyek_id" => $jadwal_proyek_id));
    $qdata_jadwal_proyek = $this->db->get("mk_jadwal_proyek");
    $data_jadwal_proyek = $qdata_jadwal_proyek->row_array();
    
    $this->db->where(array("indux_jadwal_proyek_id" => $jadwal_proyek_id));
    $this->db->where(array("type_jadwal" => 'jadwal'));
    
    $qsub_jadwal_proyek = $this->db->get("mk_jadwal_proyek");
    $sub_jadwal_proyek = $qsub_jadwal_proyek->result_array();
    
    $output[] = array(  "id"          => $data_jadwal_proyek['mk_jadwal_proyek_id'],
                        "text"        => $data_jadwal_proyek['nama_pekerjaan'],
                        "tanggal_mulai"  => $data_jadwal_proyek['tanggal_mulai'],
                        "tanggal_berakhir"    => $data_jadwal_proyek['tanggal_berakhir'],
                        "duration"    => $data_jadwal_proyek['jumlah_hari_tempuh'],
                        "parent"      => $data_jadwal_proyek['induk_jadwal_proyek_id'],
                        "progress"    => 0,
                        "open"        => 'true'
                      );
  }
  
  function _get_jadwal($master_kontrak_id = "",$vendor_id = "")
  {
    if(!empty($master_kontrak_id))
      $this->db->where(array("master_kontrak_id" => $master_kontrak_id));
    if(!empty($vendor_id))
      $this->db->where(array("vendor_id" => $vendor_id));
    
    $this->db->where(array("type_jadwal" => 'jadwal'));
    
    $qdata_jadwal_proyek = $this->db->get("mk_jadwal_proyek");
    $data_jadwal_proyek = $qdata_jadwal_proyek->result_array();
    
    $output = array();
    foreach($data_jadwal_proyek as $i => $r)
    {
      $tanggal_mulai = strtotime($r['tanggal_mulai']);
      $tanggal_berakhir = strtotime($r['tanggal_berakhir']);
      $datediff = abs($tanggal_berakhir - $tanggal_mulai);
      $jumlah_hari_tempuh = floor($datediff/(60*60*24));
      $data =  array(
                        "id"          => $r['mk_jadwal_proyek_id'],
                        "text"        => $r['nama_pekerjaan'],
                        "start_date"  =>  date("d-m-Y", strtotime($r['tanggal_mulai'])),
                        "end_date"    =>  date("d-m-Y", strtotime($r['tanggal_berakhir'])),
                        "due_date"    =>  $r['tanggal_jatuh_tempo'],
                        "duration"    => $jumlah_hari_tempuh,
                        "progress"    => 0,
                        "vol"         => $r['qty'],
                        "open"        => 'true'
                      );
       if($r['induk_jadwal_proyek_id'] > 0)
       {
          $data["parent"] = $r['induk_jadwal_proyek_id'];
       }
       $output[] = $data;
    }
    
    return $output;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
