<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_kontrak extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index($param = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_master_sumber_pendanaan_id',array($this,'_hook_create_listing_value_master_sumber_pendanaan_id'));
		$this->hook->add_action('hook_create_listing_value_total_anggaran',array($this,'_hook_create_listing_value_total_anggaran'));
		#$this->hook->add_action('hook_create_listing_value_tanggal_mulai',array($this,'_hook_create_listing_value_tanggal_mulai'));
		#$this->hook->add_action('hook_create_listing_value_tanggal_berakhir',array($this,'_hook_create_listing_value_tanggal_berakhir'));
		#$this->hook->add_action('hook_create_listing_value_tanggal_jatuh_tempo',array($this,'_hook_create_listing_value_tanggal_jatuh_tempo'));

		$is_login = $this->user_access->is_login();

        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/mk_kontrak/listing',array('response' => '','page_title' => 'Master Kontrak','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'protected')
				{
					$init[$index]['rules'] = "";
				}
				if(isset($i['name']) and $i['name'] == 'unprotected')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_kontrak/edit',array('response' => $response,'page_title' => 'Master Kontrak'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_kontrak/add',array('response' => $response,'page_title' => 'Master Kontrak'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
        $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_kontrak_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_master_sumber_pendanaan_id',array($this,'_hook_create_listing_value_master_sumber_pendanaan_id'));
		$this->hook->add_action('hook_create_form_view_value_total_anggaran',array($this,'_hook_create_listing_value_total_anggaran'));
		$this->hook->add_action('hook_create_form_view_value_tanggal_mulai',array($this,'_hook_create_listing_value_tanggal_mulai'));
		$this->hook->add_action('hook_create_form_view_value_tanggal_berakhir',array($this,'_hook_create_listing_value_tanggal_berakhir'));
		$this->hook->add_action('hook_create_form_view_value_tanggal_jatuh_tempo',array($this,'_hook_create_listing_value_tanggal_jatuh_tempo'));
		$this->hook->add_action('hook_create_form_view_value_protected',array($this,'_hook_create_listing_value_protected'));

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_kontrak/view',array('response' => '','page_title' => 'Master Kontrak'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing($param = "")
	{
        $this->index($param);
	}
	
	function _config($id_object = "")
	{
        $data_pks = $this->db->query('SELECT *,concat(kode_pks," - ",judul_pks) label FROM data_pks')->result_array();
        $data_pks_options = hierarchycal_array($data_pks,'induk_pks','data_pks_id',0,'label',0,'--');
        $data_pks_options = array_merge_recursive(array('' => '-----Pilih PKS-----'),$data_pks_options);
        
        $init = array(	'table' => "mk_master_kontrak",
						'fields' => array(
						  array(
							'name' => 'nomor_kontrak',
							'label' => 'Nomor Kontrak',
							'id' => 'nomor_kontrak',
							'value' => '',
							'type' => 'input_text',
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required'
						  ),
						  array(
							'name' => 'judul_kontrak',
							'label' => 'Judul Kontrak',
							'id' => 'judul_kontrak',
							'value' => '',
							'type' => 'input_textarea',
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required'
						  ),
						  array(
							'name' => 'pks_id',
							'label' => 'PKS',
							'id' => 'pks_id',
							'value' => '',
							'type' => 'input_selectbox',
							//'query' => 'SELECT concat("#",kode_pks,"-",judul_pks) label,data_pks_id value FROM data_pks',
							'options' => $data_pks_options,
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required',
                            'list_style' => ' style="width:200px;white-space:nowrap;" '
						),
						  array(
							'name' => 'master_vendor_id',
							'label' => 'Vendor',
							'id' => 'master_vendor_id',
							'value' => '',
							'type' => 'input_selectbox',
							'query' => 'SELECT concat(kode_vendor," - ",perusahaan) label,mk_master_vendor_id value FROM mk_master_vendor',
							'options' => array('' => '-----Pilih Vendor-----'),
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required',
                            'list_style' => ' style="width:200px;white-space:nowrap;" '
						),
						  array(
							'name' => 'master_sumber_pendanaan_id',
							'label' => 'Sumber Pendanaan',
							'id' => 'master_sumber_pendanaan_id',
							'value' => '',
							'type' => 'input_selectbox',
							'query' => 'SELECT concat(kode_sumber_pendanaan," - ",instansi) label,mk_sumber_pendanaan_id value FROM mk_sumber_pendanaan',
							'options' => array('' => '-----Pilih Sumber Pendanaan-----'),
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required',
                            'list_style' => ' style="width:200px;white-space:nowrap;" '
						),
						array(
							'name' => 'protected',
							'label' => 'File Kontrak',
							'id' => 'protected',
							'value' => '',
							'type' => 'input_file',
							'use_search' => false,
							'use_listing' => false,
							'config_upload' => array( 
													'upload_path' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_master_kontrak/protected/',
													'encrypt_name' => false,
													'allowed_types' =>  'pdf'
													),
							'rules' => 'required_file|required'
						),
						array(
							'name' => 'tanggal_mulai',
							'label' => 'Tanggal Mulai',
							'id' => 'tanggal_mulai',
							'value' => '',
							'type' => 'input_date',
							'use_search' => true,
							'use_listing' => true,
							'rules' => ''
						),
						array(
							'name' => 'tanggal_berakhir',
							'label' => 'Tanggal Berakhir',
							'id' => 'tanggal_berakhir',
							'value' => '',
							'type' => 'input_date',
							'use_search' => true,
							'use_listing' => true,
							'rules' => ''
						),
						array(
							'name' => 'tanggal_jatuh_tempo',
							'label' => 'Tanggal Jatuh Tempo',
							'id' => 'tanggal_jatuh_tempo',
							'value' => '',
							'type' => 'input_date',
							'use_search' => true,
							'use_listing' => true,
							'rules' => ''
						),
						array(
							'name' => 'total_anggaran',
							'label' => 'Total Anggaran',
							'id' => 'total_anggaran',
							'value' => '',
							'type' => 'input_text',
							'use_search' => false,
							'use_listing' => true,
							'rules' => ''
						),
						array(
                  'name' => 'status',
                  'label' => 'Status',
                  'id' => 'status',
                  'value' => '',
                  'type' => 'input_selectbox',
                  #'query' => 'SELECT concat("#",kode_pks,"-",judul_pks) label,data_pks_id value FROM data_pks',
                  'options' => array(	'' => '-----Pilih Status-----',
                                  'pending' => 'Pending',
                                  'inprogress' => 'In Progress',
                                  'cancel' => 'Cancel',
                                  'done' => 'Done'),
                    'use_search' => true,
                    'use_listing' => true,
                    'rules' => 'required'
						)
                    ),
                    'primary_key' => 'mk_master_kontrak_id',
                    'path' => "/admin/",
                    'controller' => 'mk_kontrak',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Master Kontrak";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Master Kontrak";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_master_sumber_pendanaan_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE mk_sumber_pendanaan_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_sumber_pendanaan']) or isset($d['instansi']))?$d['kode_sumber_pendanaan'].' - '.$d['instansi']:$default_value;
  }
  
  function _hook_create_listing_value_total_anggaran($default_value = ""){
      return format_currency($default_value,"");
  }
  
  function _hook_create_listing_value_tanggal_mulai($default_value = ""){
      return $this->data->human_date($default_value,$the_time = false, $the_day = false);
  }
  
  function _hook_create_listing_value_tanggal_berakhir($default_value = ""){
      return $this->data->human_date($default_value,$the_time = false, $the_day = false);
  }
  
  function _hook_create_listing_value_tanggal_jatuh_tempo($default_value = ""){
      return $this->data->human_date($default_value,$the_time = false, $the_day = false);
  }
  
  function _hook_create_listing_value_protected($default_value = ""){
      if(!empty($default_value))
      {
          $file_path = FCPATH."uploads/mk_master_kontrak/protected/".$default_value;
          $file_url = base_url()."uploads/mk_master_kontrak/protected/".$default_value;
          if(file_exists($file_path))
          {
            return '<a href="'.$file_url.'" title="klik untuk download file">'.$default_value.'</a>';
          }
      }
      return $default_value;
  }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
