<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_sumber_pendanaan extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_sumber_pendanaan_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Widget','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	
					'query' => "SELECT ba.*,sp.* FROM mk_beban_anggaran ba JOIN mk_sumber_pendanaan sp on sp.mk_sumber_pendanaan_id = ba.sumber_pendanaan_id",
					//'table' => "mk_sumber_pendanaan",
					'fields' => array(
									  array(
										'name' => 'master_kontrak_id',
										'label' => 'Master Kontrak',
										'id' => 'master_kontrak_id',
										'value' => '',
										'type' => 'input_selectbox',
										'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
										'options' => array('' => '-----Pilih Master Kontrak-----'),
										'use_search' => true,
										'use_listing' => true,
										'rules' => 'required',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'pks_id',
										'label' => 'PKS',
										'id' => 'pks_id',
										'value' => '',
										'type' => 'input_hidden',
										'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
										'options' => array('' => '-----Pilih PKS-----'),
										'use_search' => true,
										'use_listing' => true,
										'readonly' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'master_vendor_id',
										'label' => 'Vendor',
										'id' => 'vendor_id',
										'value' => '',
										'type' => 'input_hidden',
										'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
										'options' => array('' => '-----Pilih Vendor-----'),
										'use_search' => true,
										'use_listing' => true,
										'readonly' => true,
										'rules' => ''
									  ),
									  array(
										'name' => 'instansi',
										'label' => 'Sumber Pendanaan',
										'id' => 'instansi',
										'value' => '',
										'type' => 'input_selectbox',
										'query' => 'SELECT instansi label,mk_sumber_pendanaan_id value FROM mk_sumber_pendanaan',
										'options' => array('' => '-----Pilih Sumber Pendanaan-----'),
										'use_search' => true,
										'use_listing' => true,
										'readonly' => true,
										'rules' => ''
									  ),
									  array(
										'name' => 'beban_anggaran',
										'label' => 'Beban Anggran',
										'id' => 'beban_anggaran',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'dana_alokasi',
										'label' => 'Dana Alokasi',
										'id' => 'dana_alokasi',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'alamat_instansi',
										'label' => 'Alamat',
										'id' => 'alamat_instansi',
										'value' => '',
										'type' => 'input_textarea',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'nama',
										'label' => 'Nama Lengkap',
										'id' => 'nama',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'nama',
										'label' => 'Jabatan',
										'id' => 'nama',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'no_telepon',
										'label' => 'No. Telepon',
										'id' => 'no_telepon',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'email',
										'label' => 'Email',
										'id' => 'email',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'mk_sumber_pendanaan',
                    'function' => 'index',
                    'primary_key' => 'mk_beban_anggaran_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "ADD NEW WIDGET";
  }
  
  function _hook_create_form_title_edit($title){
    return "EDIT WIDGET";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
