<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_notifikasi_pekerjaan extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));
		$this->hook->add_action('hook_create_listing_value_data',array($this,'_hook_create_listing_value_data'));

		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/mk_notifikasi_pekerjaan/listing',array('response' => '','page_title' => 'Notifikasi Pekerjaan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_notifikasi_pekerjaan/edit',array('response' => $response,'page_title' => 'Notifikasi Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_do_after_add',array($this,'_hook_do_after_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_notifikasi_pekerjaan/add',array('response' => $response,'page_title' => 'Notifikasi Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));
		#$this->hook->add_action('hook_create_form_view_value_data',array($this,'_hook_create_listing_value_data'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_notifikasi_pekerjaan/view',array('response' => '','page_title' => 'Notifikasi Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _send_notification($data_notifikasi = array())
	{
        $this->load->helper('directory');
		$this->load->library("pdfwriter");
		$this->load->library("email");
        $this->load->helper("file");
        
		if(isset($data_notifikasi['data']) and is_array($data_notifikasi['data']) and count($data_notifikasi['data']) > 0)
		{
			if(!file_exists('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id']))
			{
				@mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'],0777,1);
			}else{
                /*
				$files = directory_map('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/');
				if(is_array($files) and count($files) > 0)
				{
					foreach($files as $if => $file)
					{
						if(is_file($file))
						{
							unlink('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$file);
						}
					}
				}*/
                delete_files(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'], true);
				@mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'],0777,1);
			}
            
            $type_user = array();
			foreach($data_notifikasi['data'] as $i => $d)
			{
				$file_name = "";
				$html = "";
				$paper = "A2";
				$orientation = "L";
				switch($d)
				{
					case 'Master Kontrak':
					ob_start();
					$this->attach_master_kontrak($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'master_kontrak';
					$paper = "A4";
					$orientation = "P";
					break;
					case 'Master Vendor':
					ob_start();
					$this->attach_master_vendor($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'master_vendor';
					$paper = "A4";
					$orientation = "P";
					break;
					case 'Sumber Pendanaan':
					ob_start();
					$this->attach_sumber_pendanaan($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'sumber_pendanaan';
					$paper = "A4";
					$orientation = "P";
					break;
					case 'Jadwal Proyek':
					ob_start();
					$this->attach_jadwal_proyek($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'jadwal_proyek';
					$paper = "A2";
					$orientation = "L";
					break;
					case 'Syarat dan Ketentuan':
					ob_start();
					$this->attach_syarat_dan_ketentuan($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'syarat_dan_ketentuan';
					$paper = "A4";
					$orientation = "P";
					break;
					case 'Desa Kelurahan - Lokasi Proyek':
					ob_start();
					$this->attach_desa_kelurahan_lokasi_proyek($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'desa_kelurahan_lokasi_proyek';
					$paper = "A4";
					$orientation = "P";
					break;
					case 'Spesifikasi':
					ob_start();
					$this->attach_spesifikasi($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'spesifikasi';
					break;
					case 'Harga dan Total Biaya':
					ob_start();
					$this->attach_harga_dan_total_biaya($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'harga_dan_total_biaya';
					$paper = "A2";
					$orientation = "L";
					break;
					case 'Beban Anggaran':
					ob_start();
					$this->attach_beban_anggaran($data_notifikasi['master_kontrak_id']);
					$html = ob_get_clean();
					$file_name = 'beban_anggaran';
					$paper = "A2";
					$orientation = "L";
					break;
					case 'Informasi User Login':
					$file_name = 'informasi_user_login';
					$paper = "A4";
					$orientation = "P";
					break;
				}
				
				if($file_name == 'informasi_user_login')
				{
					if($data_notifikasi['send_type'] == 'vendor')
					{
						ob_start();
						$this->attach_informasi_user_login($data_notifikasi['master_kontrak_id'],'vendor');
						$html = ob_get_clean();
						$type_user['_vendor'] = $html;
					}elseif($data_notifikasi['send_type'] == 'sumber pendanaan')
					{
						ob_start();
						$this->attach_informasi_user_login($data_notifikasi['master_kontrak_id'],'sumber_pendanaan');
						$html = ob_get_clean();
						$type_user['_sumber_pendanaan'] = $html;
					}elseif($data_notifikasi['send_type'] == 'semua'){
						ob_start();
						$this->attach_informasi_user_login($data_notifikasi['master_kontrak_id'],'vendor');
						$html_vendor = ob_get_clean();
						$type_user['_vendor'] = $html_vendor;
						ob_start();
						$this->attach_informasi_user_login($data_notifikasi['master_kontrak_id'],'sumber_pendanaan');
						$html_sumber_pendanaan = ob_get_clean();
						$type_user['_sumber_pendanaan'] = $html_sumber_pendanaan;
					}
					if(is_array($type_user) and count($type_user) > 0)
					{
						foreach($type_user as $itu => $tu)
						{
							$this->pdfwriter->set_html($tu);
							$this->pdfwriter->mpdf(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$file_name.$itu,$paper,$orientation,false);
						}
					}
				}else{
					$this->pdfwriter->set_html($html);
					$this->pdfwriter->mpdf(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$file_name,$paper,$orientation,false);
				}
			}
            
            $email_user = array();
            
            if($data_notifikasi['send_type'] == 'vendor')
            {
                $email_user['_vendor'] = 1;
            }elseif($data_notifikasi['send_type'] == 'sumber pendanaan')
            {
                $email_user['_sumber_pendanaan'] = 1;
            }elseif($data_notifikasi['send_type'] == 'semua'){
                $email_user['_sumber_pendanaan'] = 1;
                $email_user['_vendor'] = 1;
            }
            

            if(is_array($email_user) and count($email_user) > 0)
            {
                $this->load->library("zip");
                
                foreach($email_user  as $i_zip => $zip)
                {
                    $this->zip->clear_data();
                    #$this->zip->add_dir(dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id']);
                    $files = directory_map('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/');

                    if(is_array($files) and count($files) > 0)
                    {
                        foreach($files as $if => $file)
                        {
                            if(is_file('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$file))
                            {
                                echo $file . ' => '.  $i_zip . '<br/>';
                                $file_extention = explode(".",$file);
                                $file_extention = $file_extention[count($file_extention)-1];
                                if(($file_extention == 'pdf' and $i_zip == '_sumber_pendanaan' and $file == 'informasi_user_login_vendor.pdf') or ($i_zip == '_vendor' and $file == 'informasi_user_login_sumber_pendanaan.pdf'))
                                {}else{
                                    $this->zip->read_file('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$file); 
                                }
                            }
                        }
                    }
                    
                    $zip_path = dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/attachment-pmpldt-'.$i_zip.'.zip';
                    #$this->zip->read_dir('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/', FALSE); 
                    $this->zip->archive($zip_path); 
                    
                    #$config['protocol'] = 'sendmail';
                    #$config['mailpath'] = '/usr/sbin/sendmail';
                    $config['useragent'] = 'pm-pldt';
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = SMTP_HOST;
                    $config['smtp_port'] = SMTP_PORT;
                    $config['smtp_user'] = SMTP_USER;
                    $config['smtp_pass'] = SMTP_PASS;
                    $config['charset'] = 'utf-8';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = TRUE;

                    $this->email->initialize($config);
                    $this->email->clear();
                    $this->email->from(ADMIN_EMAIL,ADMIN_NAME);
                    $this->email->subject('Email Notifikasi Pekerjaan - PM-PLDT');
                    
                    if($i_zip == '_vendor')
                        $this->email->message($data_notifikasi['pesan_vendor']);
                    if($i_zip == '_sumber_pendanaan')
                        $this->email->message($data_notifikasi['pesan_sumber_pendanaan']);
                    
                    $this->email->attach($zip_path);
                    if(isset($data_notifikasi['attachment']) and !empty($data_notifikasi['attachment']) and file_exists('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$data_notifikasi['attachment']))
                    {
                        $this->email->attach('./uploads/mk_notifikasi_pekerjaan/temp_pdf/'.$data_notifikasi['master_kontrak_id'].'/'.$data_notifikasi['attachment']);
                    }
                    
                    if($i_zip == '_vendor')
                    {
                        $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$data_notifikasi['master_vendor_id']."'");
                        $data_vendor = $q->row_array();
                        $email_vendor = (isset($data_vendor['email']))?$data_vendor['email']:"";
                        $this->email->to($email_vendor);
                        $this->email->set_newline("\r\n");
                        if(!empty($email_vendor))
                        {
                            $this->email->send();
                            #echo $this->email->print_debugger();
                        }
                    }
                    
                    if($i_zip == '_sumber_pendanaan')
                    {
                        $q = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE mk_sumber_pendanaan_id = '".$data_notifikasi['sumber_pendanaan_id']."'");
                        $data_sumber_pendanaan = $q->row_array();
                        $email_sumber_pendanaan = (isset($data_sumber_pendanaan['email']))?$data_sumber_pendanaan['email']:"";
                        $this->email->to($email_sumber_pendanaan);
                        $this->email->set_newline("\r\n");
                        if(!empty($email_sumber_pendanaan))
                        {
                            $this->email->send();
                            #echo $this->email->print_debugger();
                        }
                    }
                    
                }
            }
        }
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_notifikasi_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));
		$this->hook->add_action('hook_create_listing_value_data',array($this,'_hook_create_listing_value_data'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_notifikasi_pekerjaan/listing',array('response' => '','page_title' => 'Notifikasi Pekerjaan','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
		$init = array(	'table' => "mk_notifikasi",
					'fields' => array(
						  array(
							'name' => 'master_kontrak_id',
							'label' => 'Master Kontrak',
							'id' => 'master_kontrak_id',
							'value' => '',
							'type' => 'input_selectbox',
							'query' => 'SELECT concat("#",mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
							'options' => array('' => '-----Pilih Master Kontrak-----'),
							'use_search' => true,
							'use_listing' => true,
							'rules' => 'required',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'pks_id',
							'label' => 'PKS',
							'id' => 'pks_id',
							'value' => '',
							'type' => 'input_hidden',
							'query' => 'SELECT judul_pks label,data_pks_id value FROM data_pks',
							'options' => array('' => '-----Pilih PKS-----'),
							'use_search' => true,
							'use_listing' => true,
							'readonly' => true,
							'rules' => '',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'master_vendor_id',
							'label' => 'Vendor',
							'id' => 'vendor_id',
							'value' => '',
							'type' => 'input_hidden',
							'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
							'options' => array('' => '-----Pilih Vendor-----'),
							'use_search' => true,
							'use_listing' => true,
							'rules' => ''
						  ),
						  array(
							'name' => 'sumber_pendanaan_id',
							'label' => 'Sumber Pendanaan',
							'id' => 'sumber_pendanaan_id',
							'value' => '',
							'type' => 'input_hidden',
							'query' => 'SELECT instansi label,mk_sumber_pendanaan_id value FROM mk_sumber_pendanaan',
							'options' => array('' => '-----Pilih Sumber Pendanaan-----'),
							'use_search' => true,
							'use_listing' => true,
							'rules' => ''
						  ),
						  array(
							'name' => 'send_type',
							'label' => 'Tujuan',
							'id' => 'send_type',
							'value' => '',
							'type' => 'input_selectbox',
							'options' => array('' => '-----Pilih Tujuan-----','vendor' => 'Vendor','sumber pendanaan' => 'Sumber Pendanaan','semua' => 'Vendor dan Sumber Pendanaan'),
							'use_search' => true,
							'use_listing' => true,
							'readonly' => true,
							'rules' => 'required',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'pesan_sumber_pendanaan',
							'label' => 'Pesan Tambahan kepada Sumber Pendanaan',
							'id' => 'pesan_sumber_pendanaan',
							'value' => '',
							'type' => 'input_mce',
							'use_search' => false,
							'use_listing' => false,
							'rules' => '',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'pesan_vendor',
							'label' => 'Pesan Tambahan kepada Vendor',
							'id' => 'pesan_vendor',
							'value' => '',
							'type' => 'input_mce',
							'use_search' => false,
							'use_listing' => false,
							'rules' => '',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  /*
						  array(
							'name' => 'send_to',
							'label' => 'Kirim Ke',
							'id' => 'send_to',
							'value' => '',
							'type' => 'input_hidden',
							'query' => 'SELECT judul_pks label,data_pks_id value FROM data_pks',
							'options' => array('' => '-----Pilih PKS-----'),
							'use_search' => false,
							'use_listing' => false,
							'readonly' => true,
							'rules' => '',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),*/
						  array(
							'name' => 'send_datetime',
							'label' => 'Tanggal Pengiriman',
							'id' => 'send_datetime',
							'value' => date('Y-m-d H:i:s'),
							'type' => 'input_hidden',
							'use_search' => true,
							'use_listing' => true,
							'readonly' => true,
							'rules' => '',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'data',
							'label' => 'Data yang dikirim',
							'id' => 'data',
							'value' => '',
							'type' => 'input_checkbox',
							'options' => array(
													'mk_kontrak' => 'Master Kontrak',
													'mk_vendor' => 'Master Vendor',
													'mk_sumber_pendanaan' => 'Sumber Pendanaan',
													#'mk_jadwal_proyek' => 'Jadwal Proyek',
													#'mk_pengoperasian' => 'Penanggung Jawab Pengoperasian',
													'mk_syarat_ketentuan' => 'Syarat dan Ketentuan',
													'mk_desa_proyek' => 'Desa Kelurahan - Lokasi Proyek',
													'mk_spesifikasi' => 'Spesifikasi',
													#'mk_cakupan_pekerjaan' => 'Cakupan Pekerjaan',
													'mk_harga_biaya' => 'Harga dan Total Biaya',
													'mk_beban_anggaran' => 'Beban Anggaran',
													'profile' => 'Informasi User Login'
												),
							'use_search' => false,
							'use_listing' => false,
							'rules' => 'required',
							'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
						  ),
						  array(
							'name' => 'attachment',
							'label' => 'Attachment Lainnya',
							'id' => 'attachment',
							'value' => '',
							'type' => 'input_file',
							'use_search' => false,
							'use_listing' => false,
							'config_upload' => array( 
													'upload_path' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/mk_notifikasi_pekerjaan/',
													'encrypt_name' => false,
													'allowed_types' =>  'pdf|zip|doc|xls|docx|xlsx|jpg|gif|png'
													),
							'rules' => ''
						  )
						),
						'path' => "/admin/",
						'controller' => 'mk_notifikasi_pekerjaan',
						'function' => 'index',
						'primary_key' => 'mk_notifikasi_id',
						'panel_function' => array(
																			#array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
																			array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
																			array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog'),
																			#array('title' => 'Send Notification','name' => 'send_notification', 'class' => 'glyphicon glyphicon-send')
																		),
						'bulk_options' => array(
																			array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog'),
																			#array('title' => 'Send Notification','name' => 'send_notification', 'class' => 'glyphicon glyphicon-send')
																		)
          );
        
        $js = '
        $("body").on("click",".view_attachment",function(){
            var the_function = $(this).attr("id");
            var master_kontrak_id = $("form#form #master_kontrak_id").val();
            if(!master_kontrak_id)
            {
                alert("Silahkan pilih master kontrak terlebih dahulu");
            }else{
                $.ajax({
                    data:"is_modal=1",
                    type: "post",
                    url: "'.base_url().'admin/mk_notifikasi_pekerjaan/"+the_function+"/"+master_kontrak_id,
                    success: function(content)
                    {
                        var the_content = $("<div></div>").append(content);
                        var html_content = $(the_content).find(".ajax_container").html();
                        html_content = (!html_content)?$(the_content).html():html_content;
                        $("#popupoverlay").find(".modal-content").html(html_content);
                        $("#popupoverlay").modal("show");
                    }
                });
            }
        });
        ';
        
        $this->hook->add_action("form_fields_input_checkbox_mk_kontrak",array($this,"_form_fields_input_checkbox_mk_kontrak"));
        $this->hook->add_action("form_fields_input_checkbox_mk_vendor",array($this,"_form_fields_input_checkbox_mk_vendor"));
        $this->hook->add_action("form_fields_input_checkbox_mk_sumber_pendanaan",array($this,"_form_fields_input_checkbox_mk_sumber_pendanaan"));
        $this->hook->add_action("form_fields_input_checkbox_mk_jadwal_proyek",array($this,"_form_fields_input_checkbox_mk_jadwal_proyek"));
        $this->hook->add_action("form_fields_input_checkbox_mk_pengoperasian",array($this,"_form_fields_input_checkbox_mk_pengoperasian"));
        $this->hook->add_action("form_fields_input_checkbox_mk_syarat_ketentuan",array($this,"_form_fields_input_checkbox_mk_syarat_ketentuan"));
        $this->hook->add_action("form_fields_input_checkbox_mk_desa_proyek",array($this,"_form_fields_input_checkbox_mk_desa_proyek"));
        $this->hook->add_action("form_fields_input_checkbox_mk_spesifikasi",array($this,"_form_fields_input_checkbox_mk_spesifikasi"));
        $this->hook->add_action("form_fields_input_checkbox_mk_cakupan_pekerjaan",array($this,"_form_fields_input_checkbox_mk_cakupan_pekerjaan"));
        $this->hook->add_action("form_fields_input_checkbox_mk_harga_biaya",array($this,"_form_fields_input_checkbox_mk_harga_biaya"));
        $this->hook->add_action("form_fields_input_checkbox_mk_beban_anggaran",array($this,"_form_fields_input_checkbox_mk_beban_anggaran"));
        $this->hook->add_action("form_fields_input_checkbox_profile",array($this,"_form_fields_input_checkbox_profile"));
        $this->assets->add_js_inline($js,"body");
		$this->init = $init;
	}
	
    function _form_fields_input_checkbox_mk_kontrak($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_master_kontrak" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_vendor($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_master_vendor" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_sumber_pendanaan($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_sumber_pendanaan" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_jadwal_proyek($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_jadwal_proyek" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_pengoperasian($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_penanggung_jawab_pengoperasian" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_syarat_ketentuan($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_syarat_dan_ketentuan" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_desa_proyek($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_desa_kelurahan_lokasi_proyek" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_spesifikasi($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_spesifikasi" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_cakupan_pekerjaan($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_cakupan_pekerjaan" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_harga_biaya($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_harga_dan_total_biaya" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_mk_beban_anggaran($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_beban_anggaran" class="view_attachment">'.$param.'</a>';
        return $output;
    }
    function _form_fields_input_checkbox_profile($param = ""){
        $output = '<a href="javascript:void(0);" title="Click untuk melihat data" id="attach_informasi_user_login" class="view_attachment">'.$param.'</a>';
        return $output;
    }

	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['send_datetime'] = date("Y-m-d H:i:s");		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['sumber_pendanaan_id'] = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:0;

		if(isset($param['data']) and is_array($param['data']) and count($param['data']) > 0)
		{
			$param['data'] = json_encode($param['data']);
		}else{
			if(isset($param['data']))
				unset($param['data']);
		}
		return $param;
	}

	function _hook_do_after_add($param = "")
	{
		$q = $this->db->query("SELECT * FROM mk_notifikasi WHERE mk_notifikasi_id = '".$param."'");
		$data_notifikasi = $q->row_array();
		$data_notifikasi['data'] = json_decode($data_notifikasi['data'],false);
		$this->_send_notification($data_notifikasi);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		#$q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
		#$vendor = $q->row_array();
		
		#$q_sumber_dana = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE master_kontrak_id = '".$kontrak_id."'");
		#$q_sumber_dana = $q_sumber_dana->row_array();

		$param['send_datetime'] = date("Y-m-d H:i:s");		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['sumber_pendanaan_id'] = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:0;

		if(isset($param['data']) and is_array($param['data']) and count($param['data']) > 0)
		{
			$param['data'] = json_encode($param['data']);
		}else{
			if(isset($param['data']))
				unset($param['data']);
		}
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Kirim Notifikasi Pekerjaan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Notifikasi Pekerjaan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?"#".$d['nomor_kontrak'].'-'.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?'#'.$d['kode_pks'].'-'.$d['judul_pks']:$default_value;
  }
  
  /*
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nama']) or isset($d['perusahaan']))?$d['nama'].'-'.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_sumber_pendanaan_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE mk_sumber_pendanaan_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nama']) or isset($d['instansi']))?$d['nama'].'-'.$d['instansi']:$default_value;
  }
  */
  
  function _hook_create_listing_value_data($default_value = "")
  {
	 $value = json_decode($default_value);
	 $output = "";
	 if(((is_string($default_value) && 
         (is_object(json_decode($default_value)) || 
         is_array(json_decode($default_value))))))
	 {
		 if(is_array($value) and count($value) > 0)
		 {
			 foreach($value as $i => $v)
			 {
				 $output .= $v."<br/>";
			 }
		 }
	 }
	 $output = (empty($output))?$default_value:$output;
    return $output;
  }
  
    function attach_master_kontrak($master_kontrak_id = "")
    {
        #$is_login = $this->user_access->is_login(); bypass by pdf writer
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        if(!empty($master_kontrak_id))
        {
            $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
            $kontrak = $this->db->get("mk_master_kontrak")->row_array();
            
            $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
            $sumber_pendanaan_id = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:"";
            $vendor_id = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:"";
            
            $this->db->where(array("data_pks_id" => $pks_id));
            $data_pks = $this->db->get("data_pks")->row_array();
            $pks = (isset($data_pks['judul_pks']))?$data_pks['judul_pks']:"";
            
            $this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
            $data_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
            $sumber_pendanaan = (isset($data_sumber_pendanaan['instansi']))?$data_sumber_pendanaan['instansi']:"";
            
            $this->db->where(array("mk_master_vendor_id" => $vendor_id));
            $data_vendor = $this->db->get("mk_master_vendor")->row_array();
            $vendor = (isset($data_vendor['perusahaan']))?$data_vendor['perusahaan']:"";
            
            if(!empty($pks))
                $kontrak['pks'] = $pks;
            if(!empty($sumber_pendanaan))
                $kontrak['sumber_pendanaan'] = $sumber_pendanaan;
            if(!empty($vendor))
                $kontrak['vendor'] = $vendor;
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_master_kontrak',array('response' => '','page_title' => 'Data Kontrak','data_kontrak' => $kontrak));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data kontrak tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
    }

    function attach_master_vendor($master_kontrak_id = ""){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $sumber_pendanaan_id = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:"";
        $vendor_id = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:"";
        $this->db->where(array("mk_master_vendor_id" => $vendor_id));
        $data_vendor = $this->db->get("mk_master_vendor")->row_array();
        
        if(!empty($master_kontrak_id) and isset($data_vendor ['mk_master_vendor_id']) and !empty($data_vendor ['mk_master_vendor_id']))
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $data_pks = $this->db->get("data_pks")->row_array();
            $pks = (isset($data_pks['judul_pks']))?$data_pks['judul_pks']:"";
            
            $this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
            $data_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
            $sumber_pendanaan = (isset($data_sumber_pendanaan['instansi']))?$data_sumber_pendanaan['instansi']:"";
            
            
            if(!empty($pks))
                $kontrak['pks'] = $pks;
            if(!empty($sumber_pendanaan))
                $kontrak['sumber_pendanaan'] = $sumber_pendanaan;
            if(!empty($vendor))
                $kontrak['vendor'] = $vendor;
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_master_vendor',array('response' => '','page_title' => 'Data Vendor','data_vendor' => $data_vendor));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data vendor tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
     
     }

    function attach_sumber_pendanaan($master_kontrak_id = ""){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $sumber_pendanaan_id = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:"";
        $this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
        $data_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
        
        if(!empty($master_kontrak_id) and isset($data_sumber_pendanaan ['mk_sumber_pendanaan_id']) and !empty($data_sumber_pendanaan ['mk_sumber_pendanaan_id']))
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $data_pks = $this->db->get("data_pks")->row_array();
            $pks = (isset($data_pks['judul_pks']))?$data_pks['judul_pks']:"";
            
            
            
            if(!empty($pks))
                $kontrak['pks'] = $pks;
            if(!empty($sumber_pendanaan))
                $kontrak['sumber_pendanaan'] = $sumber_pendanaan;
            if(!empty($vendor))
                $kontrak['vendor'] = $vendor;
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_sumber_pendanaan',array('response' => '','page_title' => 'Data Sumber Pendanaan','data_sumber_pendanaan' => $data_sumber_pendanaan));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data sumber pendanaan tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
     
     }

    function attach_jadwal_proyek(){}

    function attach_penanggung_jawab_pengoperasian(){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $sumber_pendanaan_id = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:"";
        $this->db->where(array("mk_koperasi_id" => $sumber_pendanaan_id));
        $data_sumber_pendanaan = $this->db->get("mk_koperasi")->row_array();
        
        if(!empty($master_kontrak_id) and isset($data_sumber_pendanaan ['mk_sumber_pendanaan_id']) and !empty($data_sumber_pendanaan ['mk_sumber_pendanaan_id']))
        {
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_penanggung_jawab_pengoperasian',array('response' => '','page_title' => 'Data Sumber Pendanaan','data_sumber_pendanaan' => $data_sumber_pendanaan));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data sumber pendanaan tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
     
     }

    function attach_syarat_dan_ketentuan($master_kontrak_id = ""){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $this->db->where(array("master_kontrak_id" => $master_kontrak_id));
        $data_syarat_dan_ketentuan = $this->db->get("mk_syarat_ketentuan")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_syarat_dan_ketentuan) and count($data_syarat_dan_ketentuan) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_syarat_dan_ketentuan',array('response' => '','page_title' => 'Data Syarat dan Ketentuan','data_syarat_dan_ketentuan' => $data_syarat_dan_ketentuan,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Syarat dan Ketentuan tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
     }

    function attach_desa_kelurahan_lokasi_proyek($master_kontrak_id = ""){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $this->db->where(array("master_kontrak_id" => $master_kontrak_id));
        $data_desa_kelurahan_lokasi_proyek = $this->db->get("mk_desa_lokasi")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_desa_kelurahan_lokasi_proyek) and count($data_desa_kelurahan_lokasi_proyek) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_desa_kelurahan_lokasi_proyek',array('response' => '','page_title' => 'Desa/Kelurahan Lokasi Proyek','data_desa_kelurahan_lokasi_proyek' => $data_desa_kelurahan_lokasi_proyek,'other_data' => $other_data));
        }else{
            $output = '<p class="alert alert-warning">Data Desa/Kelurahan Lokasi Proyek tidak tersedia.</p>';
        }
        echo $output;
     }

    function attach_spesifikasi($master_kontrak_id = ""){
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $data_spesifikasi = $this->db->query("SELECT * FROM mk_cakupan_kerja,mk_spesifikasi where mk_cakupan_kerja.spesifikasi_id = mk_spesifikasi.mk_spesifikasi_id AND master_kontrak_id = '".$master_kontrak_id."'")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_spesifikasi) and count($data_spesifikasi) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_spesifikasi',array('response' => '','page_title' => 'Desa/Kelurahan Lokasi Proyek','data_spesifikasi' => $data_spesifikasi,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Spesifikasi tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
      }

    function attach_cakupan_pekerjaan($master_kontrak_id = ""){
        $this->load->model("model_cakupan_pekerjaan");
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $data_cakupan_kerja = $this->db->query("SELECT * FROM mk_cakupan_kerja,mk_spesifikasi where mk_cakupan_kerja.spesifikasi_id = mk_spesifikasi.mk_spesifikasi_id AND master_kontrak_id = '".$master_kontrak_id."' ORDER BY mk_cakupan_kerja.tanggal_rencana")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_cakupan_kerja) and count($data_cakupan_kerja) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_cakupan_pekerjaan',array('response' => '','page_title' => 'Cakupan Pekerjaan','data_cakupan_kerja' => $data_cakupan_kerja,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Cakupan Pekerjaan tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
      }

      function _hook_create_listing_value_spesifikasi_id($default_value = ""){
        $q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE mk_spesifikasi_id = '".$default_value."'");
        $d = $q->row_array();
        return (isset($d['produk']))?$d['kode_spesifikasi'].'-'.$d['produk'].'-'.$d['modul_tipe']:$default_value;
      }
      
    function attach_harga_dan_total_biaya($master_kontrak_id = ""){
        $this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,"_hook_create_listing_value_spesifikasi_id"));
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $data_harga_dan_total_biaya = $this->db->query("SELECT * FROM mk_harga_total_biaya,mk_spesifikasi where mk_harga_total_biaya.spesifikasi_id = mk_spesifikasi.mk_spesifikasi_id AND master_kontrak_id = '".$master_kontrak_id."'")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_harga_dan_total_biaya) and count($data_harga_dan_total_biaya) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_harga_dan_total_biaya',array('response' => '','page_title' => 'Harga dan Total Biaya','data_harga_dan_total_biaya' => $data_harga_dan_total_biaya,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Harga dan Total Biaya tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
      }

      function _hook_create_listing_value_master_vendor_id($default_value = ""){
        $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
        $d = $q->row_array();
        return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
      }
      
      function _hook_create_listing_value_sumber_pendanaan_id($default_value = ""){
        $q = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE mk_sumber_pendanaan_id = '".$default_value."'");
        $d = $q->row_array();
        return (isset($d['kode_sumber_pendanaan']) or isset($d['instansi']))?$d['kode_sumber_pendanaan'].' - '.$d['instansi']:$default_value;
      }
      
      
    function attach_beban_anggaran($master_kontrak_id = ""){
        $this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,"_hook_create_listing_value_master_vendor_id"));
        $this->hook->add_action('hook_create_listing_value_sumber_pendanaan_id',array($this,"_hook_create_listing_value_sumber_pendanaan_id"));
        $is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
        
        $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:"";
        $this->db->where(array("master_kontrak_id" => $master_kontrak_id));
        $data_beban_anggaran = $this->db->get("mk_beban_anggaran")->result_array();
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_beban_anggaran) and count($data_beban_anggaran) > 0)
        {
            $this->db->where(array("data_pks_id" => $pks_id));
            $pks = $this->db->get("data_pks")->row_array();
            
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_beban_anggaran',array('response' => '','page_title' => 'Beban Anggaran','data_beban_anggaran' => $data_beban_anggaran,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Beban Anggaran tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
      }

    function attach_informasi_user_login($master_kontrak_id = "",$type_user = ""){
				$is_login = 1;
        
        if($is_login){}else
        {
            $this->load->view('layouts/login');
            exit;
        }
        
        $output = "";
        $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
        $kontrak = $this->db->get("mk_master_kontrak")->row_array();
                
        $sumber_pendanaan_id = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:"";
        $vendor_id = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:"";
        
        $this->db->where(array("mk_master_vendor_id" => $vendor_id));
        $vendor = $this->db->get("mk_master_vendor")->row_array();
                
        $this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
        $sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
        
        $data_user_login = array();
        if(isset($vendor['kode_vendor']) and !empty($vendor['kode_vendor']))
        {
            $data_user_login['vendor'] = $this->db->query("SELECT * FROM user_accounts,user_pass WHERE user_accounts.user_id = user_pass.user_id AND user_accounts.username = '".$vendor['kode_vendor']."' AND user_accounts.user_level_id = '7' ")->row_array();
        }
        if(isset($sumber_pendanaan['kode_sumber_pendanaan']) and !empty($sumber_pendanaan['kode_sumber_pendanaan']))
        {
            $data_user_login['sumber_pendanaan'] = $this->db->query("SELECT * FROM user_accounts,user_pass WHERE user_accounts.user_id = user_pass.user_id AND user_accounts.username = '".$sumber_pendanaan['kode_sumber_pendanaan']."' AND user_accounts.user_level_id = '8' ")->row_array();
        }
        $other_data = array();
        if(!empty($master_kontrak_id) and is_array($data_user_login) and count($data_user_login) > 0)
        {   
            $other_data['kode_pks'] = (isset($pks['kode_pks']))?$pks['kode_pks']:"";;
            $other_data['judul_pks'] = (isset($pks['judul_pks']))?$pks['judul_pks']:"";;
            $other_data['nomor_kontrak'] = (isset($kontrak['nomor_kontrak']))?$kontrak['nomor_kontrak']:"";
            $other_data['judul_kontrak'] = (isset($kontrak['judul_kontrak']))?$kontrak['judul_kontrak']:"";
            $other_data['type_user'] = $type_user;
            $this->load->view('layouts/mk_notifikasi_pekerjaan/attachment/attach_informasi_user_login',array('response' => '','page_title' => 'Account Login','data_user_login' => $data_user_login,'other_data' => $other_data));
        }else{
            $output = '<div class="ajax_container content-container col-sm-12 col-md-12"><div class="row data_target1 content"><div class="col-lg-12"><br><p class="alert alert-warning">Data Account Login tidak tersedia.</p><br/></div></div></div>';
        }
        echo $output;
      }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
