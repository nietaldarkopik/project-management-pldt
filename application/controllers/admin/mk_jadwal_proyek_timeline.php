<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_jadwal_proyek_timeline extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
    $this->daftar_kerja();
	}
	
	function daftar_kerja($jadwal_proyek_id = "")
	{
		$this->_config();
    if(!empty($jadwal_proyek_id))
      $this->init['where'] = " jadwal_proyek_id = '".$jadwal_proyek_id."' ";
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));

		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'List Pekerjaan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'List Pekerjaan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_listing',array($this,'_hook_show_panel_allowed'));

		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'List Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'List Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'List Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_jadwal_proyek_timeline_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'List Pekerjaan','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek_timeline",
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jadwal_proyek_id',
                            'label' => 'Induk Pekerjaan',
                            'id' => 'jadwal_proyek_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat("#",dpks.judul_pks,"  -  ",mk.kode_jadwal_proyek," ") label,mk_jadwal_proyek_id value FROM mk_jadwal_proyek mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_jadwal_proyek_id DESC',
                            'options' => array('' => '-----Pilih Induk Pekerjaan-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Kota-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Nama Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          )
                    ),
                    'primary_key' => 'mk_jadwal_proyek_timeline_id',
                    'path' => "/admin/",
                    'controller' => 'mk_jadwal_proyek_timeline',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
    $kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $d = $q->row_array();
    $param['pks_id'] = (isset($d['pks_id']))?$d['pks_id']:$kontrak_id;
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
    $kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $d = $q->row_array();
    $param['pks_id'] = (isset($d['pks_id']))?$d['pks_id']:$kontrak_id;
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah List Pekerjaan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit List Pekerjaan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_kota_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_kota WHERE data_kota_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_kota']) and isset($d['nama_kota']))?'#'.$d['kode_kota'].' - '.$d['nama_kota']:$default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
