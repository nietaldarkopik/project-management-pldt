<?php
class Controllermaker extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
    
  }
  
  function duplicate()
  {
    
      $controllers = array(
        'user_level_role',
        'users',
        'profile',
        'data_propinsi',
        'data_kota',
        'data_kelurahan',
        'data_kecamatan',
        'data_desa',
        'data_koperasi',
        'data_jenis_pekerjaan',
        'data_pekerjaan',
        'data_sumber_pendanaan',
        'data_vendor',
        'data_barang',
        'data_kompetensi',
        'data_pegawai',
        'mk_kontrak',
        'mk_vendor',
        'mk_jadwal_proyek',
        'mk_spesifikasi',
        'mk_harga_biaya',
        'mk_syarat_ketentuan',
        'mk_desa',
        'mk_desa_proyek',
        'mk_pengoperasian',
        'mk_sumber_dana',
        'mk_notifikasi_pekerjaan',
        'mp_progres_status',
        'mp_evaluasi_pelaporan',
        'mp_notifikasi_koordinasi',
        'mp_inspeksi_lapangan',
        'mp_berita_acara',
        'ap_bagan_akun',
        'ap_pencatatan_aset',
        'ap_daftar_aset',
        'ap_aset_serah_terima',
        'ap_kepemilikan',
        'ip_settlement',
        'ip_tagihan',
        'ip_pembayaran',
        'gr_laporan',
        'gr_inquiry',
        'gr_notifikasi'
      );
      
      
      foreach($controllers as $c => $d)
      {
        //open file and get data
        $data = file_get_contents(BASEPATH."../application/controllers/admin/adminblocks.php");

        // do tag replacements or whatever you want
        $data = str_replace("adminblocks", $d, $data);
        $data = str_replace("Adminblocks", ucfirst($d), $data);
        $data = str_replace("ADMINBLOCKS", strtoupper($d), $data);

        //save it back:
        file_put_contents(BASEPATH."../application/controllers/admin/".$d.".php", $data);
      }
    }
}
