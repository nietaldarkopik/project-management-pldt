<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ap_bagan_akun extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		
		$is_login = $this->user_access->is_login();

        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $bagan_akun_list = $this->_show_bagan_akun_list();
        
		if($is_login)
			$this->load->view('layouts/ap_bagan_akun/listing',array('bagan_akun_list' => $bagan_akun_list,'response' => '','page_title' => 'Bagan Akun','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/ap_bagan_akun/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_bagan_akun/edit',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_bagan_akun/add',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_bagan_akun/view',array('response' => '','page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_bagan_akun_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		
		$is_login = $this->user_access->is_login();


        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $bagan_akun_list = $this->_show_bagan_akun_list();
		
		$is_login = $this->user_access->is_login();
        
		if($is_login)
			$this->load->view('layouts/ap_bagan_akun/listing',array('bagan_akun_list' => $bagan_akun_list,'response' => '','page_title' => 'Bagan Akun','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "ap_bagan_akun",
                    'fields' => array(
                                  array(
                                    'name' => 'pks_id',
                                    'label' => 'PKS',
                                    'id' => 'pks_id',
                                    'value' => '',
                                    'type' => 'input_selectbox',
                                    'query' => 'SELECT CONCAT(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                                    'options' => array('0' => '-----Pilih Induk PKS-----'),
                                    'use_search' => true,
                                    'use_listing' => true,
                                    'rules' => ''
                                  ),
                                  array(
                                    'name' => 'parent_bagan_akun',
                                    'label' => 'Induk Akun',
                                    'id' => 'parent_bagan_akun',
                                    'value' => '',
                                    'type' => 'input_selectbox',
                                    'query' => 'SELECT CONCAT(akun," - " ,nama_akun) label,ap_bagan_akun_id value FROM ap_bagan_akun',
                                    'options' => array('0' => '-----Pilih Induk Akun-----'),
                                    'use_search' => true,
                                    'use_listing' => true,
                                    'rules' => ''
                                  ),
                                  array(
                                    'name' => 'akun',
                                    'label' => 'Kode Akun',
                                    'id' => 'akun',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => true,
                                    'use_listing' => true,
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'nama_akun',
                                    'label' => 'Nama Akun',
                                    'id' => 'nama_akun',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => true,
                                    'use_listing' => true,
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'alokasi_laba_rugi',
                                    'label' => 'Alokasi Laba Rugi',
                                    'id' => 'alokasi_laba_rugi',
                                    'value' => '',
                                    'type' => 'input_textarea',
                                    'use_search' => true,
                                    'use_listing' => true,
                                    'rules' => ''
                                  )
                            ),
                            'primary_key' => 'ap_bagan_akun_id',
                            'path' => "/admin/",
                            'controller' => 'ap_bagan_akun',
                            'function' => 'index',
                            'panel_function' => array(
                                                      array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                                      array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                                      array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                                    ),
                            'bulk_options' => array(
                                                      array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                                    )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Akun";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Akun";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _show_bagan_akun_list($parent_bagan_akun = 0)
  {
        $output = "";
        $this->db->where(array("parent_bagan_akun" => $parent_bagan_akun));
        $q = $this->db->get("ap_bagan_akun");
        #$data_rows = $this->data->data_rows;
        $data_rows = $q->result_array();
        $no = 1;
        if(is_array($data_rows) AND COUNT($data_rows) > 0)
        {
          foreach($data_rows as $i => $r)
          {
            ob_start();
            $pks = ($r['pks_id'] == 0)?"":$this->hook->do_action("hook_create_listing_value_pks_id",$r['pks_id']);
      ?>
        <tr>
          <td class="no-print" align="center" valign="middle"><input name="bulk_data[]" value="<?php echo $r['ap_bagan_akun_id'];?>" class="bulk_data" type="checkbox"></td>
          <td style="white-space: nowrap;"><?php echo $pks;?></td>
          <td style="white-space: nowrap;"><?php echo $r['akun'];?></td>
          <td style="white-space: nowrap;"><?php echo $r['nama_akun'];?></td>
          <td style="white-space: nowrap;"><?php echo $r['alokasi_laba_rugi'];?></td>
          <td class="rows_action">
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm">Choose Action</button>
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
                <li role="presentation" class="">
                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/edit/<?php echo $r['ap_bagan_akun_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Edit</a>
                </li>
                <li role="presentation" class="">
                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/view/<?php echo $r['ap_bagan_akun_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> View</a>
                </li>
                <li role="presentation" class="">
                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/delete/<?php echo $r['ap_bagan_akun_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Delete</a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      <?php
          $output .= ob_get_contents();
          $output .= $this->_show_bagan_akun_list($r['ap_bagan_akun_id']);
          ob_end_clean();
          }
        }
     
     return $output;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
