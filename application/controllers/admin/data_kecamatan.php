<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_kecamatan extends Admin_Controller {

  var $init = array();
  var $page_title = "";

  function index()
  {
    $this->load->model('model_data_propinsi');
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID kecamatan
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_kecamatan_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');

    if($is_login)
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Kecamatan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init, 'data_propinsi' => $this->model_data_propinsi->get()));
    else
      $this->load->view('layouts/login');
      
  }
  
  
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();
    $this->load->model('model_data_propinsi');

    //hilangkan ID Kecamatan
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_kecamatan_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Kecamatan', 'data_propinsi' => $this->model_data_propinsi->get()));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();
    $this->load->model('model_data_propinsi');

    //hilangkan ID Kecamatan
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_kecamatan_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Kecamatan', 'data_propinsi' => $this->model_data_propinsi->get()));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Kecamatan'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing()
  {
    $this->load->model('model_data_propinsi');
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_kecamatan_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
        
    $is_login = $this->user_access->is_login();

    //hilangkan ID kecamatan
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_kecamatan_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Kecamatan','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add, 'data_propinsi' => $this->model_data_propinsi->get()));
    else
      $this->load->view('layouts/login');
    
  }

  function _config($id_object = "")
  {
    $init = array(  'table' => 'data_kecamatan',
            'fields' => array(
                           array(
                            'name' => 'data_kecamatan_id',
                            'label' => 'ID Kecamatan',
                            'id' => 'data_kecamatan_id',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'propinsi_id',
                            'label' => 'Propinsi',
                            'id' => 'propinsi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            //'table' => 'data_propinsi',
                            'query' => 'SELECT nama_propinsi as label,data_propinsi_id as value FROM data_propinsi',
                            'options' => array('0' => '-----Pilih Propinsi-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota / Kabupaten',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(dat2,\' \',nama_kota) label,data_kota_id value FROM data_kota',
                            'options' => array('0' => '-----Pilih Kota / Kabupaten-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'primary_key' => 'data_kota_id',
                            'js_connect_to' => array(
                              'table' => 'data_kota',
                              'select' => 'nama_kota label,data_kota_id value',
                              'id_field_parent' => 'propinsi_id',
                              'foreign_key' => 'propinsi_id',
                              'primary_key' => 'data_propinsi_id',
                              'where' => ''
                            ),
                          ),
                          array(
                            'name' => 'kode_kecamatan',
                            'label' => 'Kode Kecamatan',
                            'id' => 'kode_kecamatan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama_kecamatan',
                            'label' => 'Nama Kecamatan',
                            'id' => 'nama_kecamatan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'data_kecamatan',
                    'function' => 'index',
                    'primary_key' => 'data_kecamatan_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );
    $this->init = $init;
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Tambah Kecamatan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Kecamatan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }

  function _hook_create_listing_value_propinsi_id($default_value = "")
  {
    $q = $this->db->query("SELECT * FROM data_propinsi WHERE data_propinsi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nama_propinsi'])) ? $d['nama_propinsi']   : $default_value;

  }

  function _hook_create_listing_value_kota_id($default_value = "")
  {
    $q = $this->db->query("SELECT * FROM data_kota WHERE data_kota_id = '".$default_value."'");
    $d = $q->row_array();
    $dat = ($d['dat2'] == 'Kota') ? 'Kota' : 'Kabupaten';
    return (isset($d['nama_kota'])) ? $dat . ' '. $d['nama_kota']   : $default_value;
  }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
