<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_harga_biaya extends Admin_Controller {

	var $init = array();
	var $config_import = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
    
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/mk_harga_biaya/listing',array('response' => '','page_title' => 'Harga &amp; Total Biaya','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function pdf()
	{
    $this->load->library("pdfwriter");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
    
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
    {
			$html = $this->load->view('layouts/mk_harga_biaya/report',array('response' => '',
                                                              'page_title' => 'Harga &amp; Total Biaya',
                                                              'config_form_add' => $config_form_add,
                                                              'config_form_filter' => $config_form_filter,
                                                              'listing_config' => $this->init),TRUE);

      #echo my_urlsafe_b64encode("admin/mk_beban_anggaran/index");
      #$url = base_url().my_urlsafe_b64decode($url_base64);
      #$html = file_get_contents($url);
      $this->pdfwriter->set_html($html);
      $this->pdfwriter->dompdf();
    }
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_harga_biaya/edit',array('response' => $response,'page_title' => 'Harga &amp; Total Biaya'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_harga_biaya/add',array('response' => $response,'page_title' => 'Harga &amp; Total Biaya'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_harga_biaya/view',array('response' => '','page_title' => 'Harga &amp; Total Biaya'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_harga_biaya_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_harga_biaya/listing',array('response' => '','page_title' => 'Harga &amp; Total Biaya','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(
            'table' => 'mk_harga_total_biaya',
            'query' => "SELECT mk_hb.*,mk_sp.produk,mk_sp.modul_tipe,mk_sp.spesifikasi FROM mk_spesifikasi mk_sp JOIN mk_harga_total_biaya mk_hb ON mk_sp.mk_spesifikasi_id = mk_hb.spesifikasi_id",
						'fields' => array(
                          array(
                            'name' => 'mk_hb.master_kontrak_id',
                            'label' => 'Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'mk_hb.pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'spesifikasi_id',
                            'label' => 'Produk',
                            'id' => 'spesifikasi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(kode_spesifikasi," - ",produk," (",modul_tipe,")") label,mk_spesifikasi_id value FROM mk_spesifikasi ORDER BY mk_spesifikasi_id ASC',
                            'options' => array('' => '-----Pilih Produk-----'),
                            'js_connect_to' => array( 'table' => 'mk_spesifikasi',
                                                      'where' => '',
                                                      'select' => 'produk label,mk_spesifikasi_id value',
                                                      'primary_key' => 'mk_spesifikasi_id',
                                                      'foreign_key' => 'master_kontrak_id',
                                                      'id_field_parent' => 'master_kontrak_id'
                                                      ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'modul_tipe',
                            'label' => 'Module/Tipe',
                            'id' => 'modul_tipe',
                            'value' => '',
                            'type' => 'input_hidden',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'spesifikasi',
                            'label' => 'Spesifikasi',
                            'id' => 'spesifikasi',
                            'value' => '',
                            'type' => 'input_hidden',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'volume',
                            'label' => 'volume',
                            'id' => 'volume',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'import',
                            'label' => 'Jenis Pengadaan',
                            'id' => 'id_import',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'options' => array('import' => 'Import','dalam negeri' => 'Dalam Negeri'),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'mata_uang',
                            'label' => 'Mata Uang',
                            'id' => 'mata_uang',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'options' => array("IDR" => "IDR","USD" => "USD"),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_satuan',
                            'label' => 'Harga Satuan',
                            'id' => 'harga_satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_fob',
                            'label' => 'Harga FOB',
                            'id' => 'harga_fob',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'freight',
                            'label' => 'Freight',
                            'id' => 'freight',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'exchange_rate',
                            'label' => 'Exchange Rate',
                            'id' => 'exchange_rate',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'bea_masuk',
                            'label' => 'Bea Masuk',
                            'id' => 'bea_masuk',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'ppn',
                            'label' => 'PPN',
                            'id' => 'ppn',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'pph',
                            'label' => 'PPH',
                            'id' => 'pph',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'total',
                            'label' => 'Total',
                            'id' => 'total',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          )
                    ),
                    'primary_key' => 'mk_harga_total_biaya_id',
                    'path' => "/admin/",
                    'controller' => 'mk_harga_biaya',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_before_insert_mk_harga_total_biaya',array($this,'_hook_importer_do_insert_data_before_insert_mk_harga_total_biaya'));
    $fields_insert = 
                    array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Nomor Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'spesifikasi_id',
                            'label' => 'Kode Spesifikasi',
                            'id' => 'spesifikasi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(kode_spesifikasi," - ",produk," (",modul_tipe,")") label,mk_spesifikasi_id value FROM mk_spesifikasi ORDER BY mk_spesifikasi_id ASC',
                            'options' => array('' => '-----Pilih Produk-----'),
                            'js_connect_to' => array( 'table' => 'mk_spesifikasi',
                                                      'where' => '',
                                                      'select' => 'produk label,mk_spesifikasi_id value',
                                                      'primary_key' => 'mk_spesifikasi_id',
                                                      'foreign_key' => 'master_kontrak_id',
                                                      'id_field_parent' => 'master_kontrak_id'
                                                      ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'volume',
                            'label' => 'volume',
                            'id' => 'volume',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'import',
                            'label' => 'Jenis Pengadaan (import/dalam negeri)',
                            'id' => 'import',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'options' => array('import' => 'Import','dalam negeri' => 'Dalam Negeri'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'mata_uang',
                            'label' => 'Mata Uang (IDR/USD)',
                            'id' => 'mata_uang',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'options' => array("IDR" => "IDR","USD" => "USD"),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_satuan',
                            'label' => 'Harga Satuan',
                            'id' => 'harga_satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_fob',
                            'label' => 'Harga FOB',
                            'id' => 'harga_fob',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'freight',
                            'label' => 'Freight',
                            'id' => 'freight',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'exchange_rate',
                            'label' => 'Exchange Rate',
                            'id' => 'exchange_rate',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'bea_masuk',
                            'label' => 'Bea Masuk',
                            'id' => 'bea_masuk',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'ppn',
                            'label' => 'PPN',
                            'id' => 'ppn',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'pph',
                            'label' => 'PPH',
                            'id' => 'pph',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'total',
                            'label' => 'Total',
                            'id' => 'total',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          )
                    );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_harga_total_biaya',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mk_harga_total_biaya_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_harga_total_biaya/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;

		if(isset($param['modul_tipe']))
		  unset($param['modul_tipe']);
		if(isset($param['spesifikasi']))
		  unset($param['spesifikasi']);
			return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;

		if(isset($param['modul_tipe']))
		  unset($param['modul_tipe']);
		if(isset($param['spesifikasi']))
		  unset($param['spesifikasi']);
   
    return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Harga dan Biaya";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Harga dan Biaya";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_spesifikasi_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE mk_spesifikasi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['produk']))?$d['kode_spesifikasi'].'-'.$d['produk'].'-'.$d['modul_tipe']:$default_value;
  }
  
  function _hook_importer_do_insert_data_before_insert_mk_harga_total_biaya($param = ""){
		$nomor_kontrak = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE nomor_kontrak = '".$nomor_kontrak."'");
		$kontrak = $q->row_array();
		
		$kode_spesifikasi = (isset($param['spesifikasi_id']))?$param['spesifikasi_id']:"";
		$q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE kode_spesifikasi = '".$kode_spesifikasi."'");
		$spesifikasi = $q->row_array();
        
		$param['spesifikasi_id'] = (isset($spesifikasi['mk_spesifikasi_id']))?$spesifikasi['mk_spesifikasi_id']:0;
		$param['master_kontrak_id'] = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		return $param;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
