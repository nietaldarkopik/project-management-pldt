<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ip_pembayaran extends Admin_Controller {

	var $init = array();
	var $init_detail_invoice = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));

		$is_login = $this->user_access->is_login();

        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        
		if($is_login)
			$this->load->view('layouts/ip_pembayaran/listing',array('response' => '','page_title' => 'Payment','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
    $this->init['fields'][0]['type'] = 'input_view';
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_form_field_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_field_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_field_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_field_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/edit',array('response' => $response,'page_title' => 'Payment'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function berita_acara($object_id = "")
	{
        $this->db->where(array("mp_asset_berita_acara_id" => $object_id));
        $q_get_berita_acara = $this->db->get("mp_asset_berita_acara");
        $data_berita_acara = $q_get_berita_acara->row_array();
        $response = "";
        $status_action = "";
            
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/detail_berita_acara',array('response' => $response,'page_title' => 'Berita Acara Uji Terima','data_berita_acara' => $data_berita_acara,'mp_asset_berita_acara_id' => $object_id));
		else
			$this->load->view('layouts/login');
		
	}
    
	function serah_terima($object_id = "")
	{
        $this->db->where(array("mp_inspeksi_berita_acara_id" => $object_id));
        $q_get_berita_acara = $this->db->get("mp_inspeksi_berita_acara");
        $data_berita_acara = $q_get_berita_acara->row_array();
        $response = "";
        $status_action = "";
            
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/detail_serah_terima',array('response' => $response,'page_title' => 'Berita Acara Serah Terima','data_berita_acara' => $data_berita_acara,'mp_inspeksi_berita_acara_id' => $object_id));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/add',array('response' => $response,'page_title' => 'Payment'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function detail_invoice()
	{
		$this->_config_detail_invoice();
		$this->data->init($this->init_detail_invoice);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add_detail_invoice'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add_detail_invoice'));
		#$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init_detail_invoice['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/add_detail_invoice',array('response' => $response,'page_title' => 'Payment'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
        $this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/view',array('response' => '','page_title' => 'Payment'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ip_pembayaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$is_login = $this->user_access->is_login();

        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ip_pembayaran/listing',array('response' => '','page_title' => 'Payment','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(
            'table' => 'ip_payment',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'invoice_id',
                            'label' => 'Invoice',
                            'id' => 'invoice_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(ip_invoice.no_invoice) label,ip_invoice_id value FROM ip_invoice',
                            'options' => array('' => '-----Pilih Invoice-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_pembayaran',
                            'label' => 'Tanggal Pembayaran',
                            'id' => 'tanggal_pembayaran',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'no_rekening',
                            'label' => 'No. Rekening Bank',
                            'id' => 'no_rekening',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama_bank',
                            'label' => 'Nama Bank',
                            'id' => 'nama_bank',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'alamat_cabang',
                            'label' => 'Alamat Kantor Cabang',
                            'id' => 'alamat_cabang',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama_cabang',
                            'label' => 'Cabang',
                            'id' => 'nama_cabang',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama_rekening',
                            'label' => 'Atas Nama',
                            'id' => 'nama_rekening_bank',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          )
                          
                    ),
                    'primary_key' => 'ip_payment_id',
                    'path' => "/admin/",
                    'controller' => 'ip_pembayaran',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_detail_invoice($id_object = "")
	{
        echo $id_object;
        $data_ip_invoice = $this->db->query("SELECT * FROM ip_invoice WHERE ip_invoice_id = '".$id_object."' ")->row_array();
        $master_kontrak_id = (isset($data_ip_invoice['master_kontrak_id']))?$data_ip_invoice['master_kontrak_id']:"";
        $init = array(
                'table' => 'ip_invoice_detail',
						'fields' => array(
                          array(
                            'name' => 'invoice_id',
                            'label' => 'Invoice ID',
                            'id' => 'invoice_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            #'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            #'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),/*
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa/Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_autocomplete',
                            #'query' => 'SELECT chain_geography.*,data_propinsi.nama_propinsi,data_kota.dat2,data_kota.nama_kota,data_kecamatan.nama_kecamatan,data_desa.nama_desa FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            #'query' => 'SELECT concat(data_desa.nama_desa,",data_kecamatan.nama_kecamatan,",",data_kota.dat2," ",data_kota.nama_kota,",",",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            'service_get_json' => array(
                              'table' => 'chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi',
                              'where' => 'chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id',
                              'select' => 'concat(data_desa.nama_desa,", Kec. ",data_kecamatan.nama_kecamatan,", ",data_kota.dat2," ",data_kota.nama_kota,", Prop. ",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value',
                              'foreign_key' => 'data_desa.data_desa_id'
                            ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),*/
                          array(
                            'name' => 'cakupan_kerja_id',
                            'label' => 'Cakupan Pekerjaan',
                            'id' => 'cakupan_kerja_id',
                            'value' => '',
                            'type' => 'input_autocomplete',
                            #'query' => 'SELECT chain_geography.*,data_propinsi.nama_propinsi,data_kota.dat2,data_kota.nama_kota,data_kecamatan.nama_kecamatan,data_desa.nama_desa FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            #'query' => 'SELECT concat(data_desa.nama_desa,",data_kecamatan.nama_kecamatan,",",data_kota.dat2," ",data_kota.nama_kota,",",",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            'service_get_json' => array(
                              'table' => 'mk_cakupan_kerja,mk_spesifikasi',
                              'where' => 'mk_cakupan_kerja.spesifikasi_id = mk_spesifikasi.mk_spesifikasi_id AND master_kontrak_id = "'.$master_kontrak_id."'",
                              'select' => 'concat(mk_spesifikasi.produk," - ",mk_spesifikasi.modul_tipe) label,mk_cakupan_kerja.mk_cakupan_kerja_id value',
                              'foreign_key' => 'mk_cakupan_kerja.mk_cakupan_kerja_id'
                            ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'volume', 
                            'label' => 'Volume',
                            'id' => 'volume',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_perunit',
                            'label' => 'Harga Perunit',
                            'id' => 'harga_perunit',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          )                          
                    ),
                    'primary_key' => 'ip_invoice_detail_id',
                    'path' => "/admin/",
                    'controller' => 'ip_pembayaran',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init_detail_invoice = $init;
	}
	
	function _hook_do_add($param = "")
	{
        /*
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		*/
        return $param;
	}
	
	function _hook_do_add_detail_invoice($param = "")
	{
        return $param;
	}
	
	function _hook_do_edit($param = "")
	{
        return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Payment";
  }
  
  function _hook_create_form_title_add_detail_invoice($title){
    return "Tambah Data Produk/Jasa";
  }
  
  function _hook_create_form_title_edit_detail_invoice($title){
    return "Edit Data Produk/Jasa";
  }
  function _hook_create_form_title_edit($title){
    return "Edit Data Payment";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_spesifikasi_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE mk_spesifikasi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['produk']))?$d['kode_spesifikasi'].'-'.$d['produk'].'-'.$d['modul_tipe']:$default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
