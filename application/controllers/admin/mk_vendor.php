<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_vendor extends Admin_Controller {

	var $init = array();
	var $config_import = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Master Vendor','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_listing',array($this,'_hook_show_panel_allowed'));

		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Master Vendor'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_do_after_add',array($this,'_hook_do_after_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
    
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Master Vendor'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_listing',array($this,'_hook_show_panel_allowed'));		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Master Vendor'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_vendor_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));

		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Master Vendor','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "mk_master_vendor",
						'fields' => array(
                        /*
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          */
                          array(
                            'name' => 'kode_vendor',
                            'label' => 'Kode Vendor',
                            'id' => 'kode_vendor',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama',
                            'label' => 'Nama Lengkap',
                            'id' => 'nama',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'no_identitas',
                            'label' => 'No Identitas/KTP',
                            'id' => 'no_identitas',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tempat_lahir',
                            'label' => 'Tempat Lahir',
                            'id' => 'tempat_lahir',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_lahir',
                            'label' => 'Tanggal Lahir',
                            'id' => 'tanggal_lahir',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => true,
                            'use_listing' => true,
                            'placeholder' => 'YYYY-MM-DD',
                            'rules' => ''
                          ),
                          array(
                            'name' => 'alamat',
                            'label' => 'Alamat Lengkap',
                            'id' => 'alamat',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'no_telepon',
                            'label' => 'No. Telepon',
                            'id' => 'no_telepon',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'email',
                            'label' => 'Email',
                            'id' => 'email',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required|unique['.TABLE_USERS.'.username]'
                          ),
                          array(
                            'name' => 'jabatan',
                            'label' => 'Jabatan',
                            'id' => 'jabatan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'perusahaan',
                            'label' => 'Nama Perusahaan/Instansi',
                            'id' => 'perusahaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'akta_pendirian',
                            'label' => 'Akta Pendirian',
                            'id' => 'akta_pendirian',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'alamat_perusahaan',
                            'label' => 'Alamat Perusahaan/Instansi',
                            'id' => 'alamat_perusahaan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          )
                    ),
                    'primary_key' => 'mk_master_vendor_id',
                    'path' => "/admin/",
                    'controller' => 'mk_vendor',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_after_insert_mk_master_vendor',array($this,'_hook_importer_do_insert_data_after_insert_mk_master_vendor'));
    $fields_insert = 
                    array(
                        /*
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          */
                          array(
                            'name' => 'kode_vendor',
                            'label' => 'Kode Vendor',
                            'id' => 'kode_vendor',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'nama',
                            'label' => 'Nama Lengkap',
                            'id' => 'nama',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'no_identitas',
                            'label' => 'No Identitas/KTP',
                            'id' => 'no_identitas',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tempat_lahir',
                            'label' => 'Tempat Lahir',
                            'id' => 'tempat_lahir',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_lahir',
                            'label' => 'Tanggal Lahir',
                            'id' => 'tanggal_lahir',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => true,
                            'use_listing' => true,
                            'placeholder' => 'YYYY-MM-DD',
                            'rules' => ''
                          ),
                          array(
                            'name' => 'alamat',
                            'label' => 'Alamat Lengkap',
                            'id' => 'alamat',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'no_telepon',
                            'label' => 'No. Telepon',
                            'id' => 'no_telepon',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'email',
                            'label' => 'Email',
                            'id' => 'email',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required|unique['.TABLE_USERS.'.username]'
                          ),
                          array(
                            'name' => 'jabatan',
                            'label' => 'Jabatan',
                            'id' => 'jabatan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'perusahaan',
                            'label' => 'Nama Perusahaan/Instansi',
                            'id' => 'perusahaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'akta_pendirian',
                            'label' => 'Akta Pendirian',
                            'id' => 'akta_pendirian',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'alamat_perusahaan',
                            'label' => 'Alamat Perusahaan/Instansi',
                            'id' => 'alamat_perusahaan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          )
                   );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_master_vendor',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mk_master_vendor_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_master_vendor/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$d = $q->row_array();
		$param['pks_id'] = (isset($d['pks_id']))?$d['pks_id']:$kontrak_id;
		return $param;
	}
	
	function _hook_do_after_add($param = "")
	{
        $this->db->where(array("mk_master_vendor_id" => $param));
        $data_inserted = $this->db->get("mk_master_vendor")->row_array();
        
        if(isset($data_inserted['kode_vendor']))
        {
          $this->load->helper('string');
          
          $this->db->where(array("username" => $data_inserted['kode_vendor']));
          $qulevel = $this->db->get("user_accounts");
          if($qulevel->num_rows() == 0)
          {
            $password = strtolower(random_string('alnum',8));
            $data_insert = array(
                                    "username" => $data_inserted['kode_vendor'],
                                    "password" => md5($password),
                                    "email" => $data_inserted['email'],
                                    "user_level_id" => 7,
                                    "active_status" => "active",
                                    "date_inserted" => date("Y-m-d H:i:s")
                                 );
            $qulevel = $this->db->insert("user_accounts",$data_insert);
            $last_user_id = $this->db->insert_id();
            
            $data_insert_pass = array("user_id" => $last_user_id,"password" => $password);
            $qulevel = $this->db->insert("user_pass",$data_insert_pass);
          }
        }
		return $param;
	}
    
	function _hook_do_edit($param = "")
	{
    $kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $d = $q->row_array();
    $param['pks_id'] = (isset($d['pks_id']))?$d['pks_id']:$kontrak_id;
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
    $this->db->where(array("mk_master_vendor_id" => $param->primary_key_value));
    $c_master_vendor = $this->db->get("mk_master_vendor");
    $c_master_vendor = $c_master_vendor->row_array();
    if(isset($c_master_vendor['kode_vendor']))
    {
      $this->db->where(array("username" => $c_master_vendor['kode_vendor']));
      $qulevel = $this->db->get("user_accounts");
      if($qulevel->num_rows() > 0)
      {
        $user = $qulevel->row_array();
        $this->db->where(array("user_id" => $user["user_id"]));
        $this->db->delete("user_pass");
        $this->db->where(array("user_id" => $user["user_id"]));
        $this->db->delete("user_accounts");
      }
    }
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Master Vendor";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Master Vendor";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_importer_do_insert_data_after_insert_mk_master_vendor($data_inserted_tbl = array())
  {
    $data_inserted = (isset($data_inserted_tbl['mk_master_vendor']))?$data_inserted_tbl['mk_master_vendor']:array();
    if(isset($data_inserted['kode_vendor']))
    {
      $this->load->helper('string');
      
      $this->db->where(array("username" => $data_inserted['kode_vendor']));
      $qulevel = $this->db->get("user_accounts");
      if($qulevel->num_rows() == 0)
      {
        $password = strtolower(random_string('alnum',8));
        $data_insert = array(
                                "username" => $data_inserted['kode_vendor'],
                                "password" => md5($password),
                                "email" => $data_inserted['email'],
                                "user_level_id" => 7,
                                "active_status" => "active",
                                "date_inserted" => date("Y-m-d H:i:s")
                             );
        $qulevel = $this->db->insert("user_accounts",$data_insert);
        $last_user_id = $this->db->insert_id();
        
        $data_insert_pass = array("user_id" => $last_user_id,"password" => $password);
        $qulevel = $this->db->insert("user_pass",$data_insert_pass);
      }
    }
  }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
