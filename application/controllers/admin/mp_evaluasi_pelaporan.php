<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mp_evaluasi_pelaporan extends Admin_Controller {

	var $init = array();
	var $init_timeline = array();
	var $init_update_progress = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->_config_timeline();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
   
    /* 
    $js = '<script src="'.current_admin_theme_url().'static/js/ganttchart/codebase/dhtmlxgantt.js"></script>';
    $js .= '<script type="text/javascript">
                var demo_tasks = {
                                  "data":[
                                    {"id":11, "text":"Project #1", "start_date":"28-03-2013", "end_date":"28-03-2013", "duration":"11", "progress": 0.6, "open": true},
                                    {"id":1, "text":"Project #2", "start_date":"01-04-2013", "end_date":"01-06-2013", "duration":"18", "progress": 0.4, "open": true},

                                    {"id":2, "text":"Task #1", "start_date":"02-04-2013", "end_date":"02-06-2013", "duration":"8", "parent":"1", "progress":0.5, "open": true},
                                    {"id":3, "text":"Task #2", "start_date":"11-04-2013", "end_date":"11-06-2013", "duration":"8", "parent":"1", "progress": 0.6, "open": true},
                                    {"id":4, "text":"Task #3", "start_date":"13-04-2013", "end_date":"13-06-2013", "duration":"6", "parent":"1", "progress": 0.5, "open": true},
                                    {"id":5, "text":"Task #1.1", "start_date":"02-04-2013", "end_date":"02-06-2013", "duration":"7", "parent":"2", "progress": 0.6, "open": true},
                                    {"id":6, "text":"Task #1.2", "start_date":"03-04-2013", "end_date":"03-06-2013", "duration":"7", "parent":"2", "progress": 0.6, "open": true},
                                    {"id":7, "text":"Task #2.1", "start_date":"11-04-2013", "end_date":"11-06-2013", "duration":"8", "parent":"3", "progress": 0.6, "open": true},
                                    {"id":8, "text":"Task #3.1", "start_date":"14-04-2013", "end_date":"14-06-2013", "duration":"5", "parent":"4", "progress": 0.5, "open": true},
                                    {"id":9, "text":"Task #3.2", "start_date":"14-04-2013", "end_date":"14-06-2013", "duration":"4", "parent":"4", "progress": 0.5, "open": true},
                                    {"id":10, "text":"Task #3.3", "start_date":"14-04-2013", "end_date":"14-06-2013", "duration":"3", "parent":"4", "progress": 0.5, "open": true},
                                    
                                    {"id":12, "text":"Task #1", "start_date":"03-04-2013", "end_date":"03-06-2013", "duration":"5", "parent":"11", "progress": 1, "open": true},
                                    {"id":13, "text":"Task #2", "start_date":"02-04-2013", "end_date":"02-06-2013", "duration":"7", "parent":"11", "progress": 0.5, "open": true},
                                    {"id":14, "text":"Task #3", "start_date":"02-04-2014", "end_date":"02-06-2014", "duration":"6", "parent":"11", "progress": 0.8, "open": true},
                                    {"id":15, "text":"Task #4", "start_date":"02-04-2014", "end_date":"02-06-2014", "duration":"5", "parent":"11", "progress": 0.2, "open": true},
                                    {"id":16, "text":"Task #5", "start_date":"02-04-2014", "end_date":"02-06-2014", "duration":"7", "parent":"11", "progress": 0, "open": true},

                                    {"id":17, "text":"Task #2.1", "start_date":"03-05-2013", "end_date":"03-06-2013", "duration":"2", "parent":"13", "progress": 1, "open": true},
                                    {"id":18, "text":"Task #2.2", "start_date":"06-05-2013", "end_date":"06-06-2013", "duration":"3", "parent":"13", "progress": 0.8, "open": true},
                                    {"id":19, "text":"Task #2.3", "start_date":"10-05-2013", "end_date":"10-06-2013", "duration":"4", "parent":"13", "progress": 0.2, "open": true},
                                    {"id":20, "text":"Task #2.4", "start_date":"10-05-2013", "end_date":"10-06-2013", "duration":"4", "parent":"13", "progress": 0, "open": true},
                                    {"id":21, "text":"Task #4.1", "start_date":"03-05-2013", "end_date":"03-06-2013", "duration":"4", "parent":"15", "progress": 0.5, "open": true},
                                    {"id":22, "text":"Task #4.2", "start_date":"03-05-2013", "end_date":"03-06-2013", "duration":"4", "parent":"15", "progress": 0.1, "open": true},
                                    {"id":23, "text":"Task #4.3", "start_date":"03-05-2013", "end_date":"03-06-2013", "duration":"5", "parent":"15", "progress": 0, "open": true}
                                  ],
                                  "links":[
                                    {"id":"1","source":"1","target":"2","type":"1"},
                                    {"id":"2","source":"2","target":"3","type":"0"},
                                    {"id":"3","source":"3","target":"4","type":"0"},
                                    {"id":"4","source":"2","target":"5","type":"2"},
                                    {"id":"5","source":"2","target":"6","type":"2"},
                                    {"id":"6","source":"3","target":"7","type":"2"},
                                    {"id":"7","source":"4","target":"8","type":"2"},
                                    {"id":"8","source":"4","target":"9","type":"2"},
                                    {"id":"9","source":"4","target":"10","type":"2"},
                                    {"id":"10","source":"11","target":"12","type":"1"},
                                    {"id":"11","source":"11","target":"13","type":"1"},
                                    {"id":"12","source":"11","target":"14","type":"1"},
                                    {"id":"13","source":"11","target":"15","type":"1"},
                                    {"id":"14","source":"11","target":"16","type":"1"},
                                    {"id":"15","source":"13","target":"17","type":"1"},
                                    {"id":"16","source":"17","target":"18","type":"0"},
                                    {"id":"17","source":"18","target":"19","type":"0"},
                                    {"id":"18","source":"19","target":"20","type":"0"},
                                    {"id":"19","source":"15","target":"21","type":"2"},
                                    {"id":"20","source":"15","target":"22","type":"2"},
                                    {"id":"21","source":"15","target":"23","type":"2"}
                                  ]
                                };
              gantt.config.columns = [
                  {name:"text",       label:"Nama Pekerjaan",  width:300, tree:true, resize: true },
                  {name:"start_date", label:"Tanggal Mulai",  width:100, align: "center", resize: true  },
                  {name:"end_date",   label:"Tanggal Mulai",  width:100, align: "center", resize: true  },
                  {name:"duration",   label:"Lama Pekerjaan",  width:100,   align: "center", resize: true  },
                  {name: "planned_start", label: "Planned start",  width:100, hide:false, resize: true   },
                  {name: "planned_end", label: "Planned end",  width:100, hide:false, resize: true   }
              ];
              gantt.config.grid_resize = true;
              //gantt.config.min_grid_column_width = 100;
              gantt.config.scale_unit = "year";
              gantt.config.step = 1;
              //gantt.config.date_scale = "%F, %Y";
              gantt.config.date_scale = "%Y";
              //gantt.config.min_column_width = 100;
              gantt.config.autosize = true;
              gantt.config.autofit = false;
              gantt.config.grid_width = 800;
              gantt.config.drag_move = false;
              gantt.config.drag_progress = false;
              gantt.config.drag_resize = false;
              gantt.config.drag_links = false;
              gantt.config.fit_tasks = "y";

              gantt.config.scale_height = 90;

              var weekScaleTemplate = function(date){
                var dateToStr = gantt.date.date_to_str("%d %M");
                var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                return dateToStr(date) + " - " + dateToStr(endDate);
              };

              gantt.config.subscales = [
                / *{unit:"week", step:1, date:"#%W" ,template:weekScaleTemplate}, * /
                {unit:"month", step:1, date:"%M"},
                {unit:"week", step:1, date:"#%W"}
              ];
              gantt.attachEvent("onGanttReady", function(){
                  $(".gantt_grid").resizable({
                    handles: "e",
                    maxWidth: 800,
                    stop: function(e,u){
                      var originalSize = u.originalSize;
                      var currentsize = u.size;
                      var diffsize = originalSize.width - currentsize.width;
                      $(".gantt_task").width(function(i,w){
                        return w + diffsize;
                      });
                    }
                    //alsoResize: ".gantt_grid_head_cell,.gantt_grid_scale,.gantt_cell,.gantt_grid_data,.gantt_row"
                  });
              });
              gantt.init("gantt_here");
              gantt.parse(demo_tasks);
              gantt.setSizes();
            </script>';
    $css = '<link rel="stylesheet" href="'.current_admin_theme_url().'static/js/ganttchart/codebase/skins/dhtmlxgantt_broadway.css"/>';
    
    $this->assets->add_js($js,"body");
    $this->assets->add_css($css,"head");
    */
    $listing = $this->_create_listing($this->init);
		if($is_login)
    {
			$this->load->view('layouts/mp_evaluasi_pelaporan/listing',array('listing' => $listing,'response' => '','page_title' => 'Evaluasi &amp; Pelaporan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
			#$this->load->view('layouts/mp_evaluasi_pelaporan/ganttchart',array('listing' => $listing,'response' => '','page_title' => 'Evaluasi Progres','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    }
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
    if(isset($this->init['where']))
    unset($this->init['where']);
    
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_listing',array($this,'_hook_show_panel_allowed'));
    
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Evaluasi Progres'));
		else
			$this->load->view('layouts/login');
		
	}

	function edit_progress($object_id = "")
	{
		$this->_config_update_progress($object_id);
    if(isset($this->init_update_progress['where']))
      unset($this->init_update_progress['where']);
    
    $this->init_update_progress['primary_key'] = 'jadwal_proyek_id';
    $this->init = $this->init_update_progress;
		$this->data->init($this->init_update_progress);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->data->primary_key = 'jadwal_proyek_id';
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit_progress'));
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_edit_progress'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_update_progress'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_listing',array($this,'_hook_show_panel_allowed'));
    
    $check_realisasi = $this->_get_realisasi_value($object_id);
    $response = "";
    if(is_array($check_realisasi) and count($check_realisasi) > 0)
    {
      $response = $this->data->edit("",$this->init_update_progress['fields']);
    }else{
      $response = $this->data->add("",$this->init_update_progress['fields']);
    }
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Evaluasi Progres'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Evaluasi Progres'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Evaluasi Progres'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function _daftar_kerja($object_id = "")
	{
    $url = base_url().$this->uri->segment(1) . "/" . "mp_evaluasi_pelaporan_timeline/daftar_kerja/".$object_id;
    echo '<script>window.location.href="'.$url.'";</script>';
    exit;
    #header("location: ".base_url().$this->uri->segment(1) . "/" . "mp_evaluasi_pelaporan_timeline/daftar_kerja/" . $object_id);
    #exit;
	}
	
	function daftar_kerja($object_id = "")
	{
		$this->_config_timeline();
		$this->data->init($this->init_timeline);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add_timeline'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add_timeline'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mp_evaluasi_pelaporan/add_timeline',array('response' => $response,'page_title' => 'Jadwal Sub Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function edit_timeline($object_id = "")
	{
		$this->_config_timeline();
    if(isset($this->init_timeline['where']))
    unset($this->init_timeline['where']);
    $this->init = $this->init_timeline;
		$this->data->init($this->init_timeline);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit_timeline'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mp_evaluasi_pelaporan_listing',array($this,'_hook_show_panel_allowed'));
		
		$response = $this->data->edit("",$this->init_timeline);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mp_evaluasi_pelaporan/add_timeline',array('response' => $response,'page_title' => 'Evaluasi Progres'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function listing($param = "")
	{
    $this->index($param = "");
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek",
            'where' => ' AND type_jadwal = "realisasi" ',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Pelaksanaan Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_hidden',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(nama,"(",perusahaan,")") label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Kota-----'),
                            'use_search' => false,
                            'use_listing' => false,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Lokasi Proyek',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          )
                    ),
                    'where' => ' induk_jadwal_proyek_id = "0" AND type_jadwal = "realisasi" ',
                    'primary_key' => 'mk_jadwal_proyek_id',
                    'path' => "/admin/",
                    'controller' => 'mp_evaluasi_pelaporan',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Evaluasi','name' => 'edit_progress', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Edit','name' => 'edit_timeline', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_timeline($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek",
            'where' => ' AND type_jadwal = "realisasi" ',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Pelaksanaan Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(nama,"(",perusahaan,")") label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Kota-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          )
                    ),
                    'primary_key' => 'mk_jadwal_proyek_id',
                    'path' => "/admin/",
                    'controller' => 'mp_evaluasi_pelaporan',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Update Progress','name' => 'edit_progress', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Edit','name' => 'edit_timeline', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init_timeline = $init;
	}
	
	function _config_update_progress($id_object = "")
	{
    $init = array(	'table' => "mk_jadwal_proyek",
            'where' => ' AND type_jadwal = "realisasi" ',
						'fields' => array(
                          array(
                            'name' => 'jadwal_proyek_id',
                            'label' => 'Evaluasi Progres',
                            'id' => 'jadwal_proyek_id',
                            'value' => $id_object,
                            'type' => 'input_hidden',
                            'use_search' => false,
                            'use_listing' => false,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'nama_pekerjaan',
                            'label' => 'Pelaksanaan Pekerjaan',
                            'id' => 'nama_pekerjaan',
                            'value' => '',
                            'type' => 'input_hidden',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_vendor," - ",perusahaan) label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT nama_kota label,data_kota_id value FROM data_kota',
                            'options' => array('' => '-----Pilih Kota-----'),
                            'use_search' => false,
                            'use_listing' => false,
                            'rules' => '',
                            'class' => 'hidden',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_desa',
                            'label' => 'Jumlah Desa',
                            'id' => 'jumlah_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'qty',
                            'label' => 'Vol',
                            'id' => 'qty',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'tanggal_mulai',
                            'label' => 'Tanggal Mulai',
                            'id' => 'tanggal_mulai',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_berakhir',
                            'label' => 'Tanggal Berakhir',
                            'id' => 'tanggal_berakhir',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'tanggal_jatuh_tempo',
                            'label' => 'Tanggal Jatuh Tempo',
                            'id' => 'tanggal_jatuh_tempo',
                            'value' => '',
                            'type' => 'input_date',
                            'placeholder' => "YYYY-MM-DD",
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'jumlah_hari_tempuh',
                            'label' => 'Jumlah Hari Tempuh',
                            'id' => 'jumlah_hari_tempuh',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap"'
                          ),
                          array(
                            'name' => 'keterangan',
                            'label' => 'Keterangan',
                            'id' => 'keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => '',
                            'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                          )
                    ),
                    'primary_key' => 'jadwal_proyek_id',
                    'path' => "/admin/",
                    'controller' => 'mp_evaluasi_pelaporan',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Update Progress','name' => 'edit_progress', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Edit','name' => 'edit_timeline', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init_update_progress = $init;
	}
	
	function _hook_do_add($param = "")
	{
    $kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $pks = $q->row_array();
    
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
    $vendor = $q->row_array();
    $param['master_vendor_id'] = (isset($vendor['mk_master_vendor_id']))?$vendor['mk_master_vendor_id']:0;
    $param['pks_id'] = (isset($pks['pks_id']))?$pks['pks_id']:0;
		return $param;
	}
	
	function _hook_do_add_timeline($param = "")
	{
    $induk_jadwal = $this->uri->segment(4);
    $q_induk_jadwal = $this->db->query("SELECT * FROM mp_evaluasi_pelaporan WHERE mp_evaluasi_pelaporan_id = '".$induk_jadwal."'");
    $d_induk_jadwal = $q_induk_jadwal->row_array();
    
    $kontrak_id = (isset($d_induk_jadwal['master_kontrak_id']))?$d_induk_jadwal['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $pks = $q->row_array();
    
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
    $vendor = $q->row_array();
    $param['master_kontrak_id'] = (isset($vendor['master_kontrak_id']))?$vendor['master_kontrak_id']:0;
    $param['master_vendor_id'] = (isset($vendor['mk_master_vendor_id']))?$vendor['mk_master_vendor_id']:0;
    $param['pks_id'] = (isset($pks['pks_id']))?$pks['pks_id']:0;
    $param['induk_progres_status_id'] = $induk_jadwal;
    $param['kota_id'] = (isset($d_induk_jadwal['kota_id']))?$d_induk_jadwal['kota_id']:0;
		return $param;
	}
  
	function _hook_do_edit($param = "")
	{
    $kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $pks = $q->row_array();
    
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
    $vendor = $q->row_array();
    $param['master_vendor_id'] = (isset($vendor['mk_master_vendor_id']))?$vendor['mk_master_vendor_id']:0;
    $param['pks_id'] = (isset($pks['pks_id']))?$pks['pks_id']:0;
    return $param;
	}
  
	function _hook_do_edit_progress($param = "")
	{
    $jadwal_id = $this->data->primary_key_value;
    $data_jadwal = $this->_get_jadwal_value($jadwal_id);
    $kontrak_id = (isset($data_jadwal['master_kontrak_id']))?$data_jadwal['master_kontrak_id']:"0";
    $vendor_id = (isset($data_jadwal['master_vendor_id']))?$data_jadwal['master_vendor_id']:"0";
    $pks_id = (isset($data_jadwal['pks_id']))?$data_jadwal['pks_id']:"0";
    $kota_id = (isset($data_jadwal['kota_id']))?$data_jadwal['kota_id']:"0";
    $param['master_kontrak_id'] = $kontrak_id;
    $param['master_vendor_id'] = $vendor_id;
    $param['pks_id'] = $pks_id;
    $param['kota_id'] = $kota_id;
    $param['jadwal_proyek_id'] = $jadwal_id;
    $param['type_jadwal'] = 'realisasi';
    return $param;
	}
	
	function _hook_do_edit_timeline($param = "")
	{
    /*
    $induk_jadwal = $this->uri->segment(4);
    $q_induk_jadwal = $this->db->query("SELECT * FROM mp_evaluasi_pelaporan WHERE mp_evaluasi_pelaporan_id = '".$induk_jadwal."'");
    $d_induk_jadwal = $q_induk_jadwal->row_array();
    
    $kontrak_id = (isset($d_induk_jadwal['master_kontrak_id']))?$d_induk_jadwal['master_kontrak_id']:"";
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
    $pks = $q->row_array();
    
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
    $vendor = $q->row_array();
    $param['master_kontrak_id'] = (isset($vendor['master_kontrak_id']))?$vendor['master_kontrak_id']:0;
    $param['master_vendor_id'] = (isset($vendor['mk_master_vendor_id']))?$vendor['mk_master_vendor_id']:0;
    $param['pks_id'] = (isset($pks['pks_id']))?$pks['pks_id']:0;
    $param['induk_progres_status_id'] = $induk_jadwal;
    $param['kota_id'] = (isset($d_induk_jadwal['kota_id']))?$d_induk_jadwal['kota_id']:0;
    */
    
    if(isset($param['master_kontrak_id']))
    unset($param['master_kontrak_id']);
    
    if(isset($param['master_vendor_id']))
    unset($param['master_vendor_id']);
    
    if(isset($param['pks_id']))
    unset($param['pks_id']);
    
    if(isset($param['induk_progres_status_id']))
    unset($param['induk_progres_status_id']);
    
    if(isset($param['kota_id']))
    unset($param['kota_id']);
    
		return $param;
	}
  
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Evaluasi Progres";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Evaluasi Progres";
  }
  function _hook_create_form_title_update_progress($title){
    return "Update Progress Evaluasi Progres";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_ajax_target_add_timeline(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_kota_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_kota WHERE data_kota_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_kota']) and isset($d['nama_kota']))?'#'.$d['kode_kota'].' - '.$d['nama_kota']:$default_value;
  }
  
  
	function _create_listing($config = array())
	{ 
		$this->load->library("data",array(),"data_jadwal");
		$this->data_jadwal->init($this->init);
		$this->data_jadwal->set_filter();
		$config = (is_array($config) and count($config) > 0)?$config:$this->data_jadwal->config;
		if($this->data_jadwal->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->data_jadwal->init_pagination($config);
		
		$t_nonce = $this->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$path = (isset($config['path']))?$config['path']:'';
		$controller = (isset($config['controller']))?$config['controller']:'';
		$function = (isset($config['function']))?$config['function']:'';
		$action = $this->data_jadwal->show_panel_allowed("",$path,$controller,$panel_function);
    
		$col = "";				
		if(!empty($this->data_jadwal->fields) and is_array($this->data_jadwal->fields) and count($this->data_jadwal->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->data_jadwal->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					
					$colspan = "";
					if($name == "jumlah_desa")
					{
						$colspan = " colspan=\"2\" ";
					}
					$col .= '
							<th '.$colspan.'>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->uri->segment($this->data_jadwal->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->uri->segment($this->data_jadwal->paging_config['uri_segment'],0);
		
		$start_page  = $this->hook->do_action('hook_create_listing_start_page',$start_page);
		$to_page  = $this->hook->do_action('hook_create_listing_to_page',$this->data_jadwal->paging_config['per_page']);
		
		$sql = $this->data_jadwal->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_jadwal->data_rows = $data_rows;
      $jadwal_id = "";
			foreach($data_rows as $index => $data_row)
			{
        $jadwal_id = (isset($data_row['mk_jadwal_proyek_id']))?$data_row['mk_jadwal_proyek_id']:"";
        $data_row_realisasi = $this->_get_realisasi_value($jadwal_id);
				if(!empty($this->data_jadwal->fields) and is_array($this->data_jadwal->fields) and count($this->data_jadwal->fields) > 0)
				{
					$tmp_list_realisasi = "";
					/*$tmp_list_realisasi.= '
					<tr class="active">';*/
					$tmp_list = "";
					$tmp_list.= '
					<tr class="active">';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->data_jadwal->primary_key]))?$data_row[$this->data_jadwal->primary_key]:"";
						$tmp_list.= '
									<td rowspan="2" align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
						#$tmp_list_realisasi.= '
						#			<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
					
					$skip_realisasi_field = array('master_kontrak_id','pks_id','master_vendor_id','nama_pekerjaan');
					$number = ++$start_page;
					$tmp_list.= '
					<td align="center" rowspan="2" style="font-weight:bold;font-size:12px;">' . $number .'</td>';
					foreach($this->data_jadwal->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
														$rowspanded = (in_array($name,$skip_realisasi_field))?true:false;
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->data_jadwal->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									#$value = $this->data_jadwal->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								
								$rowspan = "";
								if($rowspanded)
								{
									$rowspan = ' rowspan="2" ';
								}else{
                  $td_color = ($name != "jumlah_desa")?' class="bg-green" ':'';
                  $real_value = (isset($data_row_realisasi[$name]))?$data_row_realisasi[$name]:"";
									$tmp_list_realisasi.= '<td '.$list_style.' '.$td_color.'>' . $real_value . '</td>';
									$colspan = "";
									if($name == "jumlah_desa")
									{
										$colspan = "<td class=\"bg-green\">Realisasi</td>";
									}
									$tmp_list_realisasi .= $colspan;
								}
								
								$td_color = (empty($rowspan) and $name != "jumlah_desa")?' class="bg-blue" ':'';
								$tmp_list.= '
											<td '.$list_style.' '.$rowspan.' '.$td_color.'>' . $value . '</td>';
								$colspan = "";
								if($name == "jumlah_desa")
								{
									$colspan = "<td class=\"bg-blue\">Jadwal</td>";
								}
								$tmp_list.= $colspan;
											
							}else{
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									#$value = $this->data_jadwal->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								
								$rowspan = "";
								if($rowspanded)
								{
									$rowspan = ' rowspan="2" ';
								}else{
                  $td_color = ($name != "jumlah_desa")?' class="bg-green" ':'';
                  $real_value = (isset($data_row_realisasi[$name]))?$data_row_realisasi[$name]:"";
									$tmp_list_realisasi.= '<td '.$list_style.' '.$td_color.'>' . $real_value . '</td>';
									$colspan = "";
									if($name == "jumlah_desa")
									{
										$colspan = "<td class=\"bg-green\">Realisasi</td>";
									}
									$tmp_list_realisasi .= $colspan;
								}
								$td_color = (empty($rowspan) and $name != "jumlah_desa")?' class="bg-blue" ':'';
								$tmp_list.= '
											<td '.$list_style.' '.$rowspan.' '.$td_color.'>' . $value . '</td>';
								$colspan = "";
								if($name == "jumlah_desa")
								{
									$colspan = "<td class=\"bg-blue\">Jadwal</td>";
								}
								$tmp_list.=$colspan;
							}
						}
					}
					
					if(!empty($action))
					{
						$action = $this->data_jadwal->show_panel_allowed("",$path,$controller,$panel_function,$data_row[$this->data_jadwal->primary_key]);
						$list_style = (isset($this->data_jadwal->the_config['list_style']))?$this->data_jadwal->the_config['list_style']:"class='rows_action'";
						$action  = $this->hook->do_action('hook_create_listing_action',$action);
						$tmp_list.= '<td '.$list_style.' rowspan="2">'. $action .'</td>';
						#$tmp_list_realisasi.= '<td '.$list_style.'>'. $action .'</td>';
					}
					$tmp_list_realisasi.= '
								</tr>';
					$tmp_list.= '
								</tr>'.$tmp_list_realisasi;
          
					$config2 = $this->init_timeline;
					$config2['where'] = " induk_jadwal_proyek_id = '".$data_row[$this->data_jadwal->primary_key]."' AND type_jadwal = 'jadwal' ";
					$config2['parent_number'] = $number;
					$tmp_list .= $this->_create_listing_timeline($config2);
					$list  .= $this->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%" class="table table-bordered table-cellpadding-0">
						<thead>
							<tr>
                ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
  
	function _create_listing_timeline($config = array())
	{
    $this->load->library("data",array(),"data_timeline");
		$this->data_timeline->init($config);
		$this->data_timeline->set_filter();
    $config = (is_array($config) and count($config) > 0)?$config:$this->data_timeline->config;
		if($this->data_timeline->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->data_timeline->init_pagination($config);
		
		$t_nonce = $this->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$path = (isset($config['path']))?$config['path']:'';
		$controller = (isset($config['controller']))?$config['controller']:'';
		$function = (isset($config['function']))?$config['function']:'';
		$action = $this->data_timeline->show_panel_allowed("",$path,$controller,$panel_function);
    
		$col = "";				
		if(!empty($this->data_timeline->fields) and is_array($this->data_timeline->fields) and count($this->data_timeline->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->data_timeline->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->uri->segment($this->data_timeline->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->uri->segment($this->data_timeline->paging_config['uri_segment'],0);
		
		$start_page  = $this->hook->do_action('hook_create_listing_start_page',$start_page);
		$to_page  = $this->hook->do_action('hook_create_listing_to_page',$this->data_timeline->paging_config['per_page']);
		
		$sql = $this->data_timeline->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_timeline->data_rows = $data_rows;
			foreach($data_rows as $index => $data_row)
			{
        $jadwal_id = (isset($data_row['mk_jadwal_proyek_id']))?$data_row['mk_jadwal_proyek_id']:"";

        $data_row_realisasi = $this->_get_realisasi_value($jadwal_id);

				if(!empty($this->data_timeline->fields) and is_array($this->data_timeline->fields) and count($this->data_timeline->fields) > 0)
				{
					$tmp_list_realisasi = "";
					$tmp_list_realisasi.= '
					<tr class="active">';
					$tmp_list = "";
					$tmp_list.= '
					<tr class="active">';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->data_timeline->primary_key]))?$data_row[$this->data_timeline->primary_key]:"";
						$tmp_list.= '
									<td rowspan="2" align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
						#$tmp_list_realisasi.= '
						#			<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
					
					$skip_realisasi_field = array('master_kontrak_id','pks_id','master_vendor_id','nama_pekerjaan');
					$number = ++$start_page;
					$tmp_list.= '
					<td align="center" rowspan="2" style="font-weight:bold;font-size:12px;">' . $config['parent_number'] .'.' . $number .'</td>';
          $jadwal_id = "";
					foreach($this->data_timeline->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
														$rowspanded = (in_array($name,$skip_realisasi_field))?true:false;
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->data_timeline->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									#$value = $this->data_timeline->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								
								$rowspan = "";
								if($rowspanded)
								{
									$rowspan = ' rowspan="2" ';
								}else{
                  $td_color = ($name != "jumlah_desa")?' class="bg-green" ':'';
                  $real_value = (isset($data_row_realisasi[$name]))?$data_row_realisasi[$name]:"";
									$tmp_list_realisasi.= '<td '.$list_style.' '.$td_color.'>' . $real_value . '</td>';
									$colspan = "";
									if($name == "jumlah_desa")
									{
										$colspan = "<td class=\"bg-green\">Realisasi</td>";
									}
									$tmp_list_realisasi .= $colspan;
								}
								
								$td_color = (empty($rowspan) and $name != "jumlah_desa")?' class="bg-blue" ':'';
								$tmp_list.= '
											<td '.$list_style.' '.$rowspan.' '.$td_color.'>' . $value . '</td>';
								$colspan = "";
								if($name == "jumlah_desa")
								{
									$colspan = "<td class=\"bg-blue\">Jadwal</td>";
								}
								$tmp_list.=$colspan;
											
							}else{
								
								if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									#$value = $this->data_timeline->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->hook->do_action('hook_create_listing_value_'.$name,$value);
								
								$rowspan = "";
								if($rowspanded)
								{
									$rowspan = ' rowspan="2" ';
								}else{
                  $td_color = ($name != "jumlah_desa")?' class="bg-green" ':'';
                  $real_value = (isset($data_row_realisasi[$name]))?$data_row_realisasi[$name]:"";
									$tmp_list_realisasi.= '<td '.$list_style.' '.$td_color.'>' . $real_value . '</td>';
									$colspan = "";
									if($name == "jumlah_desa")
									{
										$colspan = "<td class=\"bg-green\">Realisasi</td>";
									}
									$tmp_list_realisasi .= $colspan;
								}
								
								$td_color = ($rowspan == "" and $name != "jumlah_desa")?' class="bg-blue"':'';
								$tmp_list.= '
											<td '.$list_style.' '.$rowspan.' '.$td_color.'>' .  $value  . '</td>';
								$colspan = "";
								if($name == "jumlah_desa")
								{
									$colspan = "<td class=\"bg-blue\">Jadwal</td>";
								}
								$tmp_list.=$colspan;
							}
						}
					}
					
					if(!empty($action))
					{
						$action = $this->data_timeline->show_panel_allowed("",$path,$controller,$panel_function,$data_row[$this->data_timeline->primary_key]);
						$list_style = (isset($this->data_timeline->the_config['list_style']))?$this->data_timeline->the_config['list_style']:"class='rows_action'";
						$action  = $this->hook->do_action('hook_create_listing_action',$action);
						$tmp_list.= '<td '.$list_style.' rowspan="2">'. $action .'</td>';
					}
					
					$tmp_list_realisasi.= '
								</tr>';
					$tmp_list.= '
								</tr>'.$tmp_list_realisasi;
          
					$list  .= $this->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
    
    return $list;
        $output = '<table width="100%" class="table table-bordered table-cellpadding-0">
						<thead>
							<tr>
                ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _get_realisasi_value($jadwal_id = "")
  {
    $q = $this->db->query("SELECT * FROM mk_jadwal_proyek where jadwal_proyek_id = '".$jadwal_id."'");
    $d = $q->row_array();
    return $d;
  }
  
  function _get_jadwal_value($jadwal_id = "")
  {
    $q = $this->db->query("SELECT * FROM mk_jadwal_proyek where mk_jadwal_proyek_id = '".$jadwal_id."'");
    $d = $q->row_array();
    return $d;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
