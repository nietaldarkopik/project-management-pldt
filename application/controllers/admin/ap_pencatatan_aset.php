<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ap_pencatatan_aset extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));

		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/ap_pencatatan_aset/listing',array('response' => '','page_title' => 'Pencatatan Aset','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
    $this->init['fields'][0]['type'] = 'input_view';
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_form_field_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_field_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_field_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_field_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_pencatatan_aset/edit',array('response' => $response,'page_title' => 'Pencatatan Aset'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function update_berita_acara($object_id = "",$desa_id = "")
	{
    $this->db->where(array("master_kontrak_id" => $object_id,"desa_id" => $desa_id));
    $q_get_berita_acara = $this->db->get("mp_asset_berita_acara");
    $data_berita_acara = $q_get_berita_acara->row_array();
    $response = "";
    $status_action = "";
    $do_update_berita_acara = $this->input->post('do_update_berita_acara');
    
    if($do_update_berita_acara == 'save')
    {
      $data_post = array();
      $post = $this->input->post("data");
      $data_post['berita_acara'] = (isset($post['berita_acara']))?$post['berita_acara']:"";
      $data_post['master_kontrak_id'] = $object_id;
      $data_post['desa_id'] = $desa_id;
      
      if(is_array($data_berita_acara) and count($data_berita_acara) > 0)
      {
        $this->db->where(array("master_kontrak_id" => $object_id));
        $status_action = $this->db->update("mp_asset_berita_acara",$data_post);
      }else{
        $status_action = $this->db->insert("mp_asset_berita_acara",$data_post);
      }
      
      if($status_action !== false and $status_action != "")
      {
        $response = '<p class="alert alert-success">Berita Acara berhasil disimpan</p>';
      }elseif($status_action === false and $status_action != ""){
        $response = '<p class="alert alert-warning">Berita Acara gagal tersimpan, silahkan coba lagi</p>';
      }
      
      $this->db->where(array("master_kontrak_id" => $object_id,"desa_id" => $desa_id));
      $q_get_berita_acara = $this->db->get("mp_asset_berita_acara");
      $data_berita_acara = $q_get_berita_acara->row_array();
    }
    
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_pencatatan_aset/update_berita_acara',array('response' => $response,'page_title' => 'Update Berita Acara Uji Terima','data_berita_acara' => $data_berita_acara,'master_kontrak_id' => $object_id,'desa_id' => $desa_id));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_pencatatan_aset/add',array('response' => $response,'page_title' => 'Pencatatan Aset'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_pencatatan_aset/view',array('response' => '','page_title' => 'Pencatatan Aset'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_pencatatan_aset_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_pencatatan_aset/listing',array('response' => '','page_title' => 'Pencatatan Aset','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(
            'table' => 'mp_asset_biaya',
            #'query' => "SELECT * FROM mk_spesifikasi mk_sp JOIN mp_asset_biaya mk_hb ON mk_sp.master_kontrak_id = master_kontrak_id AND mk_sp.mk_spesifikasi_id = spesifikasi_id",
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'master_vendor_id',
                            'label' => 'Vendor',
                            'id' => 'vendor_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
                            'options' => array('' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'readonly' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa/Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_autocomplete',
                            #'query' => 'SELECT chain_geography.*,data_propinsi.nama_propinsi,data_kota.dat2,data_kota.nama_kota,data_kecamatan.nama_kecamatan,data_desa.nama_desa FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            #'query' => 'SELECT concat(data_desa.nama_desa,",data_kecamatan.nama_kecamatan,",",data_kota.dat2," ",data_kota.nama_kota,",",",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            'service_get_json' => array(
                              'table' => 'chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi',
                              'where' => 'chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id',
                              'select' => 'concat(data_desa.nama_desa,", Kec. ",data_kecamatan.nama_kecamatan,", ",data_kota.dat2," ",data_kota.nama_kota,", Prop. ",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value',
                              'foreign_key' => 'data_desa.data_desa_id'
                            ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          /*
                          array(
                            'name' => 'spesifikasi_id',
                            'label' => 'Produk',
                            'id' => 'spesifikasi_id',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'query' => 'SELECT concat(kode_spesifikasi," - ",produk," (",modul_tipe,")") label,mk_spesifikasi_id value FROM mk_spesifikasi ORDER BY mk_spesifikasi_id ASC',
                            'options' => array('' => '-----Pilih Produk-----'),
                            'js_connect_to' => array( 'table' => 'mk_spesifikasi',
                                                      'where' => '',
                                                      'select' => 'produk label,mk_spesifikasi_id value',
                                                      'primary_key' => 'mk_spesifikasi_id',
                                                      'foreign_key' => 'master_kontrak_id',
                                                      'id_field_parent' => 'master_kontrak_id'
                                                      ),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          */
                          array(
                            'name' => 'produk',
                            'label' => 'Produk/Jasa',
                            'id' => 'produk',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'volume',
                            'label' => 'Volume',
                            'id' => 'volume',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'harga_satuan',
                            'label' => 'Harga Satuan',
                            'id' => 'harga_satuan',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'total',
                            'label' => 'Total',
                            'id' => 'total',
                            'value' => '',
                            #'type' => 'input_view',
														'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'ut',
                            'label' => 'Uji Terima',
                            'id' => 'ut',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'options' => array('ya' => 'Ya', 'tidak' => 'Tidak'),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'ut_keterangan',
                            'label' => 'Catatan',
                            'id' => 'ut_keterangan',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          )
                          
                    ),
                    'primary_key' => 'mp_asset_biaya_id',
                    'path' => "/admin/",
                    'controller' => 'ap_pencatatan_aset',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;

		if(isset($param['modul_tipe']))
		  unset($param['modul_tipe']);
		if(isset($param['spesifikasi']))
		  unset($param['spesifikasi']);
			return $param;
	}
	
	function _hook_do_edit($param = "")
	{
    $tmp_param = $param;
    $param = array('ut' => $tmp_param['ut'],'ut_keterangan' => $tmp_param['ut_keterangan']);
    return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Harga dan Biaya";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Harga dan Biaya";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_spesifikasi_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE mk_spesifikasi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['produk']))?$d['kode_spesifikasi'].'-'.$d['produk'].'-'.$d['modul_tipe']:$default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
