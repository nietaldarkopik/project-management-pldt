<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_desa extends Admin_Controller {

  var $init = array();
  var $page_title = "";
  
  function index()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_listing_value_kecamatan_id',array($this,'_hook_create_listing_value_kecamatan_id'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID desa
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_desa_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    if($is_login)
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Desa / Kelurahan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    else
      $this->load->view('layouts/login');
      
  }
  
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Desa / Kelurahan'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();
    
    //hilangkan ID Desa
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'data_desa_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Desa / Kelurahan'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_listing_value_kecamatan_id',array($this,'_hook_create_listing_value_kecamatan_id'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Desa / Kelurahan'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing($offset = 0)
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_data_desa_listing',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_create_listing_value_propinsi_id',array($this,'_hook_create_listing_value_propinsi_id'));
    $this->hook->add_action('hook_create_listing_value_kota_id',array($this,'_hook_create_listing_value_kota_id'));
    $this->hook->add_action('hook_create_listing_value_kecamatan_id',array($this,'_hook_create_listing_value_kecamatan_id'));

    $is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Desa / Kelurahan','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
    else
      $this->load->view('layouts/login');
    
  }
  
  function _config($id_object = "")
  {
    $init = array(  'table' => 'data_desa',
            'fields' => array(
                          array(
                            'name' => 'data_desa_id',
                            'label' => 'ID Desa / Kelurahan',
                            'id' => 'data_desa_id',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'kode_desa',
                            'label' => 'Kode Desa / Kelurahan',
                            'id' => 'kode_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'propinsi_id',
                            'label' => 'Propinsi',
                            'id' => 'propinsi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            //'table' => 'data_propinsi',
                            'query' => 'SELECT nama_propinsi as label,data_propinsi_id as value FROM data_propinsi',
                            'options' => array('0' => '-----Pilih Propinsi-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'kota_id',
                            'label' => 'Kota / Kabupaten',
                            'id' => 'kota_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(dat2, \' \',nama_kota) label,data_kota_id value FROM data_kota',
                            'options' => array('0' => '-----Pilih Kota / Kabupaten-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'primary_key' => 'data_kota_id',
                            'js_connect_to' => array(
                              'table' => 'data_kota',
                              'select' => 'nama_kota label,data_kota_id value',
                              'id_field_parent' => 'propinsi_id',
                              'foreign_key' => 'propinsi_id',
                              'primary_key' => 'data_propinsi_id',
                              'where' => ''
                            ),
                          ),
                          array(
                            'name' => 'kecamatan_id',
                            'label' => 'Kecamatan',
                            'id' => 'kecamatan_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT nama_kecamatan label,data_kecamatan_id value FROM data_kecamatan',
                            'options' => array('0' => '-----Pilih Kecamatan-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'primary_key' => 'data_kecamatan_id',
                            'js_connect_to' => array(
                              'table' => 'data_kecamatan',
                              'select' => 'nama_kecamatan label,data_kecamatan_id value',
                              'id_field_parent' => 'kota_id',
                              'foreign_key' => 'kota_id',
                              'primary_key' => 'data_kota_id',
                              'where' => ''
                            ),
                          ),
                          array(
                            'name' => 'nama_desa',
                            'label' => 'Nama Desa / Kelurahan',
                            'id' => 'nama_desa',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'data_desa',
                    'function' => 'index',
                    'primary_key' => 'data_desa_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );
    $this->init = $init;
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Tambah Desa / Kelurahan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Desa / Kelurahan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }


  function _hook_create_listing_value_propinsi_id($default_value = "")
  {
    $q = $this->db->query("SELECT * FROM data_propinsi WHERE data_propinsi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nama_propinsi'])) ? $d['nama_propinsi']   : $default_value;

  }

  function _hook_create_listing_value_kota_id($default_value = "")
  {
    $q = $this->db->query("SELECT * FROM data_kota WHERE data_kota_id = '".$default_value."'");
    $d = $q->row_array();
    $dat = ($d['dat2'] == 'Kota') ? 'Kota' : 'Kabupaten';
    return (isset($d['nama_kota'])) ? $dat . ' '. $d['nama_kota']   : $default_value;
  }
  
  function _hook_create_listing_value_kecamatan_id($default_value = "")
  {
    $q = $this->db->query("SELECT * FROM data_kecamatan WHERE data_kecamatan_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nama_kecamatan'])) ? $d['nama_kecamatan']   : $default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
