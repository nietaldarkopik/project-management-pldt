<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_cakupan_pekerjaan extends Admin_Controller {

	var $init = array();
	var $config_import = array();
	var $page_title = "";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("model_cakupan_pekerjaan");
	}
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    		if($is_login)
			$this->load->view('layouts/mk_cakupan_pekerjaan/listing',array('response' => '','page_title' => 'Cakupan Pekerjaan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Cakupan Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Cakupan Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_form_view_value_spesifikasi_id',array($this,'_hook_create_listing_value_spesifikasi_id'));
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Cakupan Pekerjaan'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_cakupan_pekerjaan_listing',array($this,'_hook_show_panel_allowed'));
    
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    		if($is_login)
			$this->load->view('layouts/mk_cakupan_pekerjaan/listing',array('response' => '','page_title' => 'Cakupan Pekerjaan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function _config($id_object = "")
	{
    $init = array(
            'table' => 'mk_cakupan_kerja',
            #'query' => "SELECT mk_hb.*,mk_sp.* FROM mk_spesifikasi mk_sp LEFT JOIN mk_cakupan_kerja mk_hb ON mk_sp.master_kontrak_id = mk_hb.master_kontrak_id AND mk_sp.mk_spesifikasi_id = mk_hb.spesifikasi_id",
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa/Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(data_desa.kode_desa,' - ',data_desa.nama_desa) label,mk_desa_lokasi.desa_id value FROM mk_desa_lokasi,data_desa,mk_master_kontrak WHERE mk_desa_lokasi.master_kontrak_id = mk_master_kontrak.mk_master_kontrak_id AND mk_desa_lokasi.desa_id = data_desa.data_desa_id',
                            'options' => array('' => '-----Pilih Desa/Kelurahan-----'),
                            'use_search' => false,
                            'use_listing' => true,
                            'js_connect_to' => 
                              array(  'table' => 'mk_desa_lokasi,data_desa',
                                      'where' => 'mk_desa_lokasi.desa_id  = data_desa.data_desa_id',
                                      'select' => 'concat(data_desa.kode_desa,"-",nama_desa) label,data_desa_id value',
                                      'primary_key' => 'data_desa_id',
                                      'foreign_key' => 'mk_desa_lokasi.master_kontrak_id',
                                      'id_field_parent' => '"#master_kontrak_id"'
                              ),
                            'rules' => ''
                          ),
                          array(
                            'name' => 'spesifikasi_id',
                            'label' => 'Barang/Jasa',
                            'id' => 'spesifikasi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(kode_spesifikasi," - ",produk," (",modul_tipe,")") label,mk_spesifikasi_id value FROM mk_spesifikasi ORDER BY mk_spesifikasi_id ASC',
                            'options' => array('' => '-----Pilih Barang/Jasa-----'),
                            'js_connect_to__' => array( 'table' => 'mk_spesifikasi',
                                                      'where' => '',
                                                      'select' => 'produk label,mk_spesifikasi_id value',
                                                      'primary_key' => 'mk_spesifikasi_id',
                                                      'foreign_key' => 'master_kontrak_id',
                                                      'id_field_parent' => 'master_kontrak_id'
                                                      ),
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'nilai',
                            'label' => 'Jumlah',
                            'id' => 'nilai',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_rencana',
                            'label' => 'Tanggal Rencana',
                            'id' => 'tanggal_rencana',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          /*
                          array(
                            'name' => 'bulan',
                            'label' => 'Bulan',
                            'id' => 'bulan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'minggu',
                            'label' => 'Minggu ke',
                            'id' => 'minggu',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tahun',
                            'label' => 'Tahun',
                            'id' => 'tahun',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          )*/
                    ),
                    'group_by' => ' GROUP BY master_kontrak_id,spesifikasi_id ',
                    'primary_key' => 'mk_cakupan_kerja_id',
                    'path' => "/admin/",
                    'controller' => 'mk_cakupan_pekerjaan',
                    'function' => 'index',
                    'panel_function' => array(
                                              #array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View Detail','name' => 'view', 'class' => 'glyphicon-share'),
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              #array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_before_insert_mk_cakupan_kerja',array($this,'_hook_importer_do_insert_data_before_insert_mk_cakupan_kerja'));
    $fields_insert = 
                    array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Nomor Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          /*
                          array(
                            'name' => 'pks_id',
                            'label' => 'PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_hidden',
                            'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                            'options' => array('' => '-----Pilih PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),*/
                          array(
                            'name' => 'spesifikasi_id',
                            'label' => 'Kode Spesifikasi Barang/Jasa',
                            'id' => 'spesifikasi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(kode_spesifikasi," - ",produk," (",modul_tipe,")") label,mk_spesifikasi_id value FROM mk_spesifikasi ORDER BY mk_spesifikasi_id ASC',
                            'options' => array('' => '-----Pilih Produk-----'),
                            'js_connect_to__' => array( 'table' => 'mk_spesifikasi',
                                                      'where' => '',
                                                      'select' => 'produk label,mk_spesifikasi_id value',
                                                      'primary_key' => 'mk_spesifikasi_id',
                                                      'foreign_key' => 'master_kontrak_id',
                                                      'id_field_parent' => 'master_kontrak_id'
                                                      ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Kode Desa/Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nilai',
                            'label' => 'Jumlah',
                            'id' => 'nilai',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'satuan',
                            'label' => 'Satuan',
                            'id' => 'satuan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_rencana',
                            'label' => 'Tanggal Rencana',
                            'id' => 'tanggal_rencana',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          )/*,
                          array(
                            'name' => 'bulan',
                            'label' => 'Bulan',
                            'id' => 'bulan',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'minggu',
                            'label' => 'Minggu ke',
                            'id' => 'minggu',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tahun',
                            'label' => 'Tahun',
                            'id' => 'tahun',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          )*/
                    );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_cakupan_kerja',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mk_cakupan_kerja_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_cakupan_kerja/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{	
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;

		if(isset($param['modul_tipe']))
		  unset($param['modul_tipe']);
		if(isset($param['spesifikasi']))
		  unset($param['spesifikasi']);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Cakupan Pekerjaan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Cakupan Pekerjaan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_importer_do_insert_data_before_insert_mk_cakupan_kerja($param = array())
  {
		$nomor_kontrak = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE nomor_kontrak = '".$nomor_kontrak."'");
		$kontrak = $q->row_array();
		
		$kode_spesifikasi = (isset($param['spesifikasi_id']))?$param['spesifikasi_id']:"";
		$q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE kode_spesifikasi = '".$kode_spesifikasi."'");
		$spesifikasi = $q->row_array();
    
		$kode_desa = (isset($param['desa_id']))?$param['desa_id']:"";
		$q = $this->db->query("SELECT * FROM data_desa WHERE kode_desa = '".$kode_desa."'");
		$desa = $q->row_array();
    
		$param['desa_id'] = (isset($desa['data_desa_id']))?$desa['data_desa_id']:0;
		$param['spesifikasi_id'] = (isset($spesifikasi['mk_spesifikasi_id']))?$spesifikasi['mk_spesifikasi_id']:0;
		$param['master_kontrak_id'] = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		return $param;
  }
  
  function _hook_importer_do_insert_data_after_insert_mk_cakupan_kerja($data_inserted_tbl = array())
  {
    
  }
  
  function detail_week($tanggal_mulai = "",$tanggal_akhir = "",$minggu_ke = "",$master_kontrak_id = "",$spesifikasi_id = "")
  {
    
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();

    $minggu_ke = $minggu_ke + 1;
    $strdate1 = strtotime($tanggal_mulai);
    $strdate2 = strtotime($tanggal_akhir);
    
    #$tanggal_mulai = date('Y-m-d', strtotime("-7 days", $strdate1));
    #$tanggal_akhir = date('Y-m-d', strtotime("-7 days", $strdate2));
    
    $data_param = array('tanggal_mulai' => $tanggal_mulai,
                        'tanggal_akhir' => $tanggal_akhir,
                        'master_kontrak_id' => $master_kontrak_id,
                        'minggu_ke' => $minggu_ke,
                        'spesifikasi_id'  =>  $spesifikasi_id
                        );
    $all_data = $this->model_cakupan_pekerjaan->get_total_work_location($tanggal_mulai,$tanggal_akhir,$master_kontrak_id,$spesifikasi_id);

		$is_login = $this->user_access->is_login();
    
    if($is_login)
			$this->load->view('layouts/mk_cakupan_pekerjaan/listing_detail_week',array('response' => '','data_param' => $data_param,'page_title' => 'Detail Cakupan Pekerjaan Minggu ke '.$minggu_ke,'data_cakupan_kerja' => $all_data));
		else
			$this->load->view('layouts/login');
			
  }
  
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_desa_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_desa WHERE data_desa_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_desa']) and isset($d['nama_desa']))?$d['kode_desa'].' - '.$d['nama_desa']:$default_value;
  }
  
  function _hook_create_listing_value_spesifikasi_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_spesifikasi WHERE mk_spesifikasi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_spesifikasi']) or isset($d['produk']))?$d['kode_spesifikasi'].' - '.$d['produk']:$default_value;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
