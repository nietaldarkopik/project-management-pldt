<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_beban_anggaran extends Admin_Controller {

	var $init = array();
	var $config_import = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));

		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/mk_beban_anggaran/listing',array('response' => '','page_title' => 'Data Beban Anggaran','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_beban_anggaran/edit',array('response' => $response,'page_title' => 'Data Beban Anggaran'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_beban_anggaran/add',array('response' => $response,'page_title' => 'Data Beban Anggaran'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_form_view_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_form_view_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_form_view_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_form_view_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_beban_anggaran/view',array('response' => '','page_title' => 'Data Beban Anggaran'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_beban_anggaran_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor_id',array($this,'_hook_create_listing_value_master_vendor_id'));
		$this->hook->add_action('hook_create_listing_value_sumber_pendanaan_id',array($this,'_hook_create_listing_value_sumber_pendanaan_id'));

		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_beban_anggaran/listing',array('response' => '','page_title' => 'Data Beban Anggaran','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	
					#'query' => "SELECT * FROM mk_beban_anggaran ba JOIN mk_sumber_pendanaan sp on sp.mk_sumber_pendanaan_id = sumber_pendanaan_id",
					'table' => "mk_beban_anggaran",
					'fields' => array(
									  array(
										'name' => 'master_kontrak_id',
										'label' => 'Master Kontrak',
										'id' => 'master_kontrak_id',
										'value' => '',
										'type' => 'input_selectbox',
										'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
										'options' => array('' => '-----Pilih Master Kontrak-----'),
										'use_search' => true,
										'use_listing' => true,
										'rules' => 'required',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'pks_id',
										'label' => 'PKS',
										'id' => 'pks_id',
										'value' => '',
										'type' => 'input_hidden',
										'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
										'options' => array('' => '-----Pilih PKS-----'),
										'use_search' => true,
										'use_listing' => true,
										'readonly' => true,
										'rules' => '',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'master_vendor_id',
										'label' => 'Vendor',
										'id' => 'vendor_id',
										'value' => '',
										'type' => 'input_hidden',
										'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
										'options' => array('' => '-----Pilih Vendor-----'),
										'use_search' => true,
										'use_listing' => true,
                    'nowrap' => 'nowrap',
										'rules' => ''
									  ),
									  array(
										'name' => 'sumber_pendanaan_id',
										'label' => 'Sumber Pendanaan',
										'id' => 'sumber_pendanaan_id',
										'value' => '',
										'type' => 'input_hidden',
										'query' => 'SELECT instansi label,mk_sumber_pendanaan_id value FROM mk_sumber_pendanaan',
										'options' => array('' => '-----Pilih Sumber Pendanaan-----'),
										'use_search' => true,
										'use_listing' => true,
										'rules' => ''
									  ),
									  array(
										'name' => 'beban_anggaran',
										'label' => 'Beban Anggaran',
										'id' => 'beban_anggaran',
										'value' => '',
										'type' => 'input_text',
										'use_search' => false,
										'use_listing' => true,
										'rules' => 'required',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  ),
									  array(
										'name' => 'dana_alokasi',
										'label' => 'Dana Alokasi',
										'id' => 'dana_alokasi',
										'value' => '',
										'type' => 'input_text',
										'use_search' => false,
										'use_listing' => true,
										'rules' => 'required',
										'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
									  )
                    ),
                    'path' => "/admin/",
                    'controller' => 'mk_beban_anggaran',
                    'function' => 'index',
                    'primary_key' => 'mk_beban_anggaran_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );
		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_before_insert_mk_beban_anggaran',array($this,'_hook_importer_do_insert_data_before_insert_mk_beban_anggaran'));
    $fields_insert = 
                    array(
                      array(
                      'name' => 'master_kontrak_id',
                      'label' => 'Nomor Kontrak',
                      'id' => 'master_kontrak_id',
                      'value' => '',
                      'type' => 'input_selectbox',
                      'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                      'options' => array('' => '-----Pilih Master Kontrak-----'),
                      'use_search' => true,
                      'use_listing' => true,
                      'rules' => 'required',
                      'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                      ),
                      /*
                      array(
                      'name' => 'pks_id',
                      'label' => 'PKS',
                      'id' => 'pks_id',
                      'value' => '',
                      'type' => 'input_hidden',
                      'query' => 'SELECT concat(kode_pks," - ",judul_pks) label,data_pks_id value FROM data_pks',
                      'options' => array('' => '-----Pilih PKS-----'),
                      'use_search' => true,
                      'use_listing' => true,
                      'readonly' => true,
                      'rules' => '',
                      'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                      ),
                      array(
                      'name' => 'master_vendor_id',
                      'label' => 'Vendor',
                      'id' => 'vendor_id',
                      'value' => '',
                      'type' => 'input_hidden',
                      'query' => 'SELECT judul_vendor label,mk_master_vendor_id value FROM mk_master_vendor',
                      'options' => array('' => '-----Pilih Vendor-----'),
                      'use_search' => true,
                      'use_listing' => true,
                      'rules' => ''
                      ),
                      array(
                      'name' => 'sumber_pendanaan_id',
                      'label' => 'Sumber Pendanaan',
                      'id' => 'sumber_pendanaan_id',
                      'value' => '',
                      'type' => 'input_hidden',
                      'query' => 'SELECT instansi label,mk_sumber_pendanaan_id value FROM mk_sumber_pendanaan',
                      'options' => array('' => '-----Pilih Sumber Pendanaan-----'),
                      'use_search' => true,
                      'use_listing' => true,
                      'rules' => ''
                      ),*/
                      array(
                      'name' => 'beban_anggaran',
                      'label' => 'Beban Anggaran',
                      'id' => 'beban_anggaran',
                      'value' => '',
                      'type' => 'input_text',
                      'use_search' => true,
                      'use_listing' => true,
                      'rules' => 'required',
                      'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                      ),
                      array(
                      'name' => 'dana_alokasi',
                      'label' => 'Dana Alokasi',
                      'id' => 'dana_alokasi',
                      'value' => '',
                      'type' => 'input_text',
                      'use_search' => true,
                      'use_listing' => true,
                      'rules' => 'required',
                      'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
                      )
                      );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_beban_anggaran',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mk_beban_anggaran_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_beban_anggaran/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		#$q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
		#$vendor = $q->row_array();
		
		#$q_sumber_dana = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE master_kontrak_id = '".$kontrak_id."'");
		#$q_sumber_dana = $q_sumber_dana->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['sumber_pendanaan_id'] = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:0;
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$kontrak_id."'");
		$kontrak = $q->row_array();
		
		#$q = $this->db->query("SELECT * FROM mk_master_vendor WHERE master_kontrak_id = '".$kontrak_id."'");
		#$vendor = $q->row_array();
		
		#$q_sumber_dana = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE master_kontrak_id = '".$kontrak_id."'");
		#$q_sumber_dana = $q_sumber_dana->row_array();
		
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['sumber_pendanaan_id'] = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:0;
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Beban Anggaran";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Beban Anggaran";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_master_vendor_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) or isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_sumber_pendanaan_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_sumber_pendanaan WHERE mk_sumber_pendanaan_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_sumber_pendanaan']) or isset($d['instansi']))?$d['kode_sumber_pendanaan'].' - '.$d['instansi']:$default_value;
  }
  
  
	function _hook_importer_do_insert_data_before_insert_mk_beban_anggaran($param = "")
	{
		$kontrak_id = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE nomor_kontrak = '".$kontrak_id."'");
		$kontrak = $q->row_array();
				
		$param['master_kontrak_id'] = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor_id'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['sumber_pendanaan_id'] = (isset($kontrak['master_sumber_pendanaan_id']))?$kontrak['master_sumber_pendanaan_id']:0;
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
