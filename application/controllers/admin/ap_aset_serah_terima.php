<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ap_aset_serah_terima extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_listing_value_koperasi_id',array($this,'_hook_create_listing_value_koperasi_id'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));

		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/ap_aset_serah_terima/listing',array('response' => '','page_title' => 'Daftar Aset Diserahterimakan','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_listing',array($this,'_hook_show_panel_allowed'));	
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_aset_serah_terima/edit',array('response' => $response,'page_title' => 'Data Daftar Aset Diserahterimakan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
    
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_aset_serah_terima/add',array('response' => $response,'page_title' => 'Daftar Aset Diserahterimakan'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_listing',array($this,'_hook_show_panel_allowed'));		$this->hook->add_action('hook_create_listing_value_koperasi_id',array($this,'_hook_create_listing_value_koperasi_id'));
		$this->hook->add_action('hook_create_form_view_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_form_view_value_koperasi_id',array($this,'_hook_create_listing_value_koperasi_id'));
		$this->hook->add_action('hook_create_form_view_master_kontrak_id',array($this,'_hook_create_listing_master_kontrak_id'));

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_aset_serah_terima/view',array('response' => '','page_title' => 'Daftar Aset Diserahterimakan'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_ap_aset_serah_terima_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_listing_value_koperasi_id',array($this,'_hook_create_listing_value_koperasi_id'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));

		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/ap_aset_serah_terima/listing',array('response' => '','page_title' => 'Daftar Aset Diserahterimakan','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => "mp_pengoperasian",
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Master Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa/Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_autocomplete',
                            #'query' => 'SELECT chain_geography.*,data_propinsi.nama_propinsi,data_kota.dat2,data_kota.nama_kota,data_kecamatan.nama_kecamatan,data_desa.nama_desa FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            #'query' => 'SELECT concat(data_desa.nama_desa,",data_kecamatan.nama_kecamatan,",",data_kota.dat2," ",data_kota.nama_kota,",",",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id ORDER BY data_desa.nama_desa ASC',
                            'service_get_json' => array(
                              'table' => 'chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi',
                              'where' => 'chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id',
                              'select' => 'concat(data_desa.nama_desa,", Kec. ",data_kecamatan.nama_kecamatan,", ",data_kota.dat2," ",data_kota.nama_kota,", Prop. ",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value',
                              'foreign_key' => 'data_desa.data_desa_id'
                            ),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'koperasi_id',
                            'label' => 'Koperasi/Daftar Aset Diserahterimakan',
                            'id' => 'koperasi_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT CONCAT(nama," - ",nama_koperasi) label,mk_koperasi_id value FROM mk_koperasi',
                            'options' => array('' => '---- Pilih Daftar Aset Diserahterimakan'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'tanggal_operasi',
                            'label' => 'Tanggal Operasi',
                            'id' => 'tanggal_operasi',
                            'value' => '',
                            'type' => 'input_date',
                            'use_search' => false,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                    ),
                    'primary_key' => 'mp_pengoperasian_id',
                    'path' => "/admin/",
                    'controller' => 'mk_pengoperasian',
                    'function' => 'index',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );

		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_before_insert_ap_aset_serah_terima',array($this,'_hook_importer_do_insert_data_before_insert_ap_aset_serah_terima'));
    $fields_insert = 
                    array(
												array(
												'name' => 'master_kontrak_id',
												'label' => 'Nomor Kontrak',
												'id' => 'master_kontrak_id',
												'value' => '',
												'type' => 'input_selectbox',
												'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
												'options' => array('' => '-----Pilih Master Kontrak-----'),
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required',
												'list_style' => 'nowrap="nowrap" style="font-weight:bold;font-size:12px;" '
												),
												array(
													'name' => 'desa_id',
													'label' => 'Kode Desa',
													'id' => 'desa_id',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'koperasi_id',
													'label' => 'Kode Koperasi',
													'id' => 'koperasi_id',
													'value' => '',
													'type' => 'input_mce',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required',
													'list_style' => ' style="min-width:300px;width:300px;text-align:justify;font-weight:bold;font-size:12px;" '
												),
												array(
													'name' => 'tanggal_operasi',
													'label' => 'Tanggal Beroperasi',
													'id' => 'tanggal_operasi',
													'value' => '',
													'type' => 'input_mce',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required',
													'list_style' => ' style="min-width:300px;width:300px;text-align:justify;font-weight:bold;font-size:12px;" '
												)
                    );
		$configs = array(	'config_table' =>	array(
																					'table' => 'ap_aset_serah_terima',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'mp_pengoperasian_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/ap_aset_serah_terima/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Daftar Aset Diserahterimakan";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Daftar Aset Diserahterimakan";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_create_listing_value_desa_id($default_value = ""){
    $query = 'SELECT concat(data_desa.nama_desa,", Kec. ",data_kecamatan.nama_kecamatan,", ",data_kota.dat2," ",data_kota.nama_kota,", Prop. ",data_propinsi.nama_propinsi) label,data_desa.data_desa_id value
              FROM chain_geography,data_desa,data_kecamatan,data_kota,data_propinsi
              WHERE chain_geography.data_desa_id = data_desa.data_desa_id AND chain_geography.data_kecamatan_id = data_kecamatan.data_kecamatan_id AND chain_geography.data_kota_id = data_kota.data_kota_id AND chain_geography.data_propinsi_id = data_propinsi.data_propinsi_id
               AND data_desa.data_desa_id = "'.$default_value.'"';
              
    $q = $this->db->query($query);
    $d = $q->row_array();
    return (isset($d['label']))?$d['label']:$default_value;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_koperasi_id($default_value = ""){
    $q = $this->db->query("SELECT CONCAT(nama,' - ',nama_koperasi) label FROM mk_koperasi WHERE mk_koperasi_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['label']))?$d['label']:$default_value;
  }
    
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
    
  function _hook_importer_do_insert_data_before_insert_ap_aset_serah_terima($param = array())
  {
		$nomor_kontrak = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE nomor_kontrak = '".$nomor_kontrak."'");
		$kontrak = $q->row_array();
		
		$kode_desa = (isset($param['desa_id']))?$param['desa_id']:"";
		$q = $this->db->query("SELECT * FROM data_desa WHERE kode_desa = '".$kode_desa."'");
		$desa = $q->row_array();
    
		$kode_koperasi = (isset($param['koperasi_id']))?$param['koperasi_id']:"";
		$q = $this->db->query("SELECT * FROM mk_koperasi WHERE kode_koperasi = '".$kode_koperasi."'");
		$koperasi = $q->row_array();
    
		$param['master_kontrak_id'] = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
		$param['desa_id'] = (isset($desa['data_desa_id']))?$desa['data_desa_id']:0;
		$param['koperasi_id'] = (isset($koperasi['mk_koperasi_id']))?$koperasi['mk_koperasi_id']:0;
		return $param;
  }
  
  function _hook_importer_do_insert_data_after_insert_ap_aset_serah_terima($data_inserted_tbl = array())
  {
    
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
