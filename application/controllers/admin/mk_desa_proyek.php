<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk_desa_proyek extends Admin_Controller {

	var $init = array();
	var $page_title = "";

	function index()
	{

        $this->load->model('model_data_pks');
        $this->load->model('model_data_propinsi');
        $this->_config();

		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor',array($this,'_hook_create_listing_value_master_vendor'));
		
		$is_login = $this->user_access->is_login();

        //data pks
        $option_pks = array();
        $jenis_pks = $this->model_data_pks->get();
        if($jenis_pks['total'] > 0)
        {
          foreach($jenis_pks['data'] as $idx => $jp){
            $option_pks[$jp->data_pks_id] = $jp->judul_pks;
          }
        }

        //data propinsi
        $option_propinsi = array();
        $get_propinsi = $this->model_data_propinsi->get(); 
        if($get_propinsi['total'] > 0)
        {
          foreach($get_propinsi['data'] as $idx => $gp){
            $option_propinsi[$gp->data_propinsi_id] = $gp->nama_propinsi;
          }
        }

        $filter_data_lokasi_proyek = $this->session->userdata('filter_data_lokasi_proyek');
        $this->db->order_by('desa_lokasi_id', 'DESC');
        $data_lokasi_proyek = $this->db->get('v_mk_desa_lokasi')->result_array();

        $config_form_filter = $this->init;
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
        $config_form_add = $this->init;
        $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
        $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/mk_desa_proyek/listing',
        array(
          'response' => '',
          'page_title' => 'Data  Desa / Kelurahan - Lokasi Proyek',
          'config_form_add' => $config_form_add,
          'config_form_filter' => $config_form_filter,
          'listing_config' => $this->init,
          'option_pks' => $option_pks,
          'option_propinsi' => $option_propinsi,
          'data_lokasi_proyek' => $data_lokasi_proyek,
        )
      );
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
    $this->load->model('model_data_pks');
    $this->load->model('model_mk_master_kontrak');
    $this->load->model('model_data_propinsi');
    $this->load->model('model_data_kota');
    $this->load->model('model_data_kecamatan');
    $this->load->model('model_data_desa');
    
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_listing',array($this,'_hook_show_panel_allowed'));

    /*===================================update data====================================================*/

    $success_update = true;

    $data_update = $this->input->post('data_update');
    $desa_lokasi_id = $this->input->post('desa_lokasi_id');
    $msg_pks_id = '';
    $msg_master_kontrak_id = '';
    $msg_master_vendor = '';
    $msg_desa_id = '';
    $response = '';

    if(!empty($desa_lokasi_id)){
      if(empty($data_update['pks_id'])){
        $success_update = false;
        $msg_pks_id = '<p class="alert alert-danger error">Jenis PKS diperlukan.</p>';
      }

      if(empty($data_update['master_kontrak_id'])){
        $success_update = false;
        $msg_master_kontrak_id = '<p class="alert alert-danger error">Nomor Kontrak diperlukan.</p>';
      }

      if(empty($data_update['master_vendor'])){
        $success_update = false;
        $msg_master_vendor = '<p class="alert alert-danger error">Vendor diperlukan.</p>';
      }

      if(empty($data_update['desa_id'])){
        $success_update = false;
        $msg_desa_id = '<p class="alert alert-danger error">Desa / Kelurahan diperlukan.</p>';
      }

      if($success_update)
      {
        $data_update['user_id'] = $this->session->userdata('user_id');
        $data_update['datetime_update'] = date('Y-m-d H:i:s');
        $this->db->where('desa_lokasi_id', $desa_lokasi_id);
        $update = $this->db->update('mk_desa_lokasi', $data_update);
        if(!$update)
          $success_update = false;
      }
      $response = ($success_update) ? '<div class="alert alert-success">Data berhasil disimpan</div>' : '<div class="alert alert-danger">Data gagal disimpan</div>';

    }
		
    /*=============================== show data ========================================================*/
    $this->db->where('desa_lokasi_id', $object_id);
    $data_lokasi_proyek = $this->db->get('v_mk_desa_lokasi')->row_array();
    
    //data pks
    $option_pks = array();
    $jenis_pks = $this->model_data_pks->get();
    if($jenis_pks['total'] > 0)
    {
      foreach($jenis_pks['data'] as $idx => $jp){
        $option_pks[$jp->data_pks_id] = $jp->judul_pks;
      }
    }

    //data kontrak
    $option_kontrak = array();
    $kontrak = $this->model_mk_master_kontrak->get();
    if($kontrak['total'] > 0)
    {
      foreach($kontrak['data'] as $idx => $jp){
        $option_kontrak[$jp->mk_master_kontrak_id] = $jp->nomor_kontrak;
      }
    }

    //data propinsi
    $option_propinsi = array();
    $get_propinsi = $this->model_data_propinsi->get(); 
    if($get_propinsi['total'] > 0)
    {
      foreach($get_propinsi['data'] as $idx => $gp){
        $option_propinsi[$gp->data_propinsi_id] = $gp->nama_propinsi;
      }
    }

    //data kota
    $option_kota = array();
    $get_kota = $this->model_data_kota->get(0, 0, array('propinsi_id' => $data_lokasi_proyek['data_propinsi_id'])); 
    if($get_kota['total'] > 0)
    {
      foreach($get_kota['data'] as $idx => $dt){
        $option_kota[$dt->data_kota_id] = $dt->dat2.' '.$dt->nama_kota;
      }
    }
    
    //data kecamatan
    $option_kecamatan = array();
    $get_kecamatan = $this->model_data_kecamatan->get(0, 0, array('kota_id' => $data_lokasi_proyek['data_kota_id'])); 
    if($get_kecamatan['total'] > 0)
    {
      foreach($get_kecamatan['data'] as $idx => $dt){
        $option_kecamatan[$dt->data_kecamatan_id] = $dt->nama_kecamatan;
      }
    }

    //data desa
    $option_desa = array();
    $get_desa = $this->model_data_desa->get(0, 0, array('kecamatan_id' => $data_lokasi_proyek['data_kecamatan_id'])); 
    if($get_desa['total'] > 0)
    {
      foreach($get_desa['data'] as $idx => $dt){
        $option_desa[$dt->data_desa_id] = $dt->nama_desa;
      }
    }

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_desa_proyek/edit',array(
        'response' => $response,
        'page_title' => 'Data  Desa / Kelurahan - Lokasi Proyek',
        'data_lokasi_proyek' => $data_lokasi_proyek,
        'option_pks' => $option_pks,
        'option_kontrak' => $option_kontrak,
        'option_propinsi' => $option_propinsi,
        'option_kota' => $option_kota,
        'option_kecamatan' => $option_kecamatan,
        'option_desa' => $option_desa,
        'msg_pks_id' => $msg_pks_id,
        'msg_master_kontrak_id' => $msg_master_kontrak_id,
        'msg_master_vendor' => $msg_master_vendor,
        'msg_desa_id' => $msg_desa_id,
        ));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
    $this->load->model('model_data_pks');
    $this->load->model('model_data_propinsi');
    
    //data pks
    $option_pks = array();
    $jenis_pks = $this->model_data_pks->get();
    if($jenis_pks['total'] > 0)
    {
      foreach($jenis_pks['data'] as $idx => $jp){
        $option_pks[$jp->data_pks_id] = $jp->judul_pks;
      }
    }

    //data propinsi
    $option_propinsi = array();
    $get_propinsi = $this->model_data_propinsi->get(); 
    if($get_propinsi['total'] > 0)
    {
      foreach($get_propinsi['data'] as $idx => $gp){
        $option_propinsi[$gp->data_propinsi_id] = $gp->nama_propinsi;
      }
    }
		$success_insert = true;
    $data_insert = $this->input->post('data_insert');
    
    $msg_pks_id = '';
    if(empty($data_insert['pks_id'])){
      $success_insert = false;
      $msg_pks_id = '<p class="alert alert-danger error">Jenis PKS diperlukan.</p>';
    }

    $msg_master_kontrak_id = '';
    if(empty($data_insert['master_kontrak_id'])){
      $success_insert = false;
      $msg_master_kontrak_id = '<p class="alert alert-danger error">Nomor Kontrak diperlukan.</p>';
    }

    $msg_master_vendor = '';
    if(empty($data_insert['master_vendor'])){
      $success_insert = false;
      $msg_master_vendor = '<p class="alert alert-danger error">Vendor diperlukan.</p>';
    }

    $msg_desa_id = '';
    if(empty($data_insert['desa_id'])){
      $success_insert = false;
      $msg_desa_id = '<p class="alert alert-danger error">Desa / Kelurahan diperlukan.</p>';
    }

    if($success_insert)
    {
      $data_insert['user_id'] = $this->session->userdata('user_id');
      $insert = $this->db->insert('mk_desa_lokasi', $data_insert);
      if(!$insert)
        $success_insert = false;
    }
    
    $response = ($success_insert) ? '<div class="alert alert-success">Data berhasil disimpan</div>' : '<div class="alert alert-danger">Data gagal disimpan</div>';

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_desa_proyek/add',array(
        'option_pks' => $option_pks,
        'option_propinsi' => $option_propinsi,
        'response' => $response,
        'page_title' => 'Data Desa / Kelurahan - Lokasi Proyek',
        'msg_pks_id' => $msg_pks_id,
        'msg_master_kontrak_id' => $msg_master_kontrak_id,
        'msg_master_vendor' => $msg_master_vendor,
        'msg_desa_id' => $msg_desa_id,
        )
      );
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor',array($this,'_hook_create_listing_value_master_vendor'));
		
    
    $this->db->where('desa_lokasi_id', $object_id);
    $data_lokasi_proyek = $this->db->get('v_mk_desa_lokasi')->row_array();

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_desa_proyek/view',array('response' => '','page_title' => 'Data Desa / Kelurahan - Lokasi Proyek', 'data_lokasi_proyek' => $data_lokasi_proyek));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_mk_desa_proyek_listing',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_create_listing_value_master_kontrak_id',array($this,'_hook_create_listing_value_master_kontrak_id'));
		$this->hook->add_action('hook_create_listing_value_pks_id',array($this,'_hook_create_listing_value_pks_id'));
		$this->hook->add_action('hook_create_listing_value_desa_id',array($this,'_hook_create_listing_value_desa_id'));
		$this->hook->add_action('hook_create_listing_value_master_vendor',array($this,'_hook_create_listing_value_master_vendor'));
    
    $is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
    $filter_data_lokasi_proyek = $this->session->userdata('filter_data_lokasi_proyek');
    $this->db->order_by('desa_lokasi_id', 'DESC');
    $data_lokasi_proyek = $this->db->get('v_mk_desa_lokasi')->result_array();


		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/mk_desa_proyek/listing',array('response' => '','page_title' => 'Data Desa / Kelurahan - Lokasi Proyek','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add, 'data_lokasi_proyek' => $data_lokasi_proyek));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
    $init = array(	'table' => 'mk_desa_lokasi',
						'fields' => array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Nomor Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT nomor_kontrak as label,mk_master_kontrak_id as value FROM mk_master_kontrak',
                            'options' => array('0' => '-----Pilih Nomor Kontrak-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                            'primary_key' => 'mk_master_kontrak_id',
                            'js_connect_to' => array(
                              'table' => 'mk_master_kontrak',
                              'select' => 'nomor_kontrak label,mk_master_kontrak_id value',
                              'id_field_parent' => 'pks_id',
                              'foreign_key' => 'pks_id',
                              'primary_key' => 'data_pks_id',
                              'where' => ''
                            ),
                          ),
                          array(
                            'name' => 'pks_id',
                            'label' => 'Jenis PKS',
                            'id' => 'pks_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT judul_pks as label,data_pks_id as value FROM data_pks',
                            'options' => array('0' => '-----Pilih Jenis PKS-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'master_vendor',
                            'label' => 'Vendor',
                            'id' => 'master_vendor',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(nama, \' | \', perusahaan) as label,mk_master_vendor_id as value FROM mk_master_vendor',
                            'options' => array('0' => '-----Pilih Vendor-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          )/*,
													 array(
                            'name' => 'desa_lokasi_id',
                            'label' => 'ID Lokasi Proyek',
                            'id' => 'desa_lokasi_id',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => false,
                            'use_listing' => true,
                            'readonly' => 'readonly',
                            'rules' => 'required'
                          )*/,
                          array(
                            'name' => 'desa_id',
                            'label' => 'Desa / Kelurahan',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT nama_desa as label,data_desa_id as value FROM data_desa,mk_desa_lokasi WHERE data_desa.data_desa_id = mk_desa_lokasi.desa_id',
                            'options' => array('0' => '-----Pilih Desa / Kelurahan-----'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
                          ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'mk_desa_proyek',
                    'function' => 'index',
                    'primary_key' => 'desa_lokasi_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            )
          );
		$this->init = $init;
	}
	
	function _config_import($id_object = "")
	{
    $this->hook->add_action('hook_importer_do_insert_data_before_insert_mk_desa_lokasi',array($this,'_hook_importer_do_insert_data_before_insert_mk_desa_lokasi'));
    $fields_insert = 
                    array(
                          array(
                            'name' => 'master_kontrak_id',
                            'label' => 'Nomor Kontrak',
                            'id' => 'master_kontrak_id',
                            'value' => '',
                            'type' => 'input_selectbox',
                            'query' => 'SELECT concat(mk.nomor_kontrak,"  -  ",mk.judul_kontrak," ") label,mk_master_kontrak_id value FROM mk_master_kontrak mk,data_pks dpks where mk.pks_id = dpks.data_pks_id ORDER BY mk_master_kontrak_id DESC',
                            'options' => array('' => '-----Pilih Master Kontrak-----'),
                            'auto_complete' => 1,
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'desa_id',
                            'label' => 'Kode Desa',
                            'id' => 'desa_id',
                            'value' => '',
                            'type' => 'input_mce',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          )
                    );
		$configs = array(	'config_table' =>	array(
																					'table' => 'mk_desa_lokasi',
																					'fields_insert' => $fields_insert,
																					'fields_where' => '',
                                          'primary_key' => 'desa_lokasi_id',
																					'foreign_key' => '',
																					'sub_tables' => array()
																				),
											'controller' => '',
											'function' => 'import',
                      'upload' => array(
																'upload_path' => './uploads/importer/mk_desa_lokasi/',
																'encrypt_name' => false,
																'allowed_types' =>  'xls|xlsx'
																)
										);
										
		$this->config_import = $configs;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Desa / Kelurahan - Lokasi Proyek";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit  Desa / Kelurahan - Lokasi Proyek";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }

  function get_vendor_data($master_kontrak_id = "")
  {
    $return = array('mk_master_vendor_id' => '', 'nama_perusahaan' => '');
    if(empty($master_kontrak_id)){
      echo json_encode($return);
      die;
    }
    $get_kontrak = $this->db->get_where('mk_master_kontrak', array('mk_master_kontrak_id' => $master_kontrak_id))->row_array();
    $master_vendor_id = (isset($get_kontrak['master_vendor_id']))?$get_kontrak['master_vendor_id']:"";
    $get = $this->db->get_where('mk_master_vendor', array('mk_master_vendor_id' => $master_vendor_id))->row_array();
    if(empty($get)){
      echo json_encode($return);
      die;
    }else{
      $return = array(
        'mk_master_vendor_id' => $get['mk_master_vendor_id'],
        'nama_perusahaan' => $get['perusahaan'],
      );
      echo json_encode($return);
      die;
    }

  }

  function get_kota_data($propinsi_id = "")
  {

    $return = '';
    if(empty($propinsi_id)){
      echo $return;
      die;
    }

    $this->db->order_by('nama_kota', 'ASC');
    $get = $this->db->get_where('data_kota', array('propinsi_id' => $propinsi_id))->result_array();
    if(empty($get)){
      echo json_encode($return);
      die;
    }else{
      foreach($get as $idx => $data)
       {
        $return .= "<option value='". $data['data_kota_id'] ."'>".$data['dat2']." ".$data['nama_kota']."</option>";
       }
      echo $return;
      die;
    }

  }


  function _hook_importer_do_insert_data_before_insert_mk_desa_lokasi($param = array())
  {
		$kode_desa = (isset($param['desa_id']))?$param['desa_id']:"";
		$q = $this->db->query("SELECT * FROM data_desa WHERE kode_desa = '".$kode_desa."'");
		$desa = $q->row_array();
    
		$nomor_kontrak = (isset($param['master_kontrak_id']))?$param['master_kontrak_id']:"";
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE nomor_kontrak = '".$nomor_kontrak."'");
		$kontrak = $q->row_array();
		
		$param['master_kontrak_id'] = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
		$param['pks_id'] = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
		$param['master_vendor'] = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
		$param['desa_id'] = (isset($desa['data_desa_id']))?$desa['data_desa_id']:0;
		$param['propinsi_id'] = (isset($desa['propinsi_id']))?$desa['propinsi_id']:0;
		$param['kota_id'] = (isset($desa['kota_id']))?$desa['kota_id']:0;
		$param['kecamatan_id'] = (isset($desa['kecamatan_id']))?$desa['kecamatan_id']:0;
		return $param;
  }
  
  function _hook_importer_do_insert_data_after_insert_mk_spesifikasi($data_inserted_tbl = array())
  {
    
  }
  
  function _hook_create_listing_value_master_vendor($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_vendor WHERE mk_master_vendor_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_vendor']) and isset($d['perusahaan']))?$d['kode_vendor'].' - '.$d['perusahaan']:$default_value;
  }
  
  function _hook_create_listing_value_master_kontrak_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['nomor_kontrak']) and isset($d['judul_kontrak']))?$d['nomor_kontrak'].' - '.$d['judul_kontrak']:$default_value;
  }
  
  function _hook_create_listing_value_pks_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_pks WHERE data_pks_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_pks']) and isset($d['judul_pks']))?$d['kode_pks'].' - '.$d['judul_pks']:$default_value;
  }
  
  function _hook_create_listing_value_desa_id($default_value = ""){
    $q = $this->db->query("SELECT * FROM data_desa WHERE data_desa_id = '".$default_value."'");
    $d = $q->row_array();
    return (isset($d['kode_desa']) and isset($d['nama_desa']))?$d['kode_desa'].' - '.$d['nama_desa']:$default_value;
  }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
