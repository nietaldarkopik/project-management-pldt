<?php
class Model_data_kecamatan extends Master_model {
  var $table = 'data_kecamatan';
  var $primary_key = 'data_kecamatan_id';
  var $orderby = 'nama_kecamatan';
  var $order = 'ASC';

}
