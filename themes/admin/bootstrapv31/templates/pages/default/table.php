			<table width="100%" cellpadding="0" cellspacing="0" border="1">
				<thead>
					<tr>
						<th align="center" width="35">No.</th>
						<th align="center" width="140">NAMA PESERTA</th>
						<th align="center" width="125">NIK</th>
						<th align="center" width="121">No. KTP</th>
						<th align="center" width="150">TEMPAT / TGL. LAHIR</th>
						<th align="center" width="68">PENDIDIKAN TERAKHIR</th>
						<th align="center" width="180">INSTANSI PENGUTUS / ALAMAT</th>
						<th align="center" width="110">JABATAN</th>
						<th align="center" width="220">Alamat Kantor</th>
						<th align="center" width="120">Telp. Kantor</th>
						<th align="center" width="80">Predikat</th>
					</tr>
				</thead>				
				<tbody>
					<?php
						$get_data = $this->db->query($query)->result_array();
						$data = array();
						if(!empty($get_data)){
							foreach($get_data as $idx => $dt){
								$get_lembaga = $this->db->get_where('lembaga', array('lembaga_id' => $dt['lembaga']))->row_array();
					?>
								<tr>
									<td align="center" width="35"><?php echo ($idx + 1);?></td>
									<td width="140"><?php echo $dt['nama'];?></td>
									<td width="125" align="center"><?php echo $dt['nip'];?></td>
									<td width="121" align="center"><?php echo $dt['no_ktp'];?></td>
									<td width="150" align="center"><?php echo $dt['tempat_lahir'].' / '.$dt['tanggal_lahir'];?></td>
									<td width="68" align="center"><?php echo $dt['pendidikan_terakhir'];?></td>
									<td width="180" align="center"><?php echo (!empty($get_lembaga)) ? $get_lembaga['nmlembaga'] : $dt['lembaga'];?></td>
									<td width="110" align="center"><?php echo $dt['jabatan'];?></td>
									<td width="220"><?php echo $dt['alamat_kantor'];?></td>
									<td width="120"><?php echo $dt['telepon_kantor'];?></td>
									<td width="80" align="center">&nbsp;</td>
								</tr>

					<?php
							}
						}
					?>
					</tbody>
				</table>
