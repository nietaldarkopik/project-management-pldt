<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

  $user_id = $this->user_access->current_user_id;
  $current_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (user_custom_menus.path = '".$this->uri->segment(1)."' AND user_custom_menus.controller = '".$this->uri->segment(2)."' AND (user_custom_menus.function = 'index' or user_custom_menus.function = 'listing') AND `type` = 'home' AND user_custom_menus.status = 'active')");
  $current_menu_allowed = (isset($current_menu_allowed[0]))?$current_menu_allowed[0]:array();
  $parent_menu_allowed = array();
  if(isset($current_menu_allowed['user_menu_id']))
  {
    $parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (parent_menu = '".$current_menu_allowed['user_menu_id']."' AND user_custom_menus.status = 'active') ");
    if(is_array($parent_menu_allowed) and count($parent_menu_allowed) == 0)
    {
      $parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (parent_menu = '".$current_menu_allowed['parent_menu']."' AND user_custom_menus.status = 'active') ");
    }
  }
?>

<?php
	$is_full_page = true;
if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
{
	$is_full_page = false;
?>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="left-side sidebar-offcanvas">
		<div class="row hide">
			<div class="col-lg-12">
				<a href="#" class="btn btn-flat hidden-xs hidden-sm hidden-md btn-block sidebar-toggle left-sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="glyphicon glyphicon-transfer"></span>
				</a>
			</div>
		</div>
		<!-- sidebar -->
		<section class="sidebar">
			<!-- Panel Description -->
			<div class="well-sm panel-info"><small></small></div>

			<!-- search form -->
			<form action="#" method="get" class="sidebar-form hide">
				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="Search..."/>
					<span class="input-group-btn">
						<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>
			<!-- /.search form -->
			<br/>
			<br/>
			<!-- sidebar-menu -->
			<ul class="sidebar-menu">
				<?php
				if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
				{
				  foreach($parent_menu_allowed as $i => $m)
				  {
            $path = $this->uri->segment(1);
            $controller = $this->uri->segment(2);
            $function = $this->uri->segment(3);
            $active = ($path == $m['path'] and $controller == $m['controller'] and $function == $m['function'])?' leftmenu-list active ':'';
				?>
					<li class="<?php echo $active;?>">
						<a href="<?php echo base_url($m['path']."/".$m['controller'].'/'.$m['function']);?>">
							<i class="<?php echo $m['attributes'];?>"></i> <span><?php echo $m['menu_title'];?></span>
						</a>
					</li>
				<?php                        
				  }
				}
				?>
			</ul>
			<!-- /.sidebar-menu -->
			
		</section>               
		<!-- /.sidebar -->
	</aside>
  <?php
}
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side <?php echo ($is_full_page !== false)?"margin0":"";?>">
