<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}

$this->db->where(array("ip_invoice_id" => $this->uri->segment(4)));
$data_invoice = $this->db->get("ip_invoice")->row_array();
$master_kontrak_id = (isset($data_invoice['master_kontrak_id']))?$data_invoice['master_kontrak_id']:"";

$this->db->where(array("invoice_id" => $this->uri->segment(4)));
$data_invoice_detail = $this->db->get("ip_invoice_detail")->result_array();

$this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
$data_master_kontrak = $this->db->get("mk_master_kontrak")->row_array();
$vendor_id = (isset($data_master_kontrak['master_vendor_id']))?$data_master_kontrak['master_vendor_id']:"";
$sumber_pendanaan_id = (isset($data_master_kontrak['sumber_pendanaan_id']))?$data_master_kontrak['sumber_pendanaan_id']:"";

$this->db->where(array("mk_master_vendor_id" => $vendor_id));
$data_vendor = $this->db->get("mk_master_vendor")->row_array();

$this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
$data_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
          <ul class="nav nav-tabs margintop-1 marginleft-16 no-print">
          <?php
            echo $this->data->show_panel_allowed("","admin","",array(array("title" => "Listing","name" => "listing","class"=>"glyphicon-list")),"",false);
          ?>
          </ul>
          <div class="clearfix"></div><br/>
          <div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12 text-center">
					<h3><strong>INVOICE</strong></h3>
				</div>
				<div class="col-lg-8 pull-left">
					<?php echo (isset($data_vendor['perusahaan']))?$data_vendor['perusahaan']:"";?><br/>
					<?php echo (isset($data_vendor['alamat_perusahaan']))?$data_vendor['alamat_perusahaan']:"";?><br/>
					Kepada Yth :<br/>
					<?php echo (isset($data_sumber_pendanaan['instansi']))?$data_sumber_pendanaan['instansi']:"";?><br/>
					<?php echo (isset($data_sumber_pendanaan['alamat_instansi']))?$data_sumber_pendanaan['alamat_instansi']:"";?><br/>
				</div>
				<div class="col-lg-4 pull-right">
					<table width="100%">
						<tbody>
							<tr><td width="30%">Tanggal Invoice</td><td width="5%">:</td><td align="right"><?php echo (isset($data_invoice['tanggal_invoice']))?$this->data->human_date($data_invoice['tanggal_invoice']):"-";?></td></tr>
							<tr><td>No Invoice</td><td>:</td><td align="right"><?php echo (isset($data_invoice['no_invoice']))?$data_invoice['no_invoice']:"-";?></td></tr>
							<tr><td>No Kontrak</td><td>:</td><td align="right"><?php echo (isset($data_master_kontrak['nomor_kontrak']))?$data_master_kontrak['nomor_kontrak']:"-";?></td></tr>
							<tr><td>No PO</td><td>:</td><td align="right"><?php echo (isset($data_invoice['no_po']))?$data_invoice['no_po']:"-";?></td></tr>
							<tr><td>No. BAUT</td><td>:</td><td align="right"><?php echo (isset($data_invoice['no_baut']))?$data_invoice['no_baut']:"-";?></td></tr>
							<tr><td>NPWP</td><td>:</td><td align="right"><?php echo (isset($data_invoice['npwp']))?$data_invoice['npwp']:"-";?></td></tr>
							<tr><td>PKP</td><td>:</td><td align="right"><?php echo (isset($data_invoice['pkp']))?$data_invoice['pkp']:"-";?></td></tr>
						</tbody>
					</table>
				</div>
			  </div>
          </div>
			<br class="clearfix"/>
          <div class="row">
              <div class="col-lg-12">
                <table class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th>
                                <strong>No</strong>
                            </th>
                            <th>
                                <strong>Keterangan</strong>
                            </th>
                            <th>
                                <strong>Volume</strong>
                            </th>
                            <th>
                                <strong>Harga Per-unit</strong>
                            </th>
                            <th>
                                <strong>Jumlah</strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        $grand_total = 0;
                        if(is_array($data_invoice_detail) and count($data_invoice_detail) > 0)
                        {
                            foreach($data_invoice_detail as $i => $detail)
                            {
                                $sub_total = $detail['volume']*$detail['harga_perunit'];
                                $grand_total += $sub_total;
                        ?>
                                <tr>
                                    <td><?php echo $no;?></td>
                                    <td><?php echo $detail['keterangan'];?></td>
                                    <td style="text-align: right;"><?php echo $detail['volume'];?> <?php echo $detail['satuan'];?></td>
                                    <td style="text-align: right;"><?php echo format_currency($detail['harga_perunit'],"Rp. ");?></td>
                                    <td style="text-align: right;"><?php echo format_currency($sub_total,"Rp. ");?></td>
                                </tr>
                        <?php
                            $no++;
                            }
                        }
                        ?>
                        <?php
                        /*
                        <tr>
                            <td colspan="3" rowspan="3">&nbsp;</td>
                            <td>Sub Jumlah</td>
                            <td><?php echo $grand_total;?></td>
                        </tr>
                        <tr>
                            <td>PPN</td>
                            <td>&nbsp;</td>
                        </tr>
                       */
                       ?>
                        <tr>
                            <td colspan="4" style="text-align: right;"><strong>Total</strong></td>
                            <td style="text-align: right;"><strong><?php echo format_currency($grand_total,"Rp. ");?></strong></td>
                        </tr>
                    </tbody>
                </table>
              </div>
          </div>
          <div class="row">
                <div class="col-lg-4 pull-left">
                    No. Rekening : <?php echo (isset($data_invoice['no_rekening_bank']))?$data_invoice['no_rekening_bank']:"-";?><br/>
                    A/N <?php echo (isset($data_invoice['nama_rekening_bank']))?$data_invoice['nama_rekening_bank']:"-";?><br/>
                    <?php echo (isset($data_invoice['nama_bank']))?$data_invoice['nama_bank']:"-";?> 
                    <?php echo (isset($data_invoice['nama_cabang']))?" Cabang " . $data_invoice['nama_cabang']:"-";?><br/>
                    <?php echo (isset($data_invoice['alamat_cabang']))?$data_invoice['alamat_cabang']:"-";?><br/>
                </div>
				<div class="col-lg-4 text-center pull-right">
					Hormat Kami,<br/>
					<?php echo (isset($data_invoice['jabatan']))?$data_invoice['jabatan']:"-";?><br/><br/><br/>
					<?php echo (isset($data_invoice['nama']))?$data_invoice['nama']:"-";?><br/>
					<?php echo (isset($data_invoice['nip']))?$data_invoice['nip']:"-";;?>
				</div>
          </div>
          
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
