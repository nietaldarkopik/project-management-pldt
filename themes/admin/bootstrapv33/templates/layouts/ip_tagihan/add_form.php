<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$is_ajax = $this->input->post('is_ajax');
	$is_modal = $this->input->post('is_modal');
	$pagination_data_target = "#data_target-".$is_ajax;

	$this->db->where(array("ip_invoice_id" => $this->uri->segment(4)));
	$data_invoice = $this->db->get("ip_invoice")->row_array();
	$master_kontrak_id = (isset($data_invoice['master_kontrak_id']))?$data_invoice['master_kontrak_id']:"";

	$this->db->where(array("invoice_id" => $this->uri->segment(4),"invoice_id >" => 0));
	$data_invoice_detail = $this->db->get("ip_invoice_detail")->result_array();
	
	$this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
	$data_master_kontrak = $this->db->get("mk_master_kontrak")->row_array();
	$vendor_id = (isset($data_master_kontrak['master_vendor_id']))?$data_master_kontrak['master_vendor_id']:"";
	$sumber_pendanaan_id = (isset($data_master_kontrak['sumber_pendanaan_id']))?$data_master_kontrak['sumber_pendanaan_id']:"";

	$this->db->where(array("mk_master_vendor_id" => $vendor_id));
	$data_vendor = $this->db->get("mk_master_vendor")->row_array();

	$this->db->where(array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
	$data_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan")->row_array();
	
	$form_fields = $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init,true,true);
	
	$form_errors = "";
	$form_errors = form_error('data[master_kontrak_id]');
?>
	<div class="row">
		<div class="col-lg-12">
			<br/>
			<?php
			echo $form_errors;
			echo $form_fields['tag_form']['open'];
			?>
			<br class="fclear"/><br/>
			<div class="row">
				<div class="col-lg-4">
					<?php echo $form_fields['logo'];?>
					<?php echo (isset($data_vendor['alamat_perusahaan']))?$data_vendor['alamat_perusahaan']:"";?><br>
					Kepada Yth :<br>
					<?php echo (isset($data_sumber_pendanaan['instansi']))?$data_sumber_pendanaan['instansi']:"";?><br>
					<?php echo (isset($data_sumber_pendanaan['alamat_instansi']))?$data_sumber_pendanaan['alamat_instansi']:"";?><br>
				</div>
				<div class="col-lg-3">
					<h3><strong>INVOICE</strong></h3>
				</div>
				<div class="col-lg-5">
					<table width="100%">
						<tbody>
							<tr><td width="30%">Tanggal Invoice</td><td width="5%">:</td><td><?php echo $form_fields['tanggal_invoice'];?></td></tr>
							<tr><td>No Invoice</td><td>:</td><td><?php echo $form_fields['no_invoice'];?><?php #echo (isset($data_invoice['no_invoice']))?$data_invoice['no_invoice']:"-";?></td></tr>
							<tr><td>No Kontrak</td><td>:</td><td><div class="input-group"><span class="form-control padding0"><?php echo $form_fields['master_kontrak_id'];?></span><span class="input-group-addon"><span class="glyphicon glyphicon glyphicon-asterisk"></span></span></div><?php #echo (isset($data_master_kontrak['no_kontrak']))?$data_master_kontrak['no_kontrak']:"-";?></td></tr>
							<tr><td>No PO</td><td>:</td><td><?php echo $form_fields['no_po'];?><?php #echo (isset($data_invoice['no_po']))?$data_invoice['no_po']:"-";?></td></tr>
							<tr><td>No. BAUT</td><td>:</td><td><?php echo $form_fields['no_baut'];?><?php #echo (isset($data_invoice['no_baut']))?$data_invoice['no_baut']:"-";?></td></tr>
							<tr><td>NPWP</td><td>:</td><td><?php echo $form_fields['npwp'];?><?php #echo (isset($data_invoice['npwp']))?$data_invoice['npwp']:"-";?></td></tr>
							<tr><td>PKP</td><td>:</td><td><?php echo $form_fields['pkp'];?><?php #echo (isset($data_invoice['pkp']))?$data_invoice['pkp']:"-";?></td></tr>
						</tbody>
					</table>
				</div>
			</div>
			<br class="clearfix"/>
			<br/>
			<div class="row">
				<div class="col-lg-12">
					<button name="add_new_item" type="button" id="add_new_item" class="pull-right btn btn-primary btn-sm" aria-label="Left Align"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Item</button>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover table-condensed" width="100%" id="invoice_item_list">
							<thead>
								<tr>
									<th width="5%">
										&nbsp;
									</th>
									<th width="5%">
										<strong>No</strong>
									</th>
									<th width="30%">
										<strong>Keterangan</strong>
									</th>
									<th width="20%">
										<strong>Volume</strong>
									</th>
									<th width="20%">
										<strong>Harga Per-unit</strong>
									</th>
									<th width="20%">
										<strong>Jumlah</strong>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr class="master_row hidden">
									<td class="text-center padding0 action_row_item">
										<button name="remove_item" type="button" class="btn btn-primary btn-xs btn-danger remove_item" aria-label="Left Align">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</td>
									<td class="text-center number_row_item">&nbsp;</td>
									<td class="padding0 keterangan_row_item">
										<textarea name="data_items_keterangan" class="form-control data_items_keterangan" rows="2" placeholder="Nama Barang/Jasa"></textarea>
									</td>
									<td class="padding0 volume_row_item" style="text-align: right;">
										<input type="text" name="data_items_volume" value="" placeholder="Jumlah" class="form-control data_items_volume pull-left" style="width:48%">
										<input type="text" name="data_items_satuan" value="" placeholder="Satuan" class="form-control data_items_satuan pull-right" style="width:50%">
									</td>
									<td class="padding0 harga_perunit_row_item" style="text-align: right;">
										<div class="input-group">
											<span class="input-group-addon">
											  <span class="glyphicon">Rp. </span>
											</span>
											<input type="text" name="data_items_harga_perunit" value="" placeholder="Harga Perunit" class="form-control data_items_harga_perunit">
										</div>
									</td>
									<td style="text-align: right;" class="total_row_item harga_perunit_row_item volume_row_item number_row_item action_row_item">&nbsp;</td>
								</tr>
								<?php
								$no = 1;
								$grand_total = 0;
								if(is_array($data_invoice_detail) and count($data_invoice_detail) > 0)
								{
									foreach($data_invoice_detail as $i => $detail)
									{
										$sub_total = $detail['volume']*$detail['harga_perunit'];
										$grand_total += $sub_total;
								?>
										<tr>
											<td class="text-center padding0">
												<button name="remove_item" type="button" class="btn btn-primary btn-xs btn-danger remove_item" aria-label="Left Align">
													<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												</button>
											</td>
											<td class="text-center">
												<strong class="no_item"><?php echo $no;?></strong>
											</td>
											<td><?php echo $detail['keterangan'];?></td>
											<td style="text-align: right;"><?php echo $detail['volume'];?><?php echo $detail['satuan'];?></td>
											<td style="text-align: right;"><?php echo format_currency($detail['harga_perunit'],"Rp. ");?></td>
											<td style="text-align: right;"><?php echo format_currency($sub_total,"Rp. ");?></td>
										</tr>
								<?php
									$no++;
									}
								}
								?>
								<?php
								/*
								<tr>
									<td colspan="3" rowspan="3">&nbsp;</td>
									<td>Sub Jumlah</td>
									<td><?php echo $grand_total;?></td>
								</tr>
								<tr>
									<td>PPN</td>
									<td>&nbsp;</td>
								</tr>
							   */
							   ?>
								<tr class="total_row">
									<td colspan="5" style="text-align: right;"><strong>Total</strong></td>
									<td style="text-align: right;" class="grand_total_price"><strong><?php echo format_currency($grand_total,"Rp. ");?></strong></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					No. Rekening : <?php echo $form_fields['no_rekening_bank'];?><?php #echo (isset($data_invoice['no_rekening']))?$data_invoice['no_rekening']:"-";?><br/>
					Atas Nama : <?php echo $form_fields['nama_rekening_bank'];?><?php #echo (isset($data_invoice['nama_rekening']))?$data_invoice['nama_rekening']:"-";?><br/>
					Nama Bank : <?php echo $form_fields['nama_bank'];?><?php #cho (isset($data_invoice['nama_bank']))?$data_invoice['nama_bank']:"-";?><br/>
					Nama Cabang : <?php echo $form_fields['nama_cabang'];?><?php #echo (isset($data_invoice['nama_cabang']))?$data_invoice['nama_cabang']:"-";?><br/>
					Alamat Cabang : <?php echo $form_fields['alamat_cabang'];?><?php #echo (isset($data_invoice['alamat_cabang']))?$data_invoice['alamat_cabang']:"-";?><br/>
				</div>
				<div class="col-lg-4">&nbsp;</div>
				<div class="col-lg-4 text-center"><br/><br/><br/><br/><br/><br/><br/><br/>
					Hormat Kami,<br/>
					<?php echo $form_fields['jabatan'];?><br/><br/><br/>
					<?php echo $form_fields['nama'];?><br/>
					<?php echo $form_fields['nip'];?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<button name="input_submit" type="submit" id="input_submit" class=" required btn btn-primary btn-block">Save</button>
				</div>
			</div>
			<?php
			echo $form_fields['action_hidden'];
			echo $form_fields['tag_form']['close'];
			?>
		</div>
	</div>
