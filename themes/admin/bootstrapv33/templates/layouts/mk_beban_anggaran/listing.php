<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
                <!--<li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>-->
                <li class="hide"><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li>
                <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li>
                <!--<li><a href="#export<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-export"></span></a></li>-->
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <?php
                    $filters = $this->session->userdata("mk_beban_anggaran_data_filter");
                    $filter_master_kontrak_id = (isset($filters['master_kontrak_id']))?$filters['master_kontrak_id']:"";
                    
                    if(empty($filter_master_kontrak_id))
                    {
                      echo '<div class="alert alert-warning">Silahkan pilih data kontrak terlebih dahulu</div>';
                    }else
                    {
                  ?>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      $listing =  $this->data->create_listing($this->init);
                      $data_rows = $this->data->data_rows;
                    ?>
                    
                    <table width="100%" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th class="no-print bulk_data_all_th" align="center" valign="middle" width="5">
                            <input name="bulk_data_all" value="1" class="bulk_data_all" type="checkbox" />
                          </th>
                          <th>No</th>
                          <th>Master Kontrak</th>
                          <th>PKS</th>
                          <th>Vendor</th>
                          <th>Sumber Pendanaan</th>
                          <th class="bg-success">Beban Anggaran</th>
                          <th class="bg-success">Dana Alokasi</th>
                          <th class="action_menu_col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $row_output = "";
                        if(is_array($data_rows) and count($data_rows) > 0)
                        {
                          $total_beban_anggaran = 0;
                          $total_dana_alokasi = 0;
                          
                          foreach($data_rows as $i => $rows)
                          {                                              
                            $path = (isset($this->init['path']))?$this->init['path']:'';
                            $controller = (isset($this->init['controller']))?$this->init['controller']:'';
                            $function = (isset($this->init['function']))?$this->init['function']:'';
                            $panel_function = (isset($this->init['panel_function']))?$this->init['panel_function']:array("edit","view","delete");
                            $action = $this->data->show_panel_allowed("",$path,$controller,$panel_function);
                            $action_btn = "";
                            if(!empty($action))
                            {
                              $action = $this->data->show_panel_allowed("",$path,$controller,$panel_function,$rows[$this->data->primary_key]);
                              $list_style = (isset($this->data->the_config['list_style']))?$this->data->the_config['list_style']:"class='rows_action'";
                              $action  = $this->hook->do_action('hook_create_listing_action',$action);
                              $action_btn = '<td '.$list_style.'>'. $action .'</td>';
                            }
                            
                            $currency = 'Rp. ';
                            $total_beban_anggaran += $rows['beban_anggaran'];
                            $total_dana_alokasi += $rows['dana_alokasi'];
                            $row_output .= '
                            <tr>
                              <td align="center" valign="middle" class="no-print" width="5">
                                <input type="checkbox" name="bulk_data[]" value="'.$rows['mk_beban_anggaran_id'].'" class="bulk_data"/>
                              </td>
                              <td>'.($i+1).'</td>
                              <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_master_kontrak_id',$rows['master_kontrak_id']).'</td>
                              <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_pks_id',$rows['pks_id']).'</td>
                              <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_master_vendor_id',$rows['master_vendor_id']).'</td>
                              <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_sumber_pendanaan_id',$rows['sumber_pendanaan_id']).'</td>
                              <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['beban_anggaran'],$currency).'</td>
                              <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['dana_alokasi'],$currency).'</td>'.
                              $action_btn.'
                            </tr>';
                          }
                            $row_output .= '
                            <tr>
                              <td colspan="6" align="right" class="bg-blue">TOTAL</td>
                              <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($total_beban_anggaran,$currency).'</td>
                              <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($total_dana_alokasi,$currency).'</td>
                              <th class="bg-blue">&nbsp;</td>
                            </tr>';
                        }
                        else
                        {
                          
                        }
                        echo $row_output;
                        ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                <?php
                  }
                ?>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                   
                  <?php
                    #$init = $this->init;
                    #echo $this->data->create_form_filter($init);
                  ?> 
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
						<h3>Import Data</h3>
						<p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
						<?php
						#$init = $this->init;
						#echo $this->data->create_form_import($init);
						echo $this->importer->upload_file_form();
						?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="export<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_export($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

