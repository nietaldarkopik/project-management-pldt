<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
          <ul class="nav nav-tabs margintop-1 marginleft-16">
          <?php
            echo $this->data->show_panel_allowed("","admin","",array("listing"),"",false);
          ?>
          </ul>
          <br class="fclear"/><br/>
          <form id="form" action="#" method="post" class="form">
            <fieldset>
            <legend>View Data</legend>  
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">ID</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['desa_lokasi_id'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Jenis PKS</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['judul_pks'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Vendor</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['perusahaan'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Nomor Kontrak</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo '['. $data_lokasi_proyek['nomor_kontrak'] .'] '. $data_lokasi_proyek['judul_kontrak'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Propinsi</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['nama_propinsi'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id"><?php echo ($data_lokasi_proyek['dat2'] == 'Kota') ? 'Kota' : 'Kabupaten';?></label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['nama_kota'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Kecamatan</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['nama_kecamatan'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>
                <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group">
                        <label for="desa_lokasi_id">Desa / Kelurahan</label>
                      </div>
                    </div>
                  <div class="col-lg-9">
                    <div class="input-group">
                      <?php echo $data_lokasi_proyek['nama_desa'];?>
                    </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <br>

                <br class="fclear">
              </fieldset>
            </form>

        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
