<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

	<div class="panel panel-default">
	  <div class="panel-heading">
      <h3 class="panel-title"><?php echo $page_title;?></h3>
    </div>
	  <div class="panel-body">
			<?php
				#echo $response;
				#echo $this->data->create_form_filter();
			?>
    </div>
    <?php
      echo $this->data->show_panel_allowed("","admin","",array("add"));
    ?>
    <?php
      echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array("delete")));
    ?>
    <ul class="nav nav-tabs">
      <li class="active"><a href="#listing" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
      <li><a href="#add" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
      <li><a href="#config" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
      <li><a href="#config" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
      <li><a href="#config" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>
    </ul>
    <div id='content' class="tab-content">
      <div class="tab-pane active" id="listing">
        <div class="table-responsive">
          <?php
            echo $this->data->create_listing();
            $paging_config = (isset($paging_config))?$paging_config:array();
            echo $this->data->create_pagination($paging_config);
          ?>
        </div>
      </div>
      <div class="tab-pane" id="add">
        <ul>
            <li>profile</li>
            <li>profile</li>
            <li>profile</li>
            <li>profile</li>
            <li>profile</li>
            <li>profile</li>
            <li>profile</li>
        </ul>
      </div>
      <div class="tab-pane" id="config">asdf</div>
    </div>
	</div>


<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>

