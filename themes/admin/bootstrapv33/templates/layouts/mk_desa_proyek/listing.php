<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
                <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li>
              </ul>
              <div id='content' class="tab-content">
                
                <!-- TAB LIST -->
                <?php
                /*
                <div class="tab-pane active" id="listing">
                  <br/>            
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table width="100%" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th style="width:10%;">No</th>
                          <th style="width:20%;">Jenis PKS</th>
                          <th style="width:20%;">Vendor</th>
                          <th style="width:15%;">Nomor Kontrak</th>
                          <th style="width:20%;">Lokasi Proyek</th>
                          <th style="width:15%;" class="action_menu_col">Action</th>
                        </tr>
                      </thead>        
                      
                      <tbody>
                        <?php if(!empty($data_lokasi_proyek)):?>
                          <?php
                            $no = 1;
                            foreach($data_lokasi_proyek as $idx => $lokasi_proyek):?>
                            <tr>
                              <td><?php echo $no;$no++;?></td>
                              <td><?php echo $lokasi_proyek['judul_pks'];?></td>
                              <td><?php echo $lokasi_proyek['perusahaan'];?></td>
                              <td><?php echo '['. $lokasi_proyek['nomor_kontrak']. '] '. $lokasi_proyek['judul_kontrak'];?></td>
                              <td>
                                Desa : <?php echo $lokasi_proyek['nama_desa'];?><br>
                                Kecamatan : <?php echo $lokasi_proyek['nama_kecamatan'];?><br>
                                <?php echo $lokasi_proyek['dat2'];?> : <?php echo $lokasi_proyek['nama_kota'];?><br>
                                Propinsi : <?php echo $lokasi_proyek['nama_propinsi'];?><br>
                              </td>
                              
                              <td class="rows_action">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Choose Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                      <span class="caret"></span>
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
                                      <li role="presentation" class="">
                                        <a role="menuitem" tabindex="-1" href="<?php echo site_url('admin/mk_desa_proyek/edit/'.$lokasi_proyek['desa_lokasi_id']);?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Edit</a>
                                      </li>
                                      <li role="presentation" class="">
                                        <a role="menuitem" tabindex="-1" href="<?php echo site_url('admin/mk_desa_proyek/view/'.$lokasi_proyek['desa_lokasi_id']);?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> View</a>
                                      </li>
                                      <li role="presentation" class="">
                                        <a role="menuitem" tabindex="-1" href="<?php echo site_url('admin/mk_desa_proyek/delete/'.$lokasi_proyek['desa_lokasi_id']);?>" class="glyphicon-cog icon glyphicon ajax" data-target-ajax=".ajax_container"> Delete</a>
                                      </li>
                                    </ul>
                                </div>
                              </td>
                            </tr>
                          <?php endforeach;?>
                        <?php endif;?>  
                      </tbody>
                    </table>                  
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                */?>
                
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      echo $this->data->create_listing($this->init);
                    ?>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                
                <!-- TAB ADD DATA -->
                <div class="tab-pane" id="add">
                  <form method="post" class="form ajax" action="<?php echo site_url('admin/mk_desa_proyek/add');?>" id="form" data-target-ajax=".tab-content #add" enctype="multipart/form-data"><h3>Tambah Desa / Kelurahan - Lokasi Proyek</h3>
                    <br>
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="pks_id">Jenis PKS</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_insert[pks_id]" id="pks_id">

                            <option value="0">-----Pilih Jenis PKS-----</option>
                              <?php if(!empty($option_pks)):?>
                                <?php foreach($option_pks as $pks_id => $pks_value):?>
                                <option value="<?php echo $pks_id;?>"><?php echo $pks_value;?></option>
                              <?php endforeach;?>
                              <?php endif;?>
                            </select> 
                          </span>
                          
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                        
                      </div>
                    </div>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="master_kontrak_id">Nomor Kontrak</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_insert[master_kontrak_id]" id="master_kontrak_id">
                              <option value="0">-----Pilih Nomor Kontrak-----</option>
                            </select> 
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                      jQuery(document).ready(function(){
                        jQuery('#pks_id').on("change",function(){
                          var foreign_key = jQuery(this).val();
                          jQuery.ajax({
                            url: "<?php echo site_url('services/get_options');?>",
                            type: "post",
                            data: "foreign_key=pks_id&table=mk_master_kontrak&select=nomor_kontrak label,mk_master_kontrak_id value&value=&fk="+foreign_key,
                            success: function(msg){
                              var first_row = jQuery("#master_kontrak_id").find("option").eq(0).clone();
                              jQuery("#master_kontrak_id").html("");
                              jQuery("#master_kontrak_id").append(jQuery(first_row));
                              jQuery("#master_kontrak_id").append(msg);
                              jQuery("#display_vendor").val('');
                              jQuery("#vendor").val('');
                            
                            }
                          });
                        });
                      });
                    </script>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="display_vendor">Vendor</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <input type="text" name="data_display[vendor]" value="" label="Vendor" class="form-control" readonly="readonly" id="display_vendor">
                          <input type="hidden" name="data_insert[master_vendor]" value="" id="vendor">
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div><!-- /.col-lg-6 -->
                    </div>
                    <script type="text/javascript">
                      jQuery(document).ready(function(){
                        jQuery('#master_kontrak_id').on("change",function(){
                          var master_kontrak_id = jQuery(this).val();
                          jQuery.ajax({
                            url: "<?php echo site_url('admin/mk_desa_proyek/get_vendor_data/');?>" +'/'+ master_kontrak_id,
                            success: function(msg){
                              var dt_vendor = jQuery.parseJSON(msg);
                              var id_vendor = dt_vendor.mk_master_vendor_id;
                              var nm_vendor = dt_vendor.nama_perusahaan;
                              if(id_vendor.length == 0){
                                alert("Silahkan tambahkan data vendor terlebih dahulu pada kontrak yang terpilih");
                                jQuery("#display_vendor").val('');
                                jQuery("#vendor").val('');
                              }else{
                                jQuery("#display_vendor").val(nm_vendor);
                                jQuery("#vendor").val(id_vendor);
                                
                              }                          
                            }
                          });
                        });
                      });
                    </script>  
                    <br>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="input-group">
                          <label>&nbsp;</label>
                        </div>
                      </div>
                    </div>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="propinsi_id">Propinsi</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_display[propinsi_id]" id="propinsi_id">
                              <option value="0">-----Pilih Propinsi-----</option>
                              <?php if(!empty($option_propinsi)):?>
                                <?php foreach($option_propinsi as $data_propinsi_id => $nama_propinsi):?>
                                  <option value="<?php echo $data_propinsi_id;?>"><?php echo $nama_propinsi;?></option>
                                <?php endforeach;?>
                              <?php endif;?>
                            </select> 
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="kota_id">Kota / Kabupaten</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_display[kota_id]" id="kota_id">
                              <option value="0">-----Pilih Kota / Kabupaten-----</option>
                            </select> 
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div>
                    </div>
                     <script type="text/javascript">
                        jQuery(document).ready(function(){
                          jQuery('#propinsi_id').on("change",function(){
                            var propinsi_id = jQuery(this).val();
                            jQuery.ajax({
                              url: "<?php echo site_url('admin/mk_desa_proyek/get_kota_data/');?>" +'/'+ propinsi_id,
                              success: function(msg){
                                var first_row_kota = jQuery("#kota_id").find("option").eq(0).clone();
                                var first_row_kecamatan = jQuery("#kecamatan_id").find("option").eq(0).clone();
                                var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                                jQuery("#kota_id").html("");
                                jQuery("#kota_id").append(jQuery(first_row_kota));
                                jQuery("#kota_id").append(msg);

                                jQuery("#kecamatan_id").html("");
                                jQuery("#kecamatan_id").append(jQuery(first_row_kecamatan));
                                
                                jQuery("#desa_id").html("");
                                jQuery("#desa_id").append(jQuery(first_row_desa));
                                
                              }
                            });
                          });
                        });
                     </script>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="kecamatan_id">Kecamatan</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_display[kecamatan_id]" id="kecamatan_id">
                              <option value="0">-----Pilih Kecamatan-----</option>
                            </select> 
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div>
                    </div>
                     <script type="text/javascript">
                        jQuery(document).ready(function(){
                          jQuery('#kota_id').on("change",function(){
                            var foreign_key = jQuery(this).val();
                            jQuery.ajax({
                              url: "<?php echo site_url('services/get_options');?>",
                              type: "post",
                              data: "foreign_key=kota_id&table=data_kecamatan&select=nama_kecamatan label,data_kecamatan_id value&value=&fk="+foreign_key,
                              success: function(msg){
                                var first_row_kecamatan = jQuery("#kecamatan_id").find("option").eq(0).clone();
                                var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                                jQuery("#kecamatan_id").html("");
                                jQuery("#kecamatan_id").append(jQuery(first_row_kecamatan));
                                jQuery("#kecamatan_id").append(msg);
                                
                                jQuery("#desa_id").html("");
                                jQuery("#desa_id").append(jQuery(first_row_desa));

                              }
                            });
                          });
                        });
                      </script>
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="desa_id">Desa / Kelurahan</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <span class="form-control padding0">
                            <select name="data_insert[desa_id]" id="desa_id">
                              <option value="0">-----Pilih Desa / Kelurahan-----</option>
                            </select> 
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                          </span>
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div>
                    </div>
                     <script type="text/javascript">
                        jQuery(document).ready(function(){
                          jQuery('#kecamatan_id').on("change",function(){
                            var foreign_key = jQuery(this).val();
                            jQuery.ajax({
                              url: "<?php echo site_url('services/get_options');?>",
                              type: "post",
                              data: "foreign_key=kecamatan_id&table=data_desa&select=nama_desa label,data_desa_id value&value=&fk="+foreign_key,
                              success: function(msg){
                                var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                                jQuery("#desa_id").html("");
                                jQuery("#desa_id").append(jQuery(first_row_desa));
                                jQuery("#desa_id").append(msg);
                                
                              }
                            });
                          });
                        });
                      </script>
                    <br>

                    <input type="hidden" name="nonce" value="1425048409154f0835987a3f1"> 
                    <input type="hidden" name="ajax_target" value=".tab-content #add"> 
                    <input type="hidden" name="is_ajax" value="1"> 
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          &nbsp;
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <button name="input_submit" type="submit" id="input_submit" value="" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Save</button>
                      </div>
                      <div class="col-lg-3">
                        <div class="input-group">
                          &nbsp;
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                
                <div class="tab-pane" id="search<?php echo $is_modal;?>">                
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>

                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
                      <h3>Import Data</h3>
                      <p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
                      <?php
                      #$init = $this->init;
                      #echo $this->data->create_form_import($init);
                      echo $this->importer->upload_file_form();
                      ?>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

