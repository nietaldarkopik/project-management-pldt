<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
          <ul class="nav nav-tabs margintop-1 marginleft-16">
          <?php
            echo $this->data->show_panel_allowed("","admin","",array("listing"),"",false);
          ?>
          </ul>
          <br class="fclear"/><br/>
          <?php 
            echo $response;
          ?>
          

          <!-- SHOW FORM -->
          <form method="post" class="form ajax" action="<?php echo site_url('admin/mk_desa_proyek/edit/'.$data_lokasi_proyek['desa_lokasi_id']);?>" id="form" data-target-ajax=".ajax_container" enctype="multipart/form-data"><h3>Edit Desa / Kelurahan - Lokasi Proyek</h3>
            <input type="hidden" name="desa_lokasi_id" value="<?php echo $data_lokasi_proyek['desa_lokasi_id'];?>">
            <br>
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="pks_id">Jenis PKS</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_update[pks_id]" id="pks_id">

                    <option value="0">-----Pilih Jenis PKS-----</option>
                      <?php if(!empty($option_pks)):?>
                        <?php foreach($option_pks as $pks_id => $pks_value):?>
                        <option <?php if($pks_id == $data_lokasi_proyek['pks_id']) echo 'selected="selected"'; ?>value="<?php echo $pks_id;?>"><?php echo $pks_value;?></option>
                      <?php endforeach;?>
                      <?php endif;?>
                    </select> 
                  </span>
                  
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div><?php echo $msg_pks_id;?>
                
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="master_kontrak_id">Nomor Kontrak</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_update[master_kontrak_id]" id="master_kontrak_id">
                      <option value="0">-----Pilih Nomor Kontrak-----</option>
                      <?php if(!empty($option_kontrak)):?>
                        <?php foreach($option_kontrak as $mk_master_kontrak_id => $nomor_kontrak):?>
                        <option <?php if($mk_master_kontrak_id == $data_lokasi_proyek['master_kontrak_id']) echo 'selected="selected"'; ?>value="<?php echo $mk_master_kontrak_id;?>"><?php echo $nomor_kontrak;?></option>
                      <?php endforeach;?>
                      <?php endif;?>
                    </select> 
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div><?php echo $msg_master_kontrak_id;?>
              </div>
            </div>
            <script type="text/javascript">
              jQuery(document).ready(function(){
                jQuery('#pks_id').on("change",function(){
                  var foreign_key = jQuery(this).val();
                  jQuery.ajax({
                    url: "<?php echo site_url('services/get_options');?>",
                    type: "post",
                    data: "foreign_key=pks_id&table=mk_master_kontrak&select=nomor_kontrak label,mk_master_kontrak_id value&value=&fk="+foreign_key,
                    success: function(msg){
                      var first_row = jQuery("#master_kontrak_id").find("option").eq(0).clone();
                      jQuery("#master_kontrak_id").html("");
                      jQuery("#master_kontrak_id").append(jQuery(first_row));
                      jQuery("#master_kontrak_id").append(msg);
                      jQuery("#display_vendor").val('');
                      jQuery("#vendor").val('');
                    
                    }
                  });
                });
              });
            </script>
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="display_vendor">Vendor</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <input type="text" name="data_display[vendor]" value="<?php echo $data_lokasi_proyek['perusahaan'];?>" label="Vendor" class="form-control" readonly="readonly" id="display_vendor">
                  <input type="hidden" name="data_update[master_vendor]" value="<?php echo $data_lokasi_proyek['master_vendor'];?>" id="vendor">
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div><?php echo $msg_master_vendor;?>
              </div><!-- /.col-lg-6 -->
            </div>
            <script type="text/javascript">
              jQuery(document).ready(function(){
                jQuery('#master_kontrak_id').on("change",function(){
                  var master_kontrak_id = jQuery(this).val();
                  jQuery.ajax({
                    url: "<?php echo site_url('admin/mk_desa_proyek/get_vendor_data/');?>" +'/'+ master_kontrak_id,
                    success: function(msg){
                      var dt_vendor = jQuery.parseJSON(msg);
                      var id_vendor = dt_vendor.mk_master_vendor_id;
                      var nm_vendor = dt_vendor.nama_perusahaan;
                      if(id_vendor.length == 0){
                        alert("Silahkan tambahkan data vendor terlebih dahulu pada kontrak yang terpilih");
                        jQuery("#display_vendor").val('');
                        jQuery("#vendor").val('');
                      }else{
                        jQuery("#display_vendor").val(nm_vendor);
                        jQuery("#vendor").val(id_vendor);
                        
                      }                          
                    }
                  });
                });
              });
            </script>  
            <br>

            <div class="row">
              <div class="col-lg-12">
                <div class="input-group">
                  <label>&nbsp;</label>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="propinsi_id">Propinsi</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_display[propinsi_id]" id="propinsi_id">
                      <option value="0">-----Pilih Propinsi-----</option>
                      <?php if(!empty($option_propinsi)):?>
                        <?php foreach($option_propinsi as $id_loop => $display_loop):?>
                          <option <?php if($id_loop == $data_lokasi_proyek['data_propinsi_id']) echo 'selected="selected"';?> value="<?php echo $id_loop;?>"><?php echo $display_loop;?></option>
                        <?php endforeach;?>
                      <?php endif;?>
                    </select>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div>
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="kota_id">Kota / Kabupaten</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_display[kota_id]" id="kota_id">
                      <option value="0">-----Pilih Kota / Kabupaten-----</option>
                      <?php if(!empty($option_kota)):?>
                        <?php foreach($option_kota as $id_loop => $display_loop):?>
                          <option <?php if($id_loop == $data_lokasi_proyek['data_kota_id']) echo 'selected="selected"';?> value="<?php echo $id_loop;?>"><?php echo $display_loop;?></option>
                        <?php endforeach;?>
                      <?php endif;?>
                    </select> 
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div>
              </div>
            </div>
             <script type="text/javascript">
                jQuery(document).ready(function(){
                  jQuery('#propinsi_id').on("change",function(){
                    var propinsi_id = jQuery(this).val();
                    jQuery.ajax({
                      url: "<?php echo site_url('admin/mk_desa_proyek/get_kota_data/');?>" +'/'+ propinsi_id,
                      success: function(msg){
                        var first_row_kota = jQuery("#kota_id").find("option").eq(0).clone();
                        var first_row_kecamatan = jQuery("#kecamatan_id").find("option").eq(0).clone();
                        var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                        jQuery("#kota_id").html("");
                        jQuery("#kota_id").append(jQuery(first_row_kota));
                        jQuery("#kota_id").append(msg);

                        jQuery("#kecamatan_id").html("");
                        jQuery("#kecamatan_id").append(jQuery(first_row_kecamatan));
                        
                        jQuery("#desa_id").html("");
                        jQuery("#desa_id").append(jQuery(first_row_desa));
                        
                      }
                    });
                  });
                });
             </script>
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="kecamatan_id">Kecamatan</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_display[kecamatan_id]" id="kecamatan_id">
                      <option value="0">-----Pilih Kecamatan-----</option>
                      <?php if(!empty($option_kecamatan)):?>
                        <?php foreach($option_kecamatan as $id_loop => $display_loop):?>
                          <option <?php if($id_loop == $data_lokasi_proyek['data_kecamatan_id']) echo 'selected="selected"';?> value="<?php echo $id_loop;?>"><?php echo $display_loop;?></option>
                        <?php endforeach;?>
                      <?php endif;?>
                    </select> 
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div>
              </div>
            </div>
             <script type="text/javascript">
                jQuery(document).ready(function(){
                  jQuery('#kota_id').on("change",function(){
                    var foreign_key = jQuery(this).val();
                    jQuery.ajax({
                      url: "<?php echo site_url('services/get_options');?>",
                      type: "post",
                      data: "foreign_key=kota_id&table=data_kecamatan&select=nama_kecamatan label,data_kecamatan_id value&value=&fk="+foreign_key,
                      success: function(msg){
                        var first_row_kecamatan = jQuery("#kecamatan_id").find("option").eq(0).clone();
                        var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                        jQuery("#kecamatan_id").html("");
                        jQuery("#kecamatan_id").append(jQuery(first_row_kecamatan));
                        jQuery("#kecamatan_id").append(msg);
                        
                        jQuery("#desa_id").html("");
                        jQuery("#desa_id").append(jQuery(first_row_desa));

                      }
                    });
                  });
                });
              </script>
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  <label for="desa_id">Desa / Kelurahan</label>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="input-group">
                  <span class="form-control padding0">
                    <select name="data_update[desa_id]" id="desa_id">
                      <option value="0">-----Pilih Desa / Kelurahan-----</option>
                      <?php if(!empty($option_desa)):?>
                        <?php foreach($option_desa as $id_loop => $display_loop):?>
                          <option <?php if($id_loop == $data_lokasi_proyek['data_desa_id']) echo 'selected="selected"';?> value="<?php echo $id_loop;?>"><?php echo $display_loop;?></option>
                        <?php endforeach;?>
                      <?php endif;?>
                    </select> 
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                  </span>
                  <span class="input-group-addon">
                    <span class="glyphicon">&nbsp;</span>
                  </span>
                </div><?php echo $msg_desa_id;?>
              </div>
            </div>
             <script type="text/javascript">
                jQuery(document).ready(function(){
                  jQuery('#kecamatan_id').on("change",function(){
                    var foreign_key = jQuery(this).val();
                    jQuery.ajax({
                      url: "<?php echo site_url('services/get_options');?>",
                      type: "post",
                      data: "foreign_key=kecamatan_id&table=data_desa&select=nama_desa label,data_desa_id value&value=&fk="+foreign_key,
                      success: function(msg){
                        var first_row_desa = jQuery("#desa_id").find("option").eq(0).clone();
                        jQuery("#desa_id").html("");
                        jQuery("#desa_id").append(jQuery(first_row_desa));
                        jQuery("#desa_id").append(msg);
                        
                      }
                    });
                  });
                });
              </script>
            <br>

            <input type="hidden" name="ajax_target" value=".ajax_container"> 
            <input type="hidden" name="is_ajax" value="1"> 
            <br>

            <div class="row">
              <div class="col-lg-3">
                <div class="input-group">
                  &nbsp;
                </div>
              </div>
              <div class="col-lg-6">
                <button name="input_submit" type="submit" id="input_submit" value="" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Save</button>
              </div>
              <div class="col-lg-3">
                <div class="input-group">
                  &nbsp;
                </div>
              </div>
            </div>
          </form>
                
        </div>
      </div>
    </div>
  </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>
