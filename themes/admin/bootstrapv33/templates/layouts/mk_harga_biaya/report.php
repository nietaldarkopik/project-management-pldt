<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php /*
    <link href="<?php echo base_url();?>themes/admin/bootstrapv33/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>themes/admin/bootstrapv33/assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>themes/admin/bootstrapv33/assets/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>themes/admin/bootstrapv33/assets/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>themes/admin/bootstrapv33/assets/css/print.css" rel="stylesheet" type="text/css" media="print"/> */?>
    <style type="text/css">
      <?php #echo file_get_contents(base_url()."themes/admin/bootstrapv33/dist/css/bootstrap.css");?>
      <?php echo file_get_contents(base_url()."themes/admin/bootstrapv33/assets/css/bootstrap-pdf.css");?>
      <?php echo file_get_contents(base_url()."themes/admin/bootstrapv33/assets/font-awesome-4.3.0/css/font-awesome.min.css");?>
      <?php echo file_get_contents(base_url()."themes/admin/bootstrapv33/assets/ionicons-2.0.1/css/ionicons.min.css");?>
      <?php echo file_get_contents(base_url()."themes/admin/bootstrapv33/assets/css/styles.css");?>
      <?php echo file_get_contents(base_url()."themes/admin/bootstrapv33/assets/css/print.css");?>
      body{
        margin: 0 !important;
      }
    </style>
    <title><?php echo (isset($page_title))?$page_title:"";?></title>
  </head>
  <body>
      <?php
        $listing =  $this->data->create_listing($this->init);
        $data_rows = $this->data->data_rows;
        $first_row = (isset($this->data->data_rows[0]))?$this->data->data_rows[0]:array();
        $master_kontrak_id = (isset($first_row['master_kontrak_id']))?$first_row['master_kontrak_id']:0;
        $pks_id = (isset($first_row['pks_id']))?$first_row['pks_id']:0;
        $master_vendor_id = (isset($first_row['master_vendor_id']))?$first_row['master_vendor_id']:0;
        
        if(count($first_row) == 0)
        {
          $filters = $this->session->userdata("mk_harga_biaya_data_filter");
          $filter_master_kontrak_id = (isset($filters['master_kontrak_id']))?$filters['master_kontrak_id']:"";
          $kontrak = $this->master->get_value("mk_master_kontrak","",array('mk_master_kontrak_id' => $filter_master_kontrak_id));
          $master_kontrak_id = (isset($kontrak['mk_master_kontrak_id']))?$kontrak['mk_master_kontrak_id']:0;
          $pks_id = (isset($kontrak['pks_id']))?$kontrak['pks_id']:0;
          $master_vendor_id = (isset($kontrak['master_vendor_id']))?$kontrak['master_vendor_id']:0;
        }
      ?>
      <div class="pad_left1">
        <h1>
          <?php echo (isset($page_title))?$page_title:"";?>
        </h1>
        <h2>
          Kontrak : <?php echo $this->hook->do_action('hook_create_listing_value_master_kontrak_id',$master_kontrak_id);?>
        </h2>
        <h2>
          PKS     : <?php echo $this->hook->do_action('hook_create_listing_value_pks_id',$pks_id);?>
        </h2>
        <h2>
          Vendor     : <?php echo $this->hook->do_action('hook_create_listing_value_master_vendor_id',$master_vendor_id);?>
        </h2>
      </div>
      <div class="clearfix"></div>
      <hr/>
      <br/>
      <table width="100%" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Produk/Jasa</th>
            <th>Module/Tipe</th>
            <th>Spesifikasi</th>
            <th>volume</th>
            <th>Satuan</th>
            <th>Jenis Pengadaan</th>
            <th>Mata Uang</th>
            <td class="bg-success text-bold">Harga Satuan</th>
            <td class="bg-success text-bold">Harga FOB</th>
            <td class="bg-success text-bold">Freight</th>
            <th>Exchange Rate</th>
            <td class="bg-success text-bold">Bea Masuk</th>
            <td class="bg-success text-bold">PPN</th>
            <td class="bg-success text-bold">PPH</th>
            <td class="bg-success text-bold">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
              $row_output = "";
              if(is_array($data_rows) and count($data_rows) > 0)
              {
                $total_harga_satuan = 0;
                $total_harga_fob = 0;
                $total_freight = 0;
                $total_bea_masuk = 0;
                $total_ppn = 0;
                $total_pph = 0;
                $total_grand = 0;
                foreach($data_rows as $i => $rows)
                {
                  $kontrak  = $this->master->get_value("mk_master_kontrak","",array("mk_master_kontrak_id" => $rows['master_kontrak_id']));
                  $pks  = $this->master->get_value("data_pks","",array("data_pks_id" => $rows['pks_id']));
                  $produk  = $this->master->get_value("mk_spesifikasi","",array("mk_spesifikasi_id" => $rows['spesifikasi_id']));
                  
                  $total_harga_satuan += ($rows['mata_uang'] == 'IDR')?$rows['harga_satuan']:($rows['harga_satuan']*$rows['exchange_rate']);
                  $total_harga_fob += ($rows['mata_uang'] == 'IDR')?$rows['harga_fob']:($rows['harga_fob']*$rows['exchange_rate']);
                  $total_freight += ($rows['mata_uang'] == 'IDR')?$rows['freight']:($rows['freight']*$rows['exchange_rate']);
                  $total_bea_masuk += ($rows['mata_uang'] == 'IDR')?$rows['bea_masuk']:($rows['bea_masuk']*$rows['exchange_rate']);
                  $total_ppn += ($rows['mata_uang'] == 'IDR')?$rows['ppn']:($rows['ppn']*$rows['exchange_rate']);
                  $total_pph += ($rows['mata_uang'] == 'IDR')?$rows['pph']:($rows['pph']*$rows['exchange_rate']);
                  $total_grand += ($rows['mata_uang'] == 'IDR')?$rows['total']:($rows['total']*$rows['exchange_rate']);
                                    
                  $path = (isset($this->init['path']))?$this->init['path']:'';
                  $controller = (isset($this->init['controller']))?$this->init['controller']:'';
                  $function = (isset($this->init['function']))?$this->init['function']:'';
                  $panel_function = (isset($this->init['panel_function']))?$this->init['panel_function']:array("edit","view","delete");
                  $action = $this->data->show_panel_allowed("",$path,$controller,$panel_function);
                  $action_btn = "";
                  if(!empty($action))
                  {
                    $action = $this->data->show_panel_allowed("",$path,$controller,$panel_function,$rows[$this->data->primary_key]);
                    $list_style = (isset($this->data->the_config['list_style']))?$this->data->the_config['list_style']:"class='rows_action'";
                    $action  = $this->hook->do_action('hook_create_listing_action',$action);
                    $action_btn = '<td '.$list_style.'>'. $action .'</td>';
                  }
                  
                  $currency = ($rows['mata_uang'] == 'IDR')?'Rp. ':'$';
                  $row_output .= '
                  <tr>
                    <td>'.($i+1).'</td>
                    <td>'.$rows['produk'].'</td>
                    <td>'.$rows['modul_tipe'].'</td>
                    <td>'.$rows['spesifikasi'].'</td>
                    <td>'.$rows['volume'].'</td>
                    <td>'.$rows['satuan'].'</td>
                    <td>'.$rows['import'].'</td>
                    <td>'.$rows['mata_uang'].'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['harga_satuan'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['harga_fob'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['freight'],$currency).'</td>
                    <td align="right">'. (($rows['exchange_rate'] > 0)?format_currency($rows['exchange_rate'],'Rp. '):'-') .'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['bea_masuk'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['ppn'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['pph'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success text-bold">'.format_currency($rows['total'],$currency).'</td>
                  </tr>';
                }
                  $row_output .= '
                  <tr>
                    <td align="right" valign="middle" class="bg-blue" colspan="8">TOTAL</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_harga_satuan,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_harga_fob,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_freight,'Rp. ').'</td>
                    <td class="bg-blue">&nbsp;</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_bea_masuk,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_ppn,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_pph,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_grand,'Rp. ').'</td>
                  </tr>';
              }
              else
              {
                
              }
              echo $row_output;
              ?>
          </tbody>
      </table>
  </body>
</html>
