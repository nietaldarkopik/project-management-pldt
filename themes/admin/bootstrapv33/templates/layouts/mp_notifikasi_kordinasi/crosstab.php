<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

			<div class="pad_left1"><h2 class="pad_bot1"><?php echo (isset($page_title))?$page_title:"";?></h2></div>
			<form id="form_filter" action="<?php echo $action;?>" method="post" class="form">
				<fieldset id="search_form_fieldset">
					<legend>Search Form</legend>
					<label for="data_per">Data Per</label>
					<span class="value_view"> : 
						<select name="data_filter[data_per]" id="data_per">
							<option value="" >---- Select Option ----</option>
							<option value="wilayah">wilayah</option>
							<option value="bintek">bintek</option>
							<option value="lembaga">lembaga</option>
						</select>
					</span>
					<br class="fclear">
					<div class="tahun_crosstab_container">
						<label for="tahun_from">Tahun</label>
						<span class="value_view"> : 
							<select name="data_filter[tahun_from]" id="tahun_from">
								<?php
								for($i = YEAR_START;$i <= date("Y"); $i++)
								{
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
								?>
							</select>
							sampai
							<select name="data_filter[tahun_too]" id="tahun_too">
								<?php
								for($i = YEAR_START;$i <= date("Y"); $i++)
								{
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
								?>
							</select>
						</span>
						<br class="fclear">
					</div>
					<label for="display_type">Display Type</label>
					<span class="value_view"> : 
						<select name="data_filter[display_type]" id="display_type">
							<option value="crosstab">crosstab</option>
							<option value="grafic">grafic</option>
						</select>
					</span>
					<br class="fclear">
					<label>&nbsp;</label>
					<input type="hidden" name="nonce" value="1391544093152f1471d2d50a1"> 
					<input type="hidden" name="ajax_target" value="#main_content .col1"> 
					<input type="hidden" name="is_ajax" value="1"> 
					<span class="value_view">
						&nbsp;&nbsp;
						<input type="submit" value="Search">
					</span>
				</fieldset>	
				<br class="fclear">
			</form>
			<div id="top-panel">
				<div id="panel">&nbsp;</div>
			</div>
			<div style="width:100%;height:100%;overflow:auto;">						
				<?php echo $this->crosstab->show();?>
			</div>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					function show_hide_tahun_crosstab()
					{
						var data_per = jQuery("#data_per").val();
						if(data_per == 'bintek')
						{
							jQuery(".tahun_crosstab_container").show();
						}else{
							jQuery(".tahun_crosstab_container").hide();
						}
					}
					jQuery("#data_per").on("change",function(){
						show_hide_tahun_crosstab();
					});
					show_hide_tahun_crosstab();
				});
			</script>
