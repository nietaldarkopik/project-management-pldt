<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
          <ul class="nav nav-tabs margintop-1 marginleft-16">
          <?php
            echo $this->data->show_panel_allowed("","admin","mp_berita_acara",array(array('title' => 'Listing','name' => 'listing', 'class' => 'glyphicon-list')),"",false);
          ?>
          </ul>
          <br class="fclear"/><br/>
          <?php 
            echo $response;
            
            $berita_acara = (isset($data_berita_acara['berita_acara']))?$data_berita_acara['berita_acara']:'';
            $nomor_berita_acara = (isset($data_berita_acara['nomor_berita_acara']))?$data_berita_acara['nomor_berita_acara']:'';
            $tanggal_berita_acara = (isset($data_berita_acara['tanggal_berita_acara']) and $data_berita_acara['tanggal_berita_acara'] != '0000-00-00')?$data_berita_acara['tanggal_berita_acara']:date("Y-m-d");
            $master_kontrak_id = (isset($master_kontrak_id))?$master_kontrak_id:"";
            $desa_id = (isset($desa_id))?$desa_id:"";
            if(empty($berita_acara))
            {
              $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
              $q_master_kontrak = $this->db->get("mk_master_kontrak");
              $master_kontrak = $q_master_kontrak->row_array();
              $nomor_kontrak = (isset($master_kontrak['nomor_kontrak']))?$master_kontrak['nomor_kontrak']:'';
              
              $pks_id = (isset($master_kontrak['pks_id']))?$master_kontrak['pks_id']:'';
              $this->db->where(array("data_pks_id" => $pks_id));
              $q_pks = $this->db->get("data_pks");
              $pks = $q_pks->row_array();
              $pks_no = (isset($pks['kode_pks']))?$pks['kode_pks']:'';
              $judul_pks = (isset($pks['judul_pks']))?$pks['judul_pks']:'';
              
              $master_sumber_pendanaan_id = (isset($master_kontrak['master_sumber_pendanaan_id']))?$master_kontrak['master_sumber_pendanaan_id']:'';
              $this->db->where(array("mk_sumber_pendanaan_id" => $master_sumber_pendanaan_id));
              $q_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan");
              $sumber_pendanaan = $q_sumber_pendanaan->row_array();
              
              $nama_sumber_pendanaan = (isset($sumber_pendanaan['nama']))?$sumber_pendanaan['nama']:'';
              $perusahaan_sumber_pendanaan = (isset($sumber_pendanaan['perusahaan']))?$sumber_pendanaan['perusahaan']:'';
              
              $master_vendor_id = (isset($master_kontrak['master_vendor_id']))?$master_kontrak['master_vendor_id']:'';
              $this->db->where(array("mk_master_vendor_id" => $master_vendor_id));
              $q_vendor = $this->db->get("mk_master_vendor");
              $vendor = $q_vendor->row_array();
              
              $data_pengoperasian = $this->master->get_value("mp_pengoperasian","",array("master_kontrak_id" => $master_kontrak_id,"desa_id" => $desa_id));
              $koperasi_id = (isset($data_pengoperasian['koperasi_id']))?$data_pengoperasian['koperasi_id']:"";
              $data_koperasi = $this->master->get_value("mk_koperasi","",array("mk_koperasi_id" => $koperasi_id));

              $nama_koperasi = (isset($data_koperasi['nama_koperasi']))?$data_koperasi['nama_koperasi']:"NAMA_KOPERASI";
              $nama_pengurus_koperasi = (isset($data_koperasi['nama']))?$data_koperasi['nama']:"NAMA_KOPERASI";
              $tanggal_operasi = (isset($data_koperasi['tanggal_operasi']))?$data_koperasi['tanggal_operasi']:"TANGGAL-OPERASI";
              
              $nama_vendor = (isset($vendor['nama']))?$vendor['nama']:'';
              $perusahaan_vendor = (isset($vendor['perusahaan']))?$vendor['perusahaan']:'';
              
              $tanggal = $this->data->human_date($tanggal_berita_acara);
              $tanggal_hari = $this->data->human_date($tanggal_berita_acara,false,true);
              $tanggal_operasi = $this->data->human_date($tanggal_operasi,false,true);
              
              $berita_acara = '
              <p style="text-align: center;">
                <strong>Berita Acara Uji Terima Penyerahan Barang dan Jasa :</strong>
                <br /><strong>PKS No. '.$pks_no.' Tanggal '.$tanggal.'</strong>
                <br /><strong>Pengadaan : '.$judul_pks.'</strong>
                <br /><strong>Nomor : '.$nomor_kontrak.'</strong>
              </p>
              <p style="text-align: left;">
                <br />Pada hari ini '.$tanggal_hari.' Yang dilakukan oleh para pihak :
                <br />1 <strong>'.$nama_vendor.', '.$perusahaan_vendor.'</strong>
                <br />2 <strong>'.$nama_pengurus_koperasi.', '.$nama_koperasi.'</strong>
                <br />Telah dilakukan uji terima dan Penyerahan Barang dan Jasa <strong>'.$judul_pks.'</strong>. Dengan rincian sebagaimana tertuang dalam Lampiran : Daftar Penyerahan barang dan Jasa.
                <br />Para pihak sepakat menerima sepenuhnya sesuai dengan syarat dan ketentuan kontrak (dengan catatan - bila ada) ...................................................................................
                <br />Terhitung sejak tanggal '.$tanggal_operasi.' peralatan tersebut selanjutnya diserahkan pengoperasiannya kepada Koperasi '.$nama_koperasi.'
              </p>
              ';
            }
            
            if(empty($master_kontrak_id))
            {
          ?>
          <p class="alert alert-danger">Kode kontrak tidak terdeteksi, silahkan kembali ke listing.</p>
          <?php
            }elseif(empty($desa_id))
            {
          ?>
          <p class="alert alert-danger">Data desa tidak terdeteksi, silahkan kembali ke listing.</p>
          <?php
            }else{
          ?>
          <form method="post" class="form ajax" action="<?php echo base_url();?>admin/mp_inspeksi_lapangan/update_berita_acara/<?php echo $master_kontrak_id;?>/<?php echo $desa_id;?>" id="form" data-target-ajax=".ajax_container" enctype="multipart/form-data">
            <h3>Update Berita Acara</h3>
            <br/>
            <div class="row">
                <br/>
                <div class="col-lg-12">
                    <div class="input-group">
                        <label for="berita_acara">Nomor Berita Acara</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <input type="text" name="data[nomor_berita_acara]" label="Nomor Berita Acara" id="nomor_berita_acara" class="form-control" value="<?php echo $nomor_berita_acara;?>"/>
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <div class="row">
                <br/>
                <div class="col-lg-12">
                    <div class="input-group">
                        <label for="berita_acara">Tanggal Berita Acara</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <input type="text" name="data[tanggal_berita_acara]" label="Tanggal Berita Acara" id="tanggal_berita_acara" class="form-control" value="<?php echo $tanggal_berita_acara;?>"/>
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <div class="row">
                <br/>
                <div class="col-lg-12">
                    <div class="input-group">
                        <label for="berita_acara">Berita Acara</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <textarea name="data[berita_acara]" cols="40" rows="10" label="Berita Acara" id="berita_acara" class="form-control addon_input_mce"><?php echo $berita_acara;?></textarea>
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
            <script>
                $(document).ready(function() {
                    if (jQuery(".addon_input_mce").length > 0) {
                        /*
                        if (typeof(tinyMCE) != "undefined") {
                          if (tinyMCE.activeEditor == null || tinyMCE.activeEditor.isHidden() != false) {
                          tinyMCE.editors=[];
                          }
                        }
                        jQuery("#berita_acara").tinymce({
                          plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                          ],
                          menubar: false,
                          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        });*/

                        tinyMCE.editors = [];
                        tinyMCE.init({
                          mode: "none",
                          plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                          ],
                          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        });
                        tinyMCE.execCommand("mceRemoveEditor", false, "berita_acara");
                        tinyMCE.execCommand("mceAddEditor", false, "berita_acara");
                        tinyMCE.triggerSave();
                    }
                });
                
              $('body').on('focus','#tanggal_berita_acara',function(){
                                                                                        $(this).datepicker({
                                                                                          dateFormat: 'yy-mm-dd',
                                                                                          format: 'yyyy-mm-dd'
                                                                                        });
                                                                                      });
            </script>
            <input type="hidden" name="do_update_berita_acara" value="save" />
            <input type="hidden" name="mp_asset_berita_acara_id" value="<?php echo $master_kontrak_id;?>" />
            <input type="hidden" name="desa_id" value="<?php echo $desa_id;?>" />
            <input type="hidden" name="nonce" value="1425742053154fb18e557cc61" />
            <input type="hidden" name="ajax_target" value=".ajax_container" />
            <input type="hidden" name="is_ajax" value="1" />
            <br/>
            <div class="row">
                <div class="col-lg-3">
                    <div class="input-group">
                        &nbsp;
                    </div>
                </div>
                <div class="col-lg-6">
                    <button name="input_submit" type="submit" id="input_submit" value="" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Save</button>
                </div>
                <div class="col-lg-3">
                    <div class="input-group">
                        &nbsp;
                    </div>
                </div>
            </div>
        </form>
        <?php
        }
        ?>
        </div>
      </div>
    </div>
  </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>
