<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Inbox</h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target">
            <div class="col-lg-12">
              
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
               <li>
                  <a href="<?php echo site_url('admin/inbox/index');?>" class="box"><span class="glyphicon glyphicon-envelope"></span></a>
                </li>
                <li class="active">
                  <a href="<?php echo site_url('admin/inbox/add');?>"class="box"><span class="glyphicon glyphicon-plus"></span></a>
                </li>
              </ul>

              <div id="content" class="tab-content">
               <div class="tab-pane active" id="add">
                  <form method="post" action="<?php echo site_url('admin/inbox/do_add');?>" id="form" enctype="multipart/form-data">
                    <h3>Buat Pesan Baru</h3>  
                    <br>
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="judul_pesan">Kepada</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <input type="text" name="data[judul_pesan]" value="" label="Judul Pesan" id="judul_pesan"class="  form-control">                    
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->  
                    <br>

                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          <label for="judul_pesan">Judul Pesan</label>
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="input-group">
                          <input type="text" name="data[judul_pesan]" value="" label="Judul Pesan" id="judul_pesan"class="  form-control">                    
                          <span class="input-group-addon">
                            <span class="glyphicon">&nbsp;</span>
                          </span>
                        </div>
                      </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->  
                    <br>
        
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="input-group">
                          &nbsp;
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <button name="input_submit" type="submit" id="input_submit" value="" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Save</button>
                      </div>
                      <div class="col-lg-5">
                        <div class="input-group">
                          &nbsp;
                        </div>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

