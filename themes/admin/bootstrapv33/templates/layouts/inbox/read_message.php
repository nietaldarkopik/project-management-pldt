<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
<div class="col-lg-12">
  <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print" style="margin-left:15px;">
    <li class="active">
      <a href="<?php echo site_url('admin/inbox/index');?>" class="box"><span class="glyphicon glyphicon-envelope"></span></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/notification/index');?>" class="box"><span class="glyphicon glyphicon-warning-sign"></span></a>
    </li>
  </ul>

  <div class="mailbox row">
      <div class="col-xs-12">
          <div class="box box-solid">
              <div class="box-body">
                  <div class="row">
                      <div class="col-md-3 col-sm-4">
                          <div class="box-header">
                              <i class="fa fa-inbox"></i>
                              <h3 class="box-title">INBOX</h3>
                          </div>
                          <!-- compose message btn -->
                          <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i> Buat Pesan</a>
                          <div style="margin-top: 15px;">
                              <ul class="nav nav-pills nav-stacked">
                                  <li class="header">Folder</li>
                                  <li class="active"><a href="<?php echo site_url('admin/inbox/index');?>"><i class="fa fa-inbox"></i> Inbox <?php if($this->lib_inbox->total_unread_message > 0) echo '('.$this->lib_inbox->total_unread_message.')';?></a></li>
                                  <li><a href="<?php echo site_url('admin/inbox/sent_messages');?>"><i class="fa fa-mail-forward"></i> Terkirim</a></li>
                                  <li><a href="<?php echo site_url('admin/inbox/archive');?>"><i class="fa fa-folder"></i> Arsip</a></li>
                              </ul>
                          </div>
                      </div><!-- /.col (LEFT) -->
                      <div class="col-md-9 col-sm-8">

                        <div class="box-body">
                          <?php $success_message = $this->session->flashdata('success_inbox');?>
                          <?php if(!empty($success_message)):?>
                          <div class="alert alert-success alert-dismissable" style="padding:15px 30px;">
                              <i class="fa fa-check"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?php echo $success_message;?>
                          </div>
                          <?php endif;?>

                          <?php $failed_message = $this->session->flashdata('error_inbox');?>
                          <?php if(!empty($failed_message)):?>
                          <div class="alert alert-danger alert-dismissable" style="padding:15px 30px;">
                              <i class="fa fa-ban"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?php echo $failed_message;?>
                          </div>
                          <?php endif;?>
                        </div>


                          <div style="min-height:320px;">
                              <div>
                                <i style="color:#3C8DBC; font-size:65px;" class="glyphicon glyphicon-user"></i>
                                <div style="margin-top:-40px;margin-left:75px;font-weight:bold;font-size:20px;"><?php echo $the_message['username_pengirim']?></div>
                                <div style="margin-top:-7px;margin-left:75px;"><?php echo $the_message['email_pengirim'];?></div>
                                <div style="float:right;width:110px;">
                                  <small>
                                    <i class="fa fa-clock-o"></i> 
                                    <?php 
                                      $totime_message = strtotime($the_message['tanggal_pesan']);
                                      echo date('d/m/Y H:i', $totime_message);
                                    ?>
                                  </small>
                                </div>
                              </div>
                              
                              <hr/>

                              <div class="email-content-container">
                                <h3>
                                  <?php echo $the_message['judul_pesan'];?>
                                </h3>

                                <div class="email-content">
                                  <?php echo $the_message['pesan'];?>
                                </div>
                                
                                <?php if(!empty($the_message['lampiran'])):?>
                                <br/>
                                <div>
                                  Lampiran :<br/>
                                  <?php foreach($the_message['lampiran'] as $idx => $lampiran):?>
                                    <a href="<?php echo site_url('admin/inbox/attachment_downloader/'.$lampiran['inbox_lampiran_id']);?>" target="_blank">
                                      <?php echo $lampiran['lampiran_file'];?>
                                    </a></br>
                                  <?php endforeach;?>
                                </div>
                                <?php endif;?>
                              </div>
                          </div><!-- /.table-responsive -->

                      </div><!-- /.col (RIGHT) -->
                  </div><!-- /.row -->
              </div><!-- /.box-body -->
              
          </div><!-- /.box -->
      </div><!-- /.col (MAIN) -->
  </div>

  <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" style="width:65%;">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Buat Pesan Baru</h4>
              </div>
              <form action="<?php echo $send_email_url;?>" method="post" enctype="multipart/form-data" id="send_email">
                  <input type="hidden" name="current_url" value="<?php echo $current_url;?>" />
                  <div class="modal-body">
                    
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Kepada:</span>
                        <select name="input[penerima]" class="form-control" id="input_penerima">
                          <option value="0">Pilih penerima</option>
                          <?php if(!empty($user_data)):?>
                            <?php foreach($user_data as $idx => $ud):?>
                            <option value="<?php echo $ud['user_id'];?>"><?php echo $ud['username'] .' ('. $ud['email'] .')';?></option>
                            <?php endforeach;?>
                          <?php endif;?>
                        </select>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Judul Pesan:</span>
                        <input name="input[judul_pesan]" id="input_judul_pesan" type="text" class="form-control" placeholder="Judul Pesan">
                      </div>
                    </div>

                    <div class="form-group">
                      <textarea name="input[pesan]" cols="40" rows="10" id="pesan" type="textarea"class="form-control addon_input_mce" ></textarea>
                    </div>

                    <div class="form-group">
                      <div class="input-group">
                        <input type="file" name="lampiran[]"/>
                        <br/ >
                        
                        <br id="marker-lampiran"/ >
                        <div class="btn btn-success btn-file" id="tambah-lampiran">
                          <i class="fa fa-paperclip"></i> Tambah Lampiran
                        </div>

                      </div>
                    </div>
                    
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                        <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Kirim Pesan</button>
                    </div>
              </form>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div>

</div> <!-- .col-lg-12 -->

  <script>
    $(document).ready(function(){
      if(jQuery(".addon_input_mce").length > 0)
      {
        tinyMCE.editors=[];
        tinyMCE.execCommand("mceRemoveEditor", false, "pesan");
        tinyMCE.execCommand("mceAddEditor", false, "pesan");
        tinyMCE.triggerSave();
      }

      $('#tambah-lampiran').click(function(){
        $( '<input type="file" name="lampiran[]"/><br/ >' ).insertBefore( "#marker-lampiran" );
      });

      //send email
      $("#send_email").on("submit", function(e){
        //remove error class (if any)
        $('#input_penerima').closest('div .input-group').removeClass('has-error');
        $('#input_judul_pesan').closest('div .input-group').removeClass('has-error');

        var send_error = false;
        var input_penerima = $('#input_penerima').val();
        var input_judul_pesan = $('#input_judul_pesan').val();
        
        if(input_penerima == 0){
          $('#input_penerima').closest('div .input-group').addClass('has-error');
          send_error = true;
        }

        if(input_judul_pesan == 0){
          $('#input_judul_pesan').closest('div .input-group').addClass('has-error');
          send_error = true;
        }

        //avoid submit form
        if(send_error) return false;


      });

    });
  </script>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

