<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
<div class="col-lg-12">
  <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print" style="margin-left:15px;">
    <li class="active">
      <a href="<?php echo site_url('admin/inbox/index');?>" class="box"><span class="glyphicon glyphicon-envelope"></span></a>
    </li>
    <li>
      <a href="<?php echo site_url('admin/notification/index');?>" class="box"><span class="glyphicon glyphicon-warning-sign"></span></a>
    </li>
  </ul>

  <div class="mailbox row">
      <div class="col-xs-12">
          <div class="box box-solid">
              <div class="box-body">
                  <div class="row">
                      <div class="col-md-3 col-sm-4">
                          <div class="box-header">
                              <i class="fa fa-inbox"></i>
                              <h3 class="box-title">INBOX</h3>
                          </div>
                          <!-- compose message btn -->
                          <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i> Buat Pesan</a>
                          <div style="margin-top: 15px;">
                              <ul class="nav nav-pills nav-stacked">
                                  <li class="header">Folder</li>
                                  <li class="active"><a href="<?php echo site_url('admin/inbox/index');?>"><i class="fa fa-inbox"></i> Inbox <?php if($this->lib_inbox->total_unread_message > 0) echo '('.$this->lib_inbox->total_unread_message.')';?></a></li>
                                  <li><a href="<?php echo site_url('admin/inbox/sent_messages');?>"><i class="fa fa-mail-forward"></i> Terkirim</a></li>
                                  <li><a href="<?php echo site_url('admin/inbox/archive');?>"><i class="fa fa-folder"></i> Arsip</a></li>
                              </ul>
                          </div>
                      </div><!-- /.col (LEFT) -->
                      <div class="col-md-9 col-sm-8">

                        <div class="box-body">
                          <?php $success_message = $this->session->flashdata('success_inbox');?>
                          <?php if(!empty($success_message)):?>
                          <div class="alert alert-success alert-dismissable" style="padding:15px 30px;">
                              <i class="fa fa-check"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?php echo $success_message;?>
                          </div>
                          <?php endif;?>

                          <?php $failed_message = $this->session->flashdata('error_inbox');?>
                          <?php if(!empty($failed_message)):?>
                          <div class="alert alert-danger alert-dismissable" style="padding:15px 30px;">
                              <i class="fa fa-ban"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?php echo $failed_message;?>
                          </div>
                          <?php endif;?>
                        </div>

                        <form method="post" action="<?php echo $current_url;?>">
                          <div class="row pad">
                              
                              <div class="col-sm-6">
                                <!-- Action button -->
                                <div class="btn-group">
                                  <select name="bulk_action" class=" btn-flat ">
                                    <option>Tindakan pada pesan terpilih</option>
                                    <option>Tandai sebagai sudah terbaca</option>
                                    <option>Pindahkan ke folder arsip</option>
                                  </select>
                                </div>

                              </div>
                              <div class="col-sm-6 search-form">
                                  <div class="input-group">
                                      <input type="text" class="form-control input-sm" placeholder="Search">
                                      <div class="input-group-btn">
                                          <button type="submit" name="q" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                      </div>
                                  </div>
                              </div>
                          </div><!-- /.row -->

                          <div style="min-height:320px;">
                              <table class="table table-mailbox">
                                  <tbody>
                                    <tr class="unread" style="background-color:#3c8dbc;">
                                      <td class="small-col"> &nbsp; <input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"></td>
                                      <td class="name" style="color:#ffffff !important;">Pengirim</td>
                                      <td class="subject" style="color:#ffffff !important;">Judul Pesan</td>
                                      <td class="time" style="color:#ffffff !important;">Waktu &nbsp;</td>
                                    </tr>
                                    
                                    <?php if($message_data['count_data'] > 0):?>
                                      <?php foreach($message_data['data'] as $idx => $md):?>
                                      <tr <?php if($md['terbaca'] == 0) echo 'class="unread"';?>>
                                        <td class="small-col"> &nbsp; <input type="checkbox" name="bulk_data[]" value="<?php echo $md['inboxdet_id'];?>" class="bulk_data"></td>
                                        <td class="name"><a href="<?php echo site_url('admin/inbox/read_message/'.$md['inboxdet_id']);?>"><?php echo $md['username_pengirim'] .' ('. $md['email_pengirim'] .')';?></a></td>
                                        <td class="subject"><a href="<?php echo site_url('admin/inbox/read_message/'.$md['inboxdet_id']);?>"><?php echo $md['judul_pesan'];?></a></td>
                                        <td class="time">
                                          <?php
                                            $old_date_timestamp = strtotime($md['tanggal_pesan']);
                                            $new_date = date('d/m/Y H:i', $old_date_timestamp); 
                                            echo $new_date;
                                          ?>
                                        </td>
                                      </tr>
                                      <?php endforeach;?>
                                  
                                  <?php else:?>
                                      <tr>
                                        <td colspan="4"><small>Inbox anda kosong</small></td>
                                      </tr>
                                  <?php endif;?>
                                  
                                </tbody>
                              </table>
                          </div><!-- /.table-responsive -->
                        </form>

                      </div><!-- /.col (RIGHT) -->
                  </div><!-- /.row -->
              </div><!-- /.box-body -->
              <div class="box-footer clearfix">
                  <div class="pull-right">
                      <small>Menampilkan 1-20 dari 1240</small>
                      <button class="btn btn-xs btn-primary"><i class="fa fa-caret-left"></i></button>
                      <button class="btn btn-xs btn-primary"><i class="fa fa-caret-right"></i></button>
                  </div>
              </div><!-- box-footer -->
          </div><!-- /.box -->
      </div><!-- /.col (MAIN) -->
  </div>

  <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" style="width:65%;">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Buat Pesan Baru</h4>
              </div>
              <form action="<?php echo $send_email_url;?>" method="post" enctype="multipart/form-data" id="send_email">
                  <input type="hidden" name="current_url" value="<?php echo $current_url;?>" />
                  <div class="modal-body">
                    
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Kepada:</span>
                        <select name="input[penerima]" class="form-control" id="input_penerima">
                          <option value="0">Pilih penerima</option>
                          <?php if(!empty($user_data)):?>
                            <?php foreach($user_data as $idx => $ud):?>
                            <option value="<?php echo $ud['user_id'];?>"><?php echo $ud['username'] .' ('. $ud['email'] .')';?></option>
                            <?php endforeach;?>
                          <?php endif;?>
                        </select>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">Judul Pesan:</span>
                        <input name="input[judul_pesan]" id="input_judul_pesan" type="text" class="form-control" placeholder="Judul Pesan">
                      </div>
                    </div>

                    <div class="form-group">
                      <textarea name="input[pesan]" cols="40" rows="10" id="pesan" type="textarea"class="form-control addon_input_mce" ></textarea>
                    </div>

                    <div class="form-group">
                      <div class="input-group">
                        <input type="file" name="lampiran[]"/>
                        <br/ >
                        
                        <br id="marker-lampiran"/ >
                        <div class="btn btn-success btn-file" id="tambah-lampiran">
                          <i class="fa fa-paperclip"></i> Tambah Lampiran
                        </div>

                      </div>
                    </div>
                    
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                        <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Kirim Pesan</button>
                    </div>
              </form>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div>

</div> <!-- .col-lg-12 -->

  <script>
    $(document).ready(function(){
      if(jQuery(".addon_input_mce").length > 0)
      {
        tinyMCE.editors=[];
        tinyMCE.execCommand("mceRemoveEditor", false, "pesan");
        tinyMCE.execCommand("mceAddEditor", false, "pesan");
        tinyMCE.triggerSave();
      }

      $('#tambah-lampiran').click(function(){
        $( '<input type="file" name="lampiran[]"/><br/ >' ).insertBefore( "#marker-lampiran" );
      });

      //send email
      $("#send_email").on("submit", function(e){
        //remove error class (if any)
        $('#input_penerima').closest('div .input-group').removeClass('has-error');
        $('#input_judul_pesan').closest('div .input-group').removeClass('has-error');

        var send_error = false;
        var input_penerima = $('#input_penerima').val();
        var input_judul_pesan = $('#input_judul_pesan').val();
        
        if(input_penerima == 0){
          $('#input_penerima').closest('div .input-group').addClass('has-error');
          send_error = true;
        }

        if(input_judul_pesan == 0){
          $('#input_judul_pesan').closest('div .input-group').addClass('has-error');
          send_error = true;
        }

        //avoid submit form
        if(send_error) return false;


      });

    });
  </script>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

