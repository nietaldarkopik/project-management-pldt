<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <!-- <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li> -->
                <!--<li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>-->
                <!-- <li class="hide"><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li> -->
                <!-- <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li> -->
                <!--<li><a href="#export<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-export"></span></a></li>-->
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                     #echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box box-solid box-primary collapsed-box">
                            <div data-original-title="Form Pencarian" class="box-header" data-toggle="tooltip" title="">
                                <h3 class="box-title">Search Form</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse" id="box-collapse"><i class="fa fa-plus"></i></button>
                                    <script>
                                        $(document).ready(function(){
                                        $("#box-collapse").click();
                                        });
                                    </script>
                                    <!-- <button class="btn btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                </div>
                            </div>
                            <div style="display: none;" class="box-body">
                            <form id="form_filter" action="<?php echo base_url();?>admin/gr_laporan/index" method="post" class="form " data-target-ajax=".tab-content #search">
                            <br>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <label for="block_name">Bulan</label>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <input name="data_filter[block_name]" value="" label="Block Name" id="data_filter_block_name" use_search="1" use_listing="1" rules="required" parent_name="data_filter" star_required="0" stardesc_required="" maxlength="" size="" style="" class=" required form-control" type="text">
                                        <span class="input-group-addon">
                                        <span class="glyphicon">&nbsp;</span>
                                        </span>
                                    </div>
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->  
                            <br>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <label for="block_name">Tahun</label>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <input name="data_filter[block_name]" value="" label="Block Name" id="data_filter_block_name" use_search="1" use_listing="1" rules="required" parent_name="data_filter" star_required="0" stardesc_required="" maxlength="" size="" style="" class=" required form-control" type="text">
                                        <span class="input-group-addon">
                                        <span class="glyphicon">&nbsp;</span>
                                        </span>
                                    </div>
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->  
                            <br>
                            <label>&nbsp;</label>
                            <input name="nonce" value="14312246861554ec16e422ea1" type="hidden"> 
                            <input name="ajax_target" value=".tab-content #search" type="hidden"> 
                            <input name="is_ajax" value="" type="hidden"> 
                            <span class="value_view">
                            &nbsp;&nbsp;

                            <br>
                            <div class="row">
                            <div class="col-lg-3">
                            <div class="input-group">
                            &nbsp;
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <button name="input_submit" type="submit" id="input_submit" value="" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Search</button>
                            </div>
                            <div class="col-lg-3">
                            <div class="input-group">
                            &nbsp;
                            </div>
                            </div>
                            </div>
                            </span>

                            <br class="fclear">
                            </form>
                            </div><!-- /.box-body -->
                            <div style="display: none;" class="box-footer">
                            &nbsp;
                            </div><!-- /.box-footer-->
                            </div>
                        </div>
                    </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        #echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th rowspan="2" class="info">Program</th>
                              <th rowspan="2" class="info">Satuan</th>
                              <th class="info">Target</th>
                              <th colspan="3" class="warning">Target</th>
                              <th colspan="3" class="success">Realisasi</th>
                              <th colspan="2" class="info">Deviasi</th>
                              <th class="info">Capaian thd</th>
                            </tr>
                            <tr>
                              <th class="info">Program</th>
                              <td class="warning">Bulan Lalu</th>
                              <td class="warning">Bulan ini</th>
                              <td class="warning">Jumlah</th>
                              <td class="success">Bulan Lalu</th>
                              <td class="success">Bulan ini</th>
                              <td class="success">Jumlah</th>
                              <th class="info">Loksai</th>
                              <th class="info">%</th>
                              <th class="info">Target Program</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td class="info">&nbsp;</td>
                              <td class="info">&nbsp;</td>
                              <td class="info">&nbsp;</td>
                              <td class="warning">&nbsp;</td>
                              <td class="warning">&nbsp;</td>
                              <td class="warning">&nbsp;</td>
                              <td class="success">&nbsp;</td>
                              <td class="success">&nbsp;</td>
                              <td class="success">&nbsp;</td>
                              <td class="info">&nbsp;</td>
                              <td class="info">&nbsp;</td>
                              <td class="info">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                    <?php
                      #echo $this->data->create_listing($this->init);
                    ?>
                  </div>
                <div class="row">
                    <div class="col-lg-12">
                        <button name="send_report" type="submit" id="send_report" class="text-center btn btn-primary btn-block">Kirim Laporan ke Sumber Pendanaan</button>
                    </div>
                </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                   
                  <?php
                    #$init = $this->init;
                    #echo $this->data->create_form_filter($init);
                  ?> 
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
						<h3>Import Data</h3>
						<p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
						<?php
						#$init = $this->init;
						#echo $this->data->create_form_import($init);
						echo $this->importer->upload_file_form();
						?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="export<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_export($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

