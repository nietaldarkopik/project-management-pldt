<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container_detail_week content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <div id='content_detail_week' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                <?php
                  if(isset($data_param) and is_array($data_param) and isset($data_cakupan_kerja) and is_array($data_cakupan_kerja) and count($data_cakupan_kerja) > 0)
                  {
                    $tanggal_awal = (isset($data_param['tanggal_mulai']))?$data_param['tanggal_mulai']:"";
                    $tanggal_akhir  = (isset($data_param['tanggal_akhir']))?$data_param['tanggal_akhir']:"";
                    $data_filter_master_kontrak_id  = (isset($data_param['master_kontrak_id']))?$data_param['master_kontrak_id']:"";
                  ?>
                  <div class="table-responsive">
                    <table width="100%" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th rowspan="3" class="text-center">No</th>
                          <th rowspan="3" class="text-center">Tanggal Rencana</th>
                          <?php /*<th rowspan="3" class="text-center">Minggu Ke</th>*/ ?>
                          <th rowspan="3" class="text-center">Kontrak</th>
                          <th rowspan="3" class="text-center">PKS</th>
                          <th rowspan="3" class="text-center">Vendor</th>
                          <th rowspan="3" class="text-center">Desa</th>
                          <th rowspan="3" class="text-center">Barang/Jasa</th>
                          <th rowspan="3" class="text-center">Jumlah</th>
                          <?php /*<th rowspan="3" class="text-center">Actions</th>*/?>
                        </tr>
                      </thead>				
                      <tbody>
                        <?php
                        $data_rows = $data_cakupan_kerja;
                        if(is_array($data_rows) and count($data_rows) > 0)
                        {
                          $no = 1;
                          foreach($data_rows as $i => $d)
                          {
                            $mk_cakupan_kerja_id = $d['mk_cakupan_kerja_id'];
                            $master_kontrak_id = $d['master_kontrak_id'];
                            $master_vendor_id = $d['master_vendor_id'];
                            $pks_id = $d['pks_id'];
                            $spesifikasi_id = $d['spesifikasi_id'];
                            $desa_id = $d['desa_id'];
                            $nilai = $d['nilai'];
                            $satuan = $d['satuan'];
                            $tanggal_rencana = $d['tanggal_rencana'];
                            $tahun = $d['tahun'];
                            $bulan = $d['bulan'];
                            $minggu = $d['minggu'];
                            
                            $data_spesifikasi = $this->master->get_value("mk_spesifikasi","","mk_spesifikasi_id = '".$d['spesifikasi_id']."'");
                            $nama_desa = (isset($data_desa['nama_desa']))?$data_desa['nama_desa']:"";
                            $data_desa = $this->master->get_value("data_desa","","data_desa_id = '".$d['desa_id']."'");
                            $mk_spesifikasi_id = $d['spesifikasi_id'];
                            $satuan = (isset($d['satuan']) and !empty($d['satuan']))?$d['satuan']:" items";
                            $produk = (isset($data_spesifikasi['produk']))?$data_spesifikasi['produk']:"";
                            $kode_spesifikasi = (isset($data_spesifikasi['kode_spesifikasi']))?$data_spesifikasi['kode_spesifikasi']:"";
                            $modul_tipe = (isset($data_spesifikasi['modul_tipe']))?$data_spesifikasi['modul_tipe']:"";
                            $spesifikasi = (isset($data_spesifikasi['spesifikasi']))?$data_spesifikasi['spesifikasi']:"";
                            $harga = (isset($data_spesifikasi['harga']))?$data_spesifikasi['harga']:"";
                            
                            $data_kontrak = $this->master->get_value("mk_master_kontrak","","mk_master_kontrak_id = '".$d['master_kontrak_id']."'");
                            $kontrak = (isset($data_kontrak['nomor_kontrak']))?$data_kontrak['nomor_kontrak']:"";
                            $kontrak .= (isset($data_kontrak['judul_kontrak']))?" - ".$data_kontrak['judul_kontrak']:"";
                            
                            $data_pks = $this->master->get_value("data_pks","","data_pks_id = '".$d['pks_id']."'");
                            $pks = (isset($data_pks['kode_pks']))?$data_pks['kode_pks']:"";
                            $pks .= (isset($data_pks['judul_pks']))?" - ".$data_pks['judul_pks']:"";
                            
                            $data_desa = $this->master->get_value("data_desa","","data_desa_id = '".$d['desa_id']."'");
                            $desa = (isset($data_desa['kode_desa']))?$data_desa['kode_desa']:"";
                            $desa .= (isset($data_desa['nama_desa']))?" - ".$data_desa['nama_desa']:"";
                            
                            $master_vendor = $this->master->get_value("mk_master_vendor","","mk_master_vendor_id = '".$d['master_vendor_id']."'");
                            $vendor = (isset($master_vendor['kode_vendor']))?$master_vendor['kode_vendor']:"";
                            $vendor .= (isset($master_vendor['perusahaan']))?" - ".$master_vendor['perusahaan']:"";
                            
                            $row_cakupan_kerja = "";
                            $qtotal_desa = $this->model_cakupan_pekerjaan->get_total_work_location($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                            
                            $total_desa = (is_array($qtotal_desa) and count($qtotal_desa) > 0)?count($qtotal_desa):0;
                            $total_product = $this->model_cakupan_pekerjaan->get_total_product($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                            $panel_function = array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            );
                            $the_action = $this->data->show_panel_allowed("","admin","mk_cakupan_pekerjaan",$panel_function,$mk_cakupan_kerja_id);
                            $list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
                            $the_action  = $this->hook->do_action('hook_create_listing_action',$the_action);
                            $the_action = str_replace("btn-sm","btn-xs",$the_action);
                        ?>
                          <tr>
                            <td class="text-center"><?php echo $no;?></td>
                            <td nowrap="nowrap"><?php echo $d['tanggal_rencana'];?></td>
                            <?php /*<td nowrap="nowrap"><?php echo $d['tanggal_rencana'];?></td> */?>
                            <td nowrap="nowrap"><?php echo $kontrak;?></td>
                            <td nowrap="nowrap"><?php echo $pks;?></td>
                            <td nowrap="nowrap"><?php echo $vendor;?></td>
                            <td nowrap="nowrap"><?php echo $desa;?></td>
                            <td nowrap="nowrap"><?php echo $kode_spesifikasi .' - '. $produk;?></td>
                            <td nowrap="nowrap"><?php echo $d['nilai'];?> <?php echo $satuan;?></td>
                            <?php
                            if(!empty($the_action))
                            {
                            ?>
                              <?php /*<td <?php echo $list_style;?>><?php echo $the_action;?></td>*/?>
                            <?php
                            }
                            ?>
                          </tr>
                        <?php
                            $no++;
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                    <?php
                      }else{
                          echo '<div class="alert alert-warning" role="alert">Data cakupan pekerjaan tidak tersedia.</div>';
                      }
                    ?>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
