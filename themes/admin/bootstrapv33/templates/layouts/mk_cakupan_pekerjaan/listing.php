<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
                <!--<li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>-->
                <li class="hide"><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li>
                <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li>
                <!--<li><a href="#export<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-export"></span></a></li>-->
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>            
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      $create_listing = $this->data->create_listing($this->init);
                      $data_rows = $this->data->data_rows;

                      $filters = $this->session->userdata("mk_cakupan_pekerjaan_data_filter");
                      $data_filter_master_kontrak_id  = (isset($filters['master_kontrak_id']))?$filters['master_kontrak_id']:"";

                        $data_kontrak = $this->master->get_value("mk_master_kontrak","","mk_master_kontrak_id = '".$data_filter_master_kontrak_id."'");

                        $tanggal_awal	= (isset($data_kontrak['tanggal_mulai']))?$data_kontrak['tanggal_mulai']:"";
                        $tanggal_akhir	= (isset($data_kontrak['tanggal_berakhir']))?$data_kontrak['tanggal_berakhir']:"";
                      $weeks = $this->model_cakupan_pekerjaan->get_weeks($data_filter_master_kontrak_id);

                      $months = 0;
                      if(isset($weeks) and $weeks > 0)
                      {
                        $months = ceil($weeks/4);
                      }
                      if(!empty($data_filter_master_kontrak_id))
                      {
                    ?>
                    <table width="100%" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th rowspan="3" class="text-center">No</th>
                          <th rowspan="3" class="text-center">Kontrak</th>
                          <th rowspan="3" class="text-center">PKS</th>
                          <th rowspan="3" class="text-center">Cakupan Pekerjaan</th>
                          <th rowspan="2" colspan="2"class="text-center">Jumlah Proyek Lokasi <br/>(Desa/Kelurahan)</th>
                          <th class="text-center" colspan="<?php echo ($months*4)+1;?>"><?php echo $this->data->human_date($tanggal_awal);?> sampai <?php echo $this->data->human_date($tanggal_akhir);?></th>
                          <?php /*<th rowspan="3" class="text-center">Actions</th>*/ ?>
                        </tr>
                        <tr>
                          <th class="text-center">Bulan ke </th>
                          <?php
                          $mod_weeks = $weeks%4;
                          for($i = 1; $i <= $months; $i++)
                          {
													?>
                          <th colspan="<?php echo 4-$mod_weeks;?>" class="text-center"><?php echo $i;?></th>
                          <?php
													}
													?>
                        </tr>
                        <tr>
                          <th class="text-center">Jumlah Desa</th>
                          <th class="text-center">Jumlah Barang/Jasa</th>
                          <th class="text-center">Minggu ke </th>
													<?php
													for($i = 1; $i <= $weeks; $i++)
													{
														echo '<th class="text-center">'.$i.'</th>';
													}
													?>
                        </tr>
                      </thead>				
                      <tbody>
                        <?php
                        if(is_array($data_rows) and count($data_rows) > 0)
                        {
                          $no = 1;
                          foreach($data_rows as $i => $d)
                          {
                            $mk_cakupan_kerja_id = $d['mk_cakupan_kerja_id'];
                            $master_kontrak_id = $d['master_kontrak_id'];
                            $master_vendor_id = $d['master_vendor_id'];
                            $pks_id = $d['pks_id'];
                            $spesifikasi_id = $d['spesifikasi_id'];
                            $desa_id = $d['desa_id'];
                            $nilai = $d['nilai'];
                            $satuan = $d['satuan'];
                            $tanggal_rencana = $d['tanggal_rencana'];
                            $tahun = $d['tahun'];
                            $bulan = $d['bulan'];
                            $minggu = $d['minggu'];
                            
                            $data_spesifikasi = $this->master->get_value("mk_spesifikasi","","mk_spesifikasi_id = '".$d['spesifikasi_id']."'");
                            $nama_desa = (isset($data_desa['nama_desa']))?$data_desa['nama_desa']:"";
                            $data_desa = $this->master->get_value("data_desa","","data_desa_id = '".$d['desa_id']."'");
                            $mk_spesifikasi_id = $d['spesifikasi_id'];
                            $produk = (isset($data_spesifikasi['produk']))?$data_spesifikasi['produk']:"";
                            $kode_spesifikasi = (isset($data_spesifikasi['kode_spesifikasi']))?$data_spesifikasi['kode_spesifikasi']:"";
                            $modul_tipe = (isset($data_spesifikasi['modul_tipe']))?$data_spesifikasi['modul_tipe']:"";
                            $spesifikasi = (isset($data_spesifikasi['spesifikasi']))?$data_spesifikasi['spesifikasi']:"";
                            $harga = (isset($data_spesifikasi['harga']))?$data_spesifikasi['harga']:"";
                            
                            $data_kontrak = $this->master->get_value("mk_master_kontrak","","mk_master_kontrak_id = '".$d['master_kontrak_id']."'");
                            $kontrak = (isset($data_kontrak['nomor_kontrak']))?$data_kontrak['nomor_kontrak']:"";
                            $kontrak .= (isset($data_kontrak['judul_kontrak']))?" - ".$data_kontrak['judul_kontrak']:"";
                            
                            $data_pks = $this->master->get_value("data_pks","","data_pks_id = '".$d['pks_id']."'");
                            $pks = (isset($data_pks['kode_pks']))?$data_pks['kode_pks']:"";
                            $pks .= (isset($data_pks['judul_pks']))?" - ".$data_pks['judul_pks']:"";
                            
                            $row_cakupan_kerja = "";
                            $qtotal_desa = $this->model_cakupan_pekerjaan->get_total_work_location($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                            
                            $total_desa = (is_array($qtotal_desa) and count($qtotal_desa) > 0)?count($qtotal_desa):0;
                            $total_product = $this->model_cakupan_pekerjaan->get_total_product($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                            for($i = 0; $i < $weeks; $i++)
                            {
                              $detail_cakupan_kerja = $this->model_cakupan_pekerjaan->get_date_number_by_week($tanggal_awal,$tanggal_akhir,$i,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                              $jumlah_desa = (is_array($detail_cakupan_kerja) and count($detail_cakupan_kerja) > 0)?count($detail_cakupan_kerja):0;
                              $total_product_row = 0;
                              $satuan_row = " items";
                              $range_date_week = $this->model_cakupan_pekerjaan->get_date_range_by_number_week($tanggal_awal,$tanggal_akhir,$i);
                              $title_start_date = (isset($range_date_week['start_date']))?$range_date_week['start_date']:$tanggal_awal;
                              $title_end_date = (isset($range_date_week['end_date']))?$range_date_week['end_date']:$tanggal_akhir;
                              if(is_array($detail_cakupan_kerja) and count($detail_cakupan_kerja) > 0)
                              {
                                foreach($detail_cakupan_kerja as $r => $rd)
                                {
                                  $total_product_row += (isset($rd['nilai']))?$rd['nilai']:0;
                                  $satuan_row = (isset($rd['satuan']) and !empty($rd['satuan']))?$rd['satuan']:" items";
                                }
                              }
                              $btn_class = ($jumlah_desa > 0)?'btn-success':'btn-warning';
															$row_cakupan_kerja .= '<td class="text-center"><button type="button" class="btn '.$btn_class.' btn-xs detail_cakupan_kerja" data-toggle="modal" data-title="Detail Cakupan Pekerjaan Minggu ke '.($i+1).' Tanggal ' . $this->data->human_date($title_start_date) . ' sampai ' . $this->data->human_date($title_end_date).'" data-target="#modal_cakupan_pekerjaan_detail" data-href="'.base_url().'admin/mk_cakupan_pekerjaan/detail_week/'.$title_start_date.'/'.$title_end_date.'/'.$i.'/'.$data_filter_master_kontrak_id.'/'.$d['spesifikasi_id'].'">'.$jumlah_desa.' desa | '. $total_product_row . ''.$satuan_row.'</button></td>';
														}
                            $panel_function = (isset($this->init['panel_function']))?$this->init['panel_function']:array();
                            $the_action = $this->data->show_panel_allowed("","admin","mk_cakupan_pekerjaan",$panel_function,$mk_cakupan_kerja_id);
                            $list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
                            $the_action  = $this->hook->do_action('hook_create_listing_action',$the_action);
                            $the_action = str_replace("btn-sm","btn-xs",$the_action);
                        ?>
                          <tr>
                            <td class="text-center"><?php echo $no;?></td>
                            <td nowrap="nowrap"><?php echo $kontrak;?></td>
                            <td nowrap="nowrap"><?php echo $pks;?></td>
                            <td nowrap="nowrap"><?php echo $kode_spesifikasi .' - '. $produk;?></td>
                            <td class="text-center"><?php echo $total_desa;?></td>
                            <td class="text-center"><?php echo $total_product;?></td>
                            <td class="text-center bg-black">&nbsp;</td>
                            <?php
                            echo $row_cakupan_kerja;
                            if(!empty($the_action))
                            {
                            ?>
                              <?php /*<td <?php echo $list_style;?>><?php echo $the_action;?></td>*/ ?>
                            <?php
                            }
                            ?>
                          </tr>
                        <?php
                            $no++;
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                    <?php
                      }else{
                          echo '<div class="alert alert-warning" role="alert">Silahkan filter berdasarkan kontrak terlebih dahulu</div>';
                      }
                    ?>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                   
                  <?php
                    #$init = $this->init;
                    #echo $this->data->create_form_filter($init);
                  ?> 
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">                
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
						<h3>Import Data</h3>
						<p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
						<?php
						#$init = $this->init;
						#echo $this->data->create_form_import($init);
						echo $this->importer->upload_file_form();
						?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="export<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_export($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="modal_cakupan_pekerjaan_detail">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script type="text/javascript">
    $(document).ready(function(){
     $('#modal_cakupan_pekerjaan_detail').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var get_content = $.post( $(button).attr("data-href"), { is_ajax: "1"}, function(){});
        var content = "";
        var modal = $(this);
        modal.find('.modal-body').append('<div class="alert alert-warning">Loading...</div>');
        modal.find('.modal-title').text($(button).data("title"));
        get_content.done(function( data ) {
          var data2 = $("<div></div>").append(data);
          var content = $(data2).find(".ajax_container_detail_week .panel-body");
          modal.find('.modal-body').empty().append( content );
        });
      });
    });
  </script>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
