<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
            <?php
            /*
            <?php
              $create_listing = $this->data->create_listing($this->init);
              $data_rows = $this->data->data_rows;

              $filters = $this->session->userdata("mk_cakupan_pekerjaan_data_filter");
              $data_filter_master_kontrak_id  = (isset($filters['master_kontrak_id']))?$filters['master_kontrak_id']:"";

                $data_kontrak = $this->master->get_value("mk_master_kontrak","","mk_master_kontrak_id = '".$data_filter_master_kontrak_id."'");

                $tanggal_awal	= (isset($data_kontrak['tanggal_mulai']))?$data_kontrak['tanggal_mulai']:"";
                $tanggal_akhir	= (isset($data_kontrak['tanggal_berakhir']))?$data_kontrak['tanggal_berakhir']:"";
              $weeks = $this->model_cakupan_pekerjaan->get_weeks($data_filter_master_kontrak_id);

              $months = 0;
              if(isset($weeks) and $weeks > 0)
              {
                $months = ceil($weeks/4);
              }
              if(!empty($data_filter_master_kontrak_id))
              {
            ?>
            <table width="100%" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th rowspan="3" class="text-center">No</th>
                  <th rowspan="3" class="text-center">Kontrak</th>
                  <th rowspan="3" class="text-center">PKS</th>
                  <th rowspan="3" class="text-center">Cakupan Pekerjaan</th>
                  <th rowspan="2" colspan="2"class="text-center">Jumlah Proyek Lokasi <br/>(Desa/Kelurahan)</th>
                  <th class="text-center" colspan="<?php echo ($months*4)+1;?>"><?php echo $this->data->human_date($tanggal_awal);?> sampai <?php echo $this->data->human_date($tanggal_akhir);?></th>
                  <?php /*<th rowspan="3" class="text-center">Actions</th>* / ?>
                </tr>
                <tr>
                  <th class="text-center">Bulan ke </th>
                  <?php
                  $mod_weeks = $weeks%4;
                  for($i = 1; $i <= $months; $i++)
                  {
                                            ?>
                  <th colspan="<?php echo 4-$mod_weeks;?>" class="text-center"><?php echo $i;?></th>
                  <?php
                                            }
                                            ?>
                </tr>
                <tr>
                  <th class="text-center">Jumlah Desa</th>
                  <th class="text-center">Jumlah Barang/Jasa</th>
                  <th class="text-center">Minggu ke </th>
                                            <?php
                                            for($i = 1; $i <= $weeks; $i++)
                                            {
                                                echo '<th class="text-center">'.$i.'</th>';
                                            }
                                            ?>
                </tr>
              </thead>				
              <tbody>
                <?php
                if(is_array($data_rows) and count($data_rows) > 0)
                {
                  $no = 1;
                  foreach($data_rows as $i => $d)
                  {
                    $mk_harga_dan_total_biaya_id = $d['mk_harga_dan_total_biaya_id'];
                    $master_kontrak_id = $d['master_kontrak_id'];
                    $master_vendor_id = $d['master_vendor_id'];
                    $pks_id = $d['pks_id'];
                    $spesifikasi_id = $d['spesifikasi_id'];
                    $desa_id = $d['desa_id'];
                    $nilai = $d['nilai'];
                    $satuan = $d['satuan'];
                    $tanggal_rencana = $d['tanggal_rencana'];
                    $tahun = $d['tahun'];
                    $bulan = $d['bulan'];
                    $minggu = $d['minggu'];
                    
                    $data_spesifikasi = $this->master->get_value("mk_spesifikasi","","mk_spesifikasi_id = '".$d['spesifikasi_id']."'");
                    $nama_desa = (isset($data_desa['nama_desa']))?$data_desa['nama_desa']:"";
                    $data_desa = $this->master->get_value("data_desa","","data_desa_id = '".$d['desa_id']."'");
                    $mk_spesifikasi_id = $d['spesifikasi_id'];
                    $produk = (isset($data_spesifikasi['produk']))?$data_spesifikasi['produk']:"";
                    $kode_spesifikasi = (isset($data_spesifikasi['kode_spesifikasi']))?$data_spesifikasi['kode_spesifikasi']:"";
                    $modul_tipe = (isset($data_spesifikasi['modul_tipe']))?$data_spesifikasi['modul_tipe']:"";
                    $spesifikasi = (isset($data_spesifikasi['spesifikasi']))?$data_spesifikasi['spesifikasi']:"";
                    $harga = (isset($data_spesifikasi['harga']))?$data_spesifikasi['harga']:"";
                    
                    $data_kontrak = $this->master->get_value("mk_master_kontrak","","mk_master_kontrak_id = '".$d['master_kontrak_id']."'");
                    $kontrak = (isset($data_kontrak['nomor_kontrak']))?$data_kontrak['nomor_kontrak']:"";
                    $kontrak .= (isset($data_kontrak['judul_kontrak']))?" - ".$data_kontrak['judul_kontrak']:"";
                    
                    $data_pks = $this->master->get_value("data_pks","","data_pks_id = '".$d['pks_id']."'");
                    $pks = (isset($data_pks['kode_pks']))?$data_pks['kode_pks']:"";
                    $pks .= (isset($data_pks['judul_pks']))?" - ".$data_pks['judul_pks']:"";
                    
                    $row_harga_dan_total_biaya = "";
                    $qtotal_desa = $this->model_cakupan_pekerjaan->get_total_work_location($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                    
                    $total_desa = (is_array($qtotal_desa) and count($qtotal_desa) > 0)?count($qtotal_desa):0;
                    $total_product = $this->model_cakupan_pekerjaan->get_total_product($tanggal_awal,$tanggal_akhir,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                    for($i = 0; $i < $weeks; $i++)
                    {
                      $detail_harga_dan_total_biaya = $this->model_cakupan_pekerjaan->get_date_number_by_week($tanggal_awal,$tanggal_akhir,$i,$data_filter_master_kontrak_id,$d['spesifikasi_id']);
                      $jumlah_desa = (is_array($detail_harga_dan_total_biaya) and count($detail_harga_dan_total_biaya) > 0)?count($detail_harga_dan_total_biaya):0;
                      $total_product_row = 0;
                      $satuan_row = " items";
                      $range_date_week = $this->model_cakupan_pekerjaan->get_date_range_by_number_week($tanggal_awal,$tanggal_akhir,$i);
                      $title_start_date = (isset($range_date_week['start_date']))?$range_date_week['start_date']:$tanggal_awal;
                      $title_end_date = (isset($range_date_week['end_date']))?$range_date_week['end_date']:$tanggal_akhir;
                      if(is_array($detail_harga_dan_total_biaya) and count($detail_harga_dan_total_biaya) > 0)
                      {
                        foreach($detail_harga_dan_total_biaya as $r => $rd)
                        {
                          $total_product_row += (isset($rd['nilai']))?$rd['nilai']:0;
                          $satuan_row = (isset($rd['satuan']) and !empty($rd['satuan']))?$rd['satuan']:" items";
                        }
                      }
                      $btn_class = ($jumlah_desa > 0)?'btn-success':'btn-warning';
                                                    $row_harga_dan_total_biaya .= '<td class="text-center"><button type="button" class="btn '.$btn_class.' btn-xs detail_harga_dan_total_biaya" data-toggle="modal" data-title="Detail Cakupan Pekerjaan Minggu ke '.($i+1).' Tanggal ' . $this->data->human_date($title_start_date) . ' sampai ' . $this->data->human_date($title_end_date).'" data-target="#modal_cakupan_pekerjaan_detail" data-href="'.base_url().'admin/mk_cakupan_pekerjaan/detail_week/'.$title_start_date.'/'.$title_end_date.'/'.$i.'/'.$data_filter_master_kontrak_id.'/'.$d['spesifikasi_id'].'">'.$jumlah_desa.' desa | '. $total_product_row . ''.$satuan_row.'</button></td>';
                                                }
                    $panel_function = (isset($this->init['panel_function']))?$this->init['panel_function']:array();
                    $the_action = $this->data->show_panel_allowed("","admin","mk_cakupan_pekerjaan",$panel_function,$mk_harga_dan_total_biaya_id);
                    $list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
                    $the_action  = $this->hook->do_action('hook_create_listing_action',$the_action);
                    $the_action = str_replace("btn-sm","btn-xs",$the_action);
                ?>
                  <tr>
                    <td class="text-center"><?php echo $no;?></td>
                    <td nowrap="nowrap"><?php echo $kontrak;?></td>
                    <td nowrap="nowrap"><?php echo $pks;?></td>
                    <td nowrap="nowrap"><?php echo $kode_spesifikasi .' - '. $produk;?></td>
                    <td class="text-center"><?php echo $total_desa;?></td>
                    <td class="text-center"><?php echo $total_product;?></td>
                    <td class="text-center bg-black">&nbsp;</td>
                    <?php
                    echo $row_harga_dan_total_biaya;
                    if(!empty($the_action))
                    {
                    ?>
                      <?php /*<td <?php echo $list_style;?>><?php echo $the_action;?></td>* / ?>
                    <?php
                    }
                    ?>
                  </tr>
                <?php
                    $no++;
                  }
                }
                ?>
              </tbody>
            </table>
            <?php
              }else{
                  echo '<div class="alert alert-warning" role="alert">Silahkan filter berdasarkan kontrak terlebih dahulu</div>';
              }
            ?>
            */
            ?>
    
        <?php
          if(isset($data_harga_dan_total_biaya) and is_array($data_harga_dan_total_biaya) and count($data_harga_dan_total_biaya) > 0)
          {
          ?>
        <br/>
         <h3 style="text-align: center;">Harga dan Total Biaya</h3>
         <h4 style="text-align: center;">Kontrak :  <?php echo $other_data['nomor_kontrak'];?> - <?php echo $other_data['judul_kontrak'];?></h4>
         <h4 style="text-align: center;">PKS :  <?php echo $other_data['kode_pks'];?> - <?php echo $other_data['judul_pks'];?></h4>

        <div class="table-responsive">
          <table width="100%" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Produk/Jasa</th>
                <th>Module/Tipe</th>
                <th>Spesifikasi</th>
                <th>volume</th>
                <th>Satuan</th>
                <th>Jenis Pengadaan</th>
                <th>Mata Uang</th>
                <th class="bg-success">Harga Satuan</th>
                <th class="bg-success">Harga FOB</th>
                <th class="bg-success">Freight</th>
                <th>Exchange Rate</th>
                <th class="bg-success">Bea Masuk</th>
                <th class="bg-success">PPN</th>
                <th class="bg-success">PPH</th>
                <th class="bg-success">Total</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $row_output = "";
              if(is_array($data_harga_dan_total_biaya) and count($data_harga_dan_total_biaya) > 0)
              {
                $total_harga_satuan = 0;
                $total_harga_fob = 0;
                $total_freight = 0;
                $total_bea_masuk = 0;
                $total_ppn = 0;
                $total_pph = 0;
                $total_grand = 0;
                foreach($data_harga_dan_total_biaya as $i => $rows)
                {
                  $kontrak  = $this->master->get_value("mk_master_kontrak","",array("mk_master_kontrak_id" => $rows['master_kontrak_id']));
                  $pks  = $this->master->get_value("data_pks","",array("data_pks_id" => $rows['pks_id']));
                  $produk  = $this->master->get_value("mk_spesifikasi","",array("mk_spesifikasi_id" => $rows['spesifikasi_id']));
                  
                  $total_harga_satuan += ($rows['mata_uang'] == 'IDR')?$rows['harga_satuan']:($rows['harga_satuan']*$rows['exchange_rate']);
                  $total_harga_fob += ($rows['mata_uang'] == 'IDR')?$rows['harga_fob']:($rows['harga_fob']*$rows['exchange_rate']);
                  $total_freight += ($rows['mata_uang'] == 'IDR')?$rows['freight']:($rows['freight']*$rows['exchange_rate']);
                  $total_bea_masuk += ($rows['mata_uang'] == 'IDR')?$rows['bea_masuk']:($rows['bea_masuk']*$rows['exchange_rate']);
                  $total_ppn += ($rows['mata_uang'] == 'IDR')?$rows['ppn']:($rows['ppn']*$rows['exchange_rate']);
                  $total_pph += ($rows['mata_uang'] == 'IDR')?$rows['pph']:($rows['pph']*$rows['exchange_rate']);
                  $total_grand += ($rows['mata_uang'] == 'IDR')?$rows['total']:($rows['total']*$rows['exchange_rate']);
                  
                  $currency = ($rows['mata_uang'] == 'IDR')?'Rp. ':'$';
                  $row_output .= '
                  <tr>
                    <td>'.($i+1).'</td>
                    <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_spesifikasi_id',$rows['spesifikasi_id']).'</td>
                    <td>'.$rows['produk'].'</td>
                    <td>'.$rows['spesifikasi'].'</td>
                    <td>'.$rows['volume'].'</td>
                    <td>'.$rows['satuan'].'</td>
                    <td>'.$rows['import'].'</td>
                    <td>'.$rows['mata_uang'].'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['harga_satuan'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['harga_fob'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['freight'],$currency).'</td>
                    <td align="right">'. (($rows['exchange_rate'] > 0)?format_currency($rows['exchange_rate'],'Rp. '):'-') .'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['bea_masuk'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['ppn'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['pph'],$currency).'</td>
                    <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['total'],$currency).'</td>
                  </tr>';
                }
                  $row_output .= '
                  <tr>
                    <td align="right" valign="middle" colspan="8" class="bg-blue">TOTAL</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_harga_satuan,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_harga_fob,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_freight,'Rp. ').'</td>
                    <td class="bg-blue">&nbsp;</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_bea_masuk,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_ppn,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_pph,'Rp. ').'</td>
                    <td nowrap="nowarap" align="right" class="bg-blue">'.format_currency($total_grand,'Rp. ').'</td>
                  </tr>';
              }
              else
              {
                
              }
              echo $row_output;
              ?>
            </tbody>
          </table>
        </div>
            <?php
              }else{
                  echo '<div class="alert alert-warning" role="alert">Data cakupan pekerjaan tidak tersedia.</div>';
              }
            ?>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
