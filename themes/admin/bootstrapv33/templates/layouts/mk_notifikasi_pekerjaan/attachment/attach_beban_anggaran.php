<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
             <h3 style="text-align: center;">Beban Anggaran</h3>
             <h4 style="text-align: center;">Kontrak :  <?php echo $other_data['nomor_kontrak'];?> - <?php echo $other_data['judul_kontrak'];?></h4>
             <h4 style="text-align: center;">PKS :  <?php echo $other_data['kode_pks'];?> - <?php echo $other_data['judul_pks'];?></h4>

            <table width="100%" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Vendor</th>
                  <th>Sumber Pendanaan</th>
                  <th class="bg-success">Beban Anggaran</th>
                  <th class="bg-success">Dana Alokasi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $row_output = "";
                if(is_array($data_beban_anggaran) and count($data_beban_anggaran) > 0)
                {
                  $total_beban_anggaran = 0;
                  $total_dana_alokasi = 0;
                  
                  foreach($data_beban_anggaran as $i => $rows)
                  {                    
                    $currency = 'Rp. ';
                    $total_beban_anggaran += $rows['beban_anggaran'];
                    $total_dana_alokasi += $rows['dana_alokasi'];
                    $row_output .= '
                    <tr>
                      <td>'.($i+1).'</td>
                      <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_master_vendor_id',$rows['master_vendor_id']).'</td>
                       <td nowrap="nowrap">'.$this->hook->do_action('hook_create_listing_value_sumber_pendanaan_id',$rows['sumber_pendanaan_id']).'</td>
                      <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['beban_anggaran'],$currency).'</td>
                      <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($rows['dana_alokasi'],$currency).'</td>
                    </tr>';
                  }
                    $row_output .= '
                    <tr>
                      <td colspan="3" align="right" class="bg-blue">TOTAL</td>
                      <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($total_beban_anggaran,$currency).'</td>
                      <td nowrap="nowrap" align="right" class="bg-success">'.format_currency($total_dana_alokasi,$currency).'</td>
                    </tr>';
                }
                else
                {
                  
                }
                echo $row_output;
                ?>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
