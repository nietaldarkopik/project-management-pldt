<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
            <table class="table table-striped" width="100%">
              <tbody>
                <tr class="bg-blue"><td width="25%" class="bg-blue">Data Perusahaan/Instansi</td><td width="1%" class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Kode Sumber Pendanaan</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['kode_sumber_pendanaan']))?$data_sumber_pendanaan['kode_sumber_pendanaan']:"";?></td></tr>
                <tr><td>Kode Akun Pendanaan</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['akun_pendanaan']))?$data_sumber_pendanaan['akun_pendanaan']:"";?></td></tr>
                <tr><td>Nama Instansi</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['instansi']))?$data_sumber_pendanaan['instansi']:"";?></td></tr>
                <tr><td>Alamat Instansi</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['alamat_instansi']))?$data_sumber_pendanaan['alamat_instansi']:"";?></td></tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr class="bg-blue"><td class="bg-blue">Data Personal</td><td class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Nama Lengkap</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['nama']))?$data_sumber_pendanaan['nama']:"";?></td></tr>
                <tr><td>Jabatan</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['jabatan']))?$data_sumber_pendanaan['jabatan']:"";?></td></tr>
                <tr><td>No. Telepon</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['no_telepon']))?$data_sumber_pendanaan['no_telepon']:"";?></td></tr>
                <tr><td>Email</td><td>:</td><td><?php echo (isset($data_sumber_pendanaan['email']))?$data_sumber_pendanaan['email']:"";?></td></tr>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
