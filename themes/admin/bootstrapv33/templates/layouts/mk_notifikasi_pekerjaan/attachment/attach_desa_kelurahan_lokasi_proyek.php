<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
             <h3 style="text-align: center;">Desa/Kelurahan Lokasi Proyek</h3>
             <h4 style="text-align: center;">Kontrak :  <?php echo $other_data['nomor_kontrak'];?> - <?php echo $other_data['judul_kontrak'];?></h4>
             <h4 style="text-align: center;">PKS :  <?php echo $other_data['kode_pks'];?> - <?php echo $other_data['judul_pks'];?></h4>
            <table class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <tr>
                  <th>Desa/Kelurahan</th>
                  <th>Kecamatan</th>
                  <th>Kota</th>
                  <th>Propinsi</th>
                </tr>
              </thead>				
              <tbody>
              <?php
                  if(is_array($data_desa_kelurahan_lokasi_proyek) AND COUNT($data_desa_kelurahan_lokasi_proyek) > 0)
                  {
                      foreach($data_desa_kelurahan_lokasi_proyek as $i => $r)
                      {
                            $this->db->where(array("data_desa_id" => $r['desa_id']));
                            $desa = $this->db->get("data_desa")->row_array();
                            
                            $this->db->where(array("data_kecamatan_id" => $r['kecamatan_id']));
                            $kecamatan = $this->db->get("data_kecamatan")->row_array();
                            
                            $this->db->where(array("data_kota_id" => $r['kota_id']));
                            $kota = $this->db->get("data_kota")->row_array();
                            
                            $this->db->where(array("data_propinsi_id" => $r['propinsi_id']));
                            $propinsi = $this->db->get("data_propinsi")->row_array();
                            
                  ?>
                    <tr>
                      <td><?php echo (isset($desa['nama_desa']))?$desa['nama_desa']:"";?></td>
                      <td><?php echo (isset($kecamatan['nama_kecamatan']))?$kecamatan['nama_kecamatan']:"";?></td>
                      <td><?php echo (isset($kota['nama_kota']))?$kota['nama_kota']:"";?></td>
                      <td><?php echo (isset($propinsi['nama_propinsi']))?$propinsi['nama_propinsi']:"";?></td>
                    </tr>
                  <?php
                      }
                }else{
                  echo '<div class="alert alert-warning" role="alert">Data Syarat dan Ketentuan tidak ditemukan</div>';
                }
              ?>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
