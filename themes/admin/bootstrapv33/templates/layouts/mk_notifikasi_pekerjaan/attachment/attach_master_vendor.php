<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
            <table class="table table-striped" width="100%">
              <tbody>
                <tr class="bg-blue"><td width="25%" class="bg-blue">Data Perusahaan/Instansi</td><td width="1%" class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Kode Vendor</td><td>:</td><td><?php echo (isset($data_vendor['kode_vendor']))?$data_vendor['kode_vendor']:"";?></td></tr>
                <tr><td>Nama Perusahaan/Instansi</td><td>:</td><td><?php echo (isset($data_vendor['perusahaan']))?$data_vendor['perusahaan']:"";?></td></tr>
                <tr><td>Akta Pendirian</td><td>:</td><td><?php echo (isset($data_vendor['akta_pendirian']))?$this->data->human_date($data_vendor['akta_pendirian']):"";?></td></tr>
                <tr><td>Alamat Perusahaan/Instansi</td><td>:</td><td><?php echo (isset($data_vendor['alamat_perusahaan']))?$data_vendor['alamat_perusahaan']:"";?></td></tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr class="bg-blue"><td class="bg-blue">Data Personal</td><td class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Nama Lengkap</td><td>:</td><td><?php echo (isset($data_vendor['nama']))?$data_vendor['nama']:"";?></td></tr>
                <tr><td>No Identitas/KTP</td><td>:</td><td><?php echo (isset($data_vendor['no_identitas']))?$data_vendor['no_identitas']:"";?></td></tr>
                <tr><td>Tanggal Lahir</td><td>:</td><td><?php echo (isset($data_vendor['tempat_lahir']))?$data_vendor['tempat_lahir']:"";?></td></tr>
                <tr><td>Tempat Lahir</td><td>:</td><td><?php echo (isset($data_vendor['tanggal_lahir']))?$this->data->human_date($data_vendor['tanggal_lahir']):"";?></td></tr>
                <tr><td>Alamat Lengkap</td><td>:</td><td><?php echo (isset($data_vendor['alamat']))?$data_vendor['alamat']:"";?></td></tr>
                <tr><td>No. Telepon</td><td>:</td><td><?php echo (isset($data_vendor['no_telepon']))?$data_vendor['no_telepon']:"";?></td></tr>
                <tr><td>Email</td><td>:</td><td><?php echo (isset($data_vendor['email']))?$data_vendor['email']:"";?></td></tr>
                <tr><td>Jabatan</td><td>:</td><td><?php echo (isset($data_vendor['jabatan']))?$data_vendor['jabatan']:"";?></td></tr>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
