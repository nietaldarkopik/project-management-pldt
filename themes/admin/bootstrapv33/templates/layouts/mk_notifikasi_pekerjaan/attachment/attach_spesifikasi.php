<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
            <table class="table table-striped" width="100%">
              <tbody>
                <tr><td width="25%">Nomor Kontrak</td><td width="1%">:</td><td><?php echo (isset($data_kontrak['nomor_kontrak']))?$data_kontrak['nomor_kontrak']:"";?></td></tr>
                <tr><td>Judul Kontrak</td><td>:</td><td><?php echo (isset($data_kontrak['judul_kontrak']))?$data_kontrak['judul_kontrak']:"";?></td></tr>
                <tr><td>PKS</td><td>:</td><td><?php echo (isset($data_kontrak['pks']))?$data_kontrak['pks']:"";?></td></tr>
                <tr><td>Vendor</td><td>:</td><td><?php echo (isset($data_kontrak['vendor']))?$data_kontrak['vendor']:"";?></td></tr>
                <tr><td>Sumber Pendanaan</td><td>:</td><td><?php echo (isset($data_kontrak['sumber_pendanaan']))?$data_kontrak['sumber_pendanaan']:"";?></td></tr>
                <?php /*<tr><td>File Kontrak</td><td>:</td><td><?php echo (isset($data_kontrak['protected']))?$data_kontrak['protected']:"";?></td></tr>*/ ?>
                <tr><td>Tanggal Mulai</td><td>:</td><td><?php echo (isset($data_kontrak['tanggal_mulai']))?$this->data->human_date($data_kontrak['tanggal_mulai']):"";?></td></tr>
                <tr><td>Tanggal Berakhir</td><td>:</td><td><?php echo (isset($data_kontrak['tanggal_berakhir']))?$this->data->human_date($data_kontrak['tanggal_berakhir']):"";?></td></tr>
                <tr><td>Tanggal Jatuh Tempo</td><td>:</td><td><?php echo (isset($data_kontrak['tanggal_jatuh_tempo']))?$this->data->human_date($data_kontrak['tanggal_jatuh_tempo']):"";?></td></tr>
                <tr><td>Total Anggaran</td><td>:</td><td><?php echo (isset($data_kontrak['total_anggaran']))?format_currency($data_kontrak['total_anggaran'],'Rp. '):"";?></td></tr>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
