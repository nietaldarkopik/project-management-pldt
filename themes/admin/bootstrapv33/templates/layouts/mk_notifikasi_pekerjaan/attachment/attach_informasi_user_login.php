<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}

$vendor = array();
$sumber_pendanaan = array();
if(isset($data_user_login) and is_array($data_user_login) and count($data_user_login) > 0)
{
    extract($data_user_login);
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
            <table class="table table-striped" width="100%">
              <tbody>
								<?php
								if(isset($other_data['type_user']) and $other_data['type_user'] == 'vendor')
								{
								?>
                <tr><td width="25%" class="bg-blue">Account Login Vendor</td><td width="1%" class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Username</td><td width="1%">:</td><td><?php echo (isset($vendor['username']))?$vendor['username']:"";?></td></tr>
                <tr><td>Password</td><td>:</td><td><?php echo (isset($vendor['password']))?$vendor['password']:"";?></td></tr>
                <tr><td>&nbsp;</td><td width="1%">&nbsp;</td><td>&nbsp;</td></tr>
								<?php
								}
								?>
								<?php
								if(isset($other_data['type_user']) and $other_data['type_user'] == 'sumber_pendanaan')
								{
								?>
                <tr><td width="25%" class="bg-blue">Account Login Sumber Pendanaan</td><td width="1%" class="bg-blue">&nbsp;</td><td class="bg-blue">&nbsp;</td></tr>
                <tr><td>Username</td><td width="1%">:</td><td><?php echo (isset($sumber_pendanaan['username']))?$sumber_pendanaan['username']:"";?></td></tr>
                <tr><td>Password</td><td>:</td><td><?php echo (isset($sumber_pendanaan['password']))?$sumber_pendanaan['password']:"";?></td></tr>
                <tr><td>&nbsp;</td><td width="1%">&nbsp;</td><td>&nbsp;</td></tr>
								<?php
								}
								?>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
