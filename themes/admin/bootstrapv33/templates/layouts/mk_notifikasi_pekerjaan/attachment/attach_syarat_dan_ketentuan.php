<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
      <?php
      if(isset($page_title) and !empty($page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
            <br/>
             <h3 style="text-align: center;">Syarat dan Ketentuan</h3>
             <h4 style="text-align: center;">Kontrak :  <?php echo $other_data['nomor_kontrak'];?> - <?php echo $other_data['judul_kontrak'];?></h4>
             <h4 style="text-align: center;">PKS :  <?php echo $other_data['kode_pks'];?> - <?php echo $other_data['judul_pks'];?></h4>
            <table class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <tr>
                  <th width="20%">Nomor Pasal</th>
                  <th>Bunyi Pasal</th>
                </tr>
              </thead>				
              <tbody>
              <?php
                  if(is_array($data_syarat_dan_ketentuan) AND COUNT($data_syarat_dan_ketentuan) > 0)
                  {
                      foreach($data_syarat_dan_ketentuan as $i => $r)
                      {                            
                  ?>
                    <tr>
                      <td align="center"><?php /*echo $no; $no++;*/?><?php echo $r['no_pasal'];?></td>
                      <td style="text-align: justify;"><strong style="text-align:center;"><?php echo $r['judul_pasal'];?></strong><br/><?php echo $r['pasal'];?></td>
                    </tr>
                  <?php
                      }
                }else{
                  echo '<div class="alert alert-warning" role="alert">Data Syarat dan Ketentuan tidak ditemukan</div>';
                }
              ?>
              </tbody>
            </table>
        </div>
      </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('footer');?>
