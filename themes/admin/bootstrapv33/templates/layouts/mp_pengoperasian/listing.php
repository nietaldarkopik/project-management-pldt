<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
                <!--<li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>-->
                <li class="hide"><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li>
                <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li>
                <!--<li><a href="#export<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-export"></span></a></li>-->
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      $listing =  $this->data->create_listing($this->init);
                    ?>
                    <table class="table table-striped table-bordered table-hover" width="100%">
                      <thead>
                        <tr>
                          <th rowspan="2" class="no-print bulk_data_all_th" align="center" valign="middle" width="5"><input name="bulk_data_all" value="1" class="bulk_data_all" type="checkbox"></th>
                          <th rowspan="2">No</th>
                          <th rowspan="2">Kode Desa</th>
                          <th rowspan="2">Desa/Kelurahan</th>
                          <th rowspan="2">Kecamatan</th>
                          <th rowspan="2">DAT2</th>
                          <th rowspan="2">Kota/Kab</th>
                          <th rowspan="2">Tanggal Operasi</th>
                          <th colspan="4">Penanggung Jawab Pengoperasian</th>
                          <th rowspan="2" class="action_menu_col">Action</th>
                        </tr>
                        <tr>
                          <th>Koperasi</th>
                          <th>Pengurus</th>
                          <th>Alamat</th>
                          <th>No. Telepon</th>
                        </tr>
                      </thead>				
                      <tbody>
                      <?php
                        $data_rows = $this->data->data_rows;
                        $no = 1;
                        if(is_array($data_rows) AND COUNT($data_rows) > 0)
                        {
                          foreach($data_rows as $i => $r)
                          {
                            $data_desa = $this->master->get_value("data_desa","","data_desa_id = '".$r['desa_id']."'");
                            $kode_pos = (isset($data_desa['kode_desa']))?$data_desa['kode_desa']:"";
                            $nama_desa = (isset($data_desa['nama_desa']))?$data_desa['nama_desa']:"";
                            $kecamatan_id = (isset($data_desa['kecamatan_id']))?$data_desa['kecamatan_id']:"";
                            $kota_id = (isset($data_desa['kota_id']))?$data_desa['kota_id']:"";
                            $nama_kecamatan = $this->master->get_value("data_kecamatan","nama_kecamatan","data_kecamatan_id = '".$kecamatan_id."'");
                            $data_kota = $this->master->get_value("data_kota","","data_kota_id = '".$kota_id."'");
                            $dat2 = (isset($data_kota['dat2']))?$data_kota['dat2']:"";
                            $nama_kota = (isset($data_kota['nama_kota']))?$data_kota['nama_kota']:"";
                            $tanggal_operasi = $r['tanggal_operasi'];
                            $data_koperasi = $this->master->get_value("mk_koperasi","","mk_koperasi_id = '".$r['koperasi_id']."'");
                            $pengurus = (isset($data_koperasi['nama']))?$data_koperasi['nama']:'';
                            $alamat = (isset($data_koperasi['alamat']))?$data_koperasi['alamat']:'';
                            $nama_koperasi = (isset($data_koperasi['nama_koperasi']))?$data_koperasi['nama_koperasi']:'';
                            $no_telepon = (isset($data_koperasi['no_telepon']))?$data_koperasi['no_telepon']:'';
                      ?>
                            <tr>
                              <td class="no-print" align="center" valign="middle"><input name="bulk_data[]" value="<?php echo $r['mp_pengoperasian_id'];?>" class="bulk_data" type="checkbox"></td>
                              <td align="center"><?php echo $no; $no++;?></td>
                              <td style="white-space: nowrap;"><?php echo $kode_pos;?></td>
                              <td style="white-space: nowrap;"><?php echo $nama_desa;?></td>
                              <td style="white-space: nowrap;"><?php echo $nama_kecamatan;?></td>
                              <td style="white-space: nowrap;"><?php echo $dat2;?></td>
                              <td style="white-space: nowrap;"><?php echo $nama_kota;?></td>
                              <td style="white-space: nowrap;"><?php echo $tanggal_operasi;?></td>
                              <td style="white-space: nowrap;"><?php echo $nama_koperasi;?></td>
                              <td style="white-space: nowrap;"><?php echo $pengurus;?></td>
                              <td style="white-space: nowrap;"><?php echo $alamat;?></td>
                              <td style="white-space: nowrap;"><?php echo $no_telepon;?></td>
                              <td class="rows_action">
                                <div class="btn-group">
                                  <button type="button" class="btn btn-primary btn-sm">Choose Action</button>
                                  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
                                    <li role="presentation" class="">
                                      <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_pengoperasian/edit/<?php echo $r['mp_pengoperasian_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Edit</a>
                                    </li>
                                    <li role="presentation" class="">
                                      <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_pengoperasian/view/<?php echo $r['mp_pengoperasian_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> View</a>
                                    </li>
                                    <li role="presentation" class="">
                                      <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_pengoperasian/delete/<?php echo $r['mp_pengoperasian_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Delete</a>
                                    </li>
                                  </ul>
                                </div>
                              </td>
                            </tr>
                      <?php
                          }
                        }
                      ?>
                      </tbody>
                    </table>
                  
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                   
                  <?php
                    #$init = $this->init;
                    #echo $this->data->create_form_filter($init);
                  ?> 
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
						<h3>Import Data</h3>
						<p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
						<?php
						#$init = $this->init;
						#echo $this->data->create_form_import($init);
						echo $this->importer->upload_file_form();
						?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="export<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_export($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

