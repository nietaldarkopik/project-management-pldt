<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $page_title;?></h4>
      </div>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12 margin20">
          <?php 
            echo $response;
            
            $berita_acara = (isset($data_berita_acara['berita_acara']))?$data_berita_acara['berita_acara']:'';
            $master_kontrak_id = (isset($master_kontrak_id))?$master_kontrak_id:"";
            $desa_id = (isset($desa_id))?$desa_id:"";
            if(empty($berita_acara))
            {
              $this->db->where(array("mk_master_kontrak_id" => $master_kontrak_id));
              $q_master_kontrak = $this->db->get("mk_master_kontrak");
              $master_kontrak = $q_master_kontrak->row_array();
              $nomor_kontrak = (isset($master_kontrak['nomor_kontrak']))?$master_kontrak['nomor_kontrak']:'';
              
              $pks_id = (isset($master_kontrak['pks_id']))?$master_kontrak['pks_id']:'';
              $this->db->where(array("data_pks_id" => $pks_id));
              $q_pks = $this->db->get("data_pks");
              $pks = $q_pks->row_array();
              $pks_no = (isset($pks['kode_pks']))?$pks['kode_pks']:'';
              $judul_pks = (isset($pks['judul_pks']))?$pks['judul_pks']:'';
              
              $master_sumber_pendanaan_id = (isset($master_kontrak['master_sumber_pendanaan_id']))?$master_kontrak['master_sumber_pendanaan_id']:'';
              $this->db->where(array("mk_sumber_pendanaan_id" => $master_sumber_pendanaan_id));
              $q_sumber_pendanaan = $this->db->get("mk_sumber_pendanaan");
              $sumber_pendanaan = $q_sumber_pendanaan->row_array();
              
              $nama_sumber_pendanaan = (isset($sumber_pendanaan['nama']))?$sumber_pendanaan['nama']:'';
              $perusahaan_sumber_pendanaan = (isset($sumber_pendanaan['perusahaan']))?$sumber_pendanaan['perusahaan']:'';
              
              $master_vendor_id = (isset($master_kontrak['master_vendor_id']))?$master_kontrak['master_vendor_id']:'';
              $this->db->where(array("mk_master_vendor_id" => $master_vendor_id));
              $q_vendor = $this->db->get("mk_master_vendor");
              $vendor = $q_vendor->row_array();
              
              $data_pengoperasian = $this->master->get_value("mp_pengoperasian","",array("master_kontrak_id" => $master_kontrak_id,"desa_id" => $desa_id));
              $koperasi_id = (isset($data_pengoperasian['koperasi_id']))?$data_pengoperasian['koperasi_id']:"";
              $data_koperasi = $this->master->get_value("mk_koperasi","",array("mk_koperasi_id" => $koperasi_id));

              $nama_koperasi = (isset($data_koperasi['nama_koperasi']))?$data_koperasi['nama_koperasi']:"NAMA_KOPERASI";
              $nama_pengurus_koperasi = (isset($data_koperasi['nama']))?$data_koperasi['nama']:"NAMA_KOPERASI";
              $tanggal_operasi = (isset($data_koperasi['tanggal_operasi']))?$data_koperasi['tanggal_operasi']:"TANGGAL-OPERASI";
              
              $nama_vendor = (isset($vendor['nama']))?$vendor['nama']:'';
              $perusahaan_vendor = (isset($vendor['perusahaan']))?$vendor['perusahaan']:'';
              
              $tanggal = $this->data->human_date(date("Y-m-d"));
              $tanggal_hari = $this->data->human_date(date("Y-m-d"),false,true);
              $tanggal_operasi = $this->data->human_date($tanggal_operasi,false,true);
              
              $berita_acara = '
              <p style="text-align: center;">
                <strong>Berita Acara Uji Terima Penyerahan Barang dan Jasa :</strong>
                <br /><strong>PKS No. '.$pks_no.' Tanggal '.$tanggal.'</strong>
                <br /><strong>Pengadaan : '.$judul_pks.'</strong>
                <br /><strong>Nomor : '.$nomor_kontrak.'</strong>
              </p>
              <p style="text-align: left;">
                <br />Pada hari ini '.$tanggal_hari.' Yang dilakukan oleh para pihak :
                <br />1 <strong>'.$nama_vendor.', '.$perusahaan_vendor.'</strong>
                <br />2 <strong>'.$nama_pengurus_koperasi.', '.$nama_koperasi.'</strong>
                <br />Telah dilakukan uji terima dan Penyerahan Barang dan Jasa <strong>'.$judul_pks.'</strong>. Dengan rincian sebagaimana tertuang dalam Lampiran : Daftar Penyerahan barang dan Jasa.
                <br />Para pihak sepakat menerima sepenuhnya sesuai dengan syarat dan ketentuan kontrak (dengan catatan - bila ada) ...................................................................................
                <br />Terhitung sejak tanggal '.$tanggal_operasi.' peralatan tersebut selanjutnya diserahkan pengoperasiannya kepada Koperasi '.$nama_koperasi.'
              </p>
              ';
            }
            echo $berita_acara;
        ?>
        </div>
      </div>
    </div>
  </div>
<?php
  if(!empty($is_modal)){
?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>

<?php
}
?>