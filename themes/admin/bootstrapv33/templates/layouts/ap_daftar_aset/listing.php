<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <!-- <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li> -->
                <!--<li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>-->
                <!-- <li class="hide"><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li> -->
                <!-- <li><a href="#import<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-import"></span></a></li> -->
                <!--<li><a href="#export<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon glyphicon-export"></span></a></li>-->
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      #echo "<pre>";
                      $data_listing = $this->data->create_listing($this->init);
                      #echo "</pre>";
                    ?>
                    <table class="table table-striped table-bordered table-hover" width="100%">
                      <thead>
                        <tr>
                          <th class="no-print bulk_data_all_th" align="center" valign="middle" width="5"><input name="bulk_data_all" value="1" class="bulk_data_all" type="checkbox"></th><th>No</th>
                          <th>Berita Acara Uji Terima</th>
                          <th>Berita Acara Serah Terima</th>
                          <th>Desa/Kelurahan</th>
                          <th>Akun</th>
                          <th>Aset</th>
                          <th>Vendor</th>
                          <th>Volume</th>
                          <th>Harga Perolehan</th>
                          <!-- <th>PPN</th> -->
                          <th>Total</th>
                          <th>Beban Anggaran</th>
                          <th>Penanggung Beban Anggaran</th>
                          <th class="action_menu_col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $data_rows = $this->data->data_rows;
                        $no = 1;
                        $grand_total = 0;
                        $kode_mata_uang = "Rp. ";
                        $filters = $this->session->userdata("mp_ap_daftar_aset_filter");
                        $curr_master_kontrak_id = (isset($filters['master_kontrak_id']))?$filters['master_kontrak_id']:"";
                        $desa_id = (isset($filters['desa_id']))?$filters['desa_id']:"";
                        $harga_perolehan = 0;
                        if(is_array($data_rows) AND COUNT($data_rows) > 0)
                        {
                          foreach($data_rows as $i => $r)
                          {
                            $total = $r['harga_satuan'] * $r['volume'];
                            $harga_perolehan += $r['harga_satuan'];
                            $grand_total += $total;
                            $vendor = $this->master->get_value("mk_master_vendor","",array("mk_master_vendor_id" => $r['master_vendor_id']));
                            $vendor_perusahaan = (isset($vendor['perusahaan']))?$vendor['perusahaan']:"";
                            $vendor_kode_vendor = (isset($vendor['kode_vendor']))?$vendor['kode_vendor']:"";
                            $vendor_alamat = (isset($vendor['alamat']))?$vendor['alamat']:"";
                            
                            $master_kontrak = $this->master->get_value("mk_master_kontrak","",array("mk_master_kontrak_id" => $r['master_kontrak_id']));
                            $master_kontrak_perusahaan = (isset($master_kontrak['perusahaan']))?$master_kontrak['perusahaan']:"";
                            $master_kontrak_kode_master_kontrak = (isset($master_kontrak['kode_master_kontrak']))?$master_kontrak['kode_master_kontrak']:"";
                            $master_kontrak_alamat = (isset($master_kontrak['alamat']))?$master_kontrak['alamat']:"";
                            $pks_id = (isset($master_kontrak['pks_id']))?$master_kontrak['pks_id']:"";
                            
                            $sumber_pendanaan_id = (isset($master_kontrak['master_sumber_pendanaan_id']))?$master_kontrak['master_sumber_pendanaan_id']:"";
                            $sumber_pendanaan = $this->master->get_value("mk_sumber_pendanaan","",array("mk_sumber_pendanaan_id" => $sumber_pendanaan_id));
                            $sumber_pendanaan_instansi = (isset($sumber_pendanaan['instansi']))?$sumber_pendanaan['instansi']:"";
                            $sumber_pendanaan_kode_sumber_pendanaan = (isset($sumber_pendanaan['kode_sumber_pendanaan']))?$sumber_pendanaan['kode_sumber_pendanaan']:"";
                            $sumber_pendanaan_alamat = (isset($sumber_pendanaan['alamat_instansi']))?$sumber_pendanaan['alamat_instansi']:"";
                            #$kode_mata_uang = ($r['mata_uang'] == "IDR")?"Rp. ":"$";
                            
                            $asset_berita_acara = $this->master->get_value("mp_asset_berita_acara","",array("master_kontrak_id" => $r['master_kontrak_id'],"desa_id" => $r['desa_id']));
                            $inspeksi_serah_terima = $this->master->get_value("mp_inspeksi_berita_acara","",array("master_kontrak_id" => $r['master_kontrak_id'],"desa_id" => $r['desa_id']));
                            
                            
                      ?>
                        <tr>
                          <td class="no-print" align="center" valign="middle"><input name="bulk_data[]" value="<?php echo $r['mp_asset_biaya_id'];?>" class="bulk_data" type="checkbox"></td>
                          <td align="center"><?php echo $no; $no++;?></td>
                          <td style="white-space: nowrap;text-align: center;"><?php if(isset($asset_berita_acara['mp_asset_berita_acara_id']) and !empty($asset_berita_acara['mp_asset_berita_acara_id'])){?><a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/ap_daftar_aset/berita_acara/<?php echo $asset_berita_acara['mp_asset_berita_acara_id'];?>" class="ajax is_modal" data-target="#popupoverlay" data-target-ajax="#data_target-modal">Lihat Berita Acara</a><?php } else{ ?>Belum Uji Terima <?php } ?></td>
                          <td style="white-space: nowrap;text-align: center;"><?php if(isset($inspeksi_serah_terima['mp_inspeksi_berita_acara_id']) and !empty($inspeksi_serah_terima['mp_inspeksi_berita_acara_id'])){?><a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/ap_daftar_aset/serah_terima/<?php echo $inspeksi_serah_terima['mp_inspeksi_berita_acara_id'];?>" class="ajax is_modal" data-target="#popupoverlay" data-target-ajax="#data_target-modal">Lihat Berita Acara</a><?php } else{ ?>Belum Serah Terima <?php } ?></td>
                          <td style="white-space: nowrap;"><?php echo $this->data->get_value("data_desa","nama_desa label",array("data_desa_id" => $r['desa_id']));?></td>
                          <td style="white-space: nowrap;text-align: center;"><?php echo $this->data->get_value("ap_bagan_akun","concat(akun/*,' ',nama_akun*/) label",array("pks_id" => $pks_id));?></td>
                          <td style="white-space: nowrap;"><?php echo $r['produk'];?></td>
                          <td style="white-space: nowrap;text-align: center;"><?php echo $vendor_kode_vendor . ' - ' . $vendor_perusahaan.', '.$vendor_alamat;?></td>
                          <td style="white-space: nowrap;text-align:right;"><?php echo $r['volume'];?> <?php echo $r['satuan'];?></td>
                          <td style="white-space: nowrap;text-align:right;"><?php echo $kode_mata_uang . number_format((float) $r['harga_satuan'],2,",",".");?></td>
                          <td style="white-space: nowrap;text-align:right;"><?php echo $kode_mata_uang . number_format((float) $total,2,",",".");?></td>
                          <td style="white-space: nowrap;text-align:right;"><?php echo $kode_mata_uang . number_format((float) $total,2,",",".");?></td>
                          <td style="white-space: nowrap;text-align: center;"><?php echo $sumber_pendanaan_kode_sumber_pendanaan . ' - ' . $sumber_pendanaan_instansi.', '.$sumber_pendanaan_alamat;?></td>
                          <td class="rows_action">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm">Choose Action</button>
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="">
                                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/edit/<?php echo $r['mp_asset_biaya_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Edit</a>
                                </li>
                                <li role="presentation" class="">
                                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/view/<?php echo $r['mp_asset_biaya_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> View</a>
                                </li>
                                <li role="presentation" class="">
                                  <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>admin/mp_berita_acara/delete/<?php echo $r['mp_asset_biaya_id'];?>" class="glyphicon-share icon glyphicon ajax" data-target-ajax=".ajax_container"> Delete</a>
                                </li>
                              </ul>
                            </div>
                          </td>
                        </tr>
                      <?php
                          }
                        }
                      ?>
                        <tr>
                          <td style="white-space: nowrap;text-align:right;" class="visible-print" colspan="8"><strong>JUMLAH</strong></td>
                          <td style="white-space: nowrap;text-align:right;" class="hidden-print" colspan="9"><strong>JUMLAH</strong></td>
                          <td style="white-space: nowrap;text-align:right;"><strong><?php echo $kode_mata_uang . number_format((float) $harga_perolehan,2,",",".");?></strong></td>
                          <td style="white-space: nowrap;text-align:right;"><strong><?php echo $kode_mata_uang . number_format((float) $grand_total,2,",",".");?></strong></td>
                          <td style="white-space: nowrap;text-align:right;"><strong><?php echo $kode_mata_uang . number_format((float) $grand_total,2,",",".");?></strong></td>
                          <td style="white-space: nowrap;text-align:right;" colspan="2">&nbsp;</td>
                          <td style="white-space: nowrap;text-align:right;" class="hidden-print">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
                      ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                   
                  <?php
                    #$init = $this->init;
                    #echo $this->data->create_form_filter($init);
                  ?> 
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">
                  <?php
                    echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                  ?>
                </div>
                <div class="tab-pane" id="import<?php echo $is_modal;?>">
                  <div class="row">
                    <div class="col-lg-12">
						<h3>Import Data</h3>
						<p>Silahkan download format file <a href="<?php echo $this->importer->link_format_file();?>">disini</a>, isi file sesuai kolom yang telah disediakan. Kemudian upload kembali menggunakan form di bawah ini.</p>
						<?php
						#$init = $this->init;
						#echo $this->data->create_form_import($init);
						echo $this->importer->upload_file_form();
						?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="export<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_export($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

