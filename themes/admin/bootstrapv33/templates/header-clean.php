<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo current_admin_theme_url();?>dist/ico/favicon.ico">

    <title><?php echo WEB_NAME;?></title>
    <?php echo $this->assets->print_css("head");?>
    <?php echo $this->assets->print_js("head");?>
    <style type="text/css">
      <?php echo $this->assets->print_css_inline("head");?>
    </style>
  </head>

  <body role="document" class="skin-blue fixed">
<?php } ?>
