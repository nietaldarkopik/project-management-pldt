<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
  <div class="widget_certificates list-group text-center certificate-status <?php echo ($this->gts_greenwallet->is_registered() == "")?'':'active';?>">
    <img title="Certified Trees" alt="Certified Trees" src="<?php echo current_theme_url();?>assets/images/icon-certificate.png" />
    <h4>Certificates</h4>
    <?php
    $certificates = $this->gts_certificates->get_my_certificates();
    if(is_array($certificates) and count($certificates) > 0)
    {
      $no = 0;
      foreach($certificates as $i => $certificate)
      {
        $no++;
        $is_sale = $this->gts_certificates->is_sale($certificate['certificate_id']);
    ?>
    <a href="<?php echo base_url('certificates/detail_certificate/'.$certificate['certificate_id']);?>" class="list-group-item"><span class="square-badge"><?php echo $no;?></span> <?php echo $certificate['nomor_sertifikat'];?></a>
    <?php
          if($no >= 5)
            break;
        }
      }
    ?>
    <a href="<?php echo base_url('certificates/sell');?>" class="list-group-item"><span class="square-badge"><i class="glyphicon glyphicon-th-list"></i></span> View All Certificate</a>
    
    <!--p>Unavailable</p-->
  </div><!-- .panel-user -->
