<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>						
  <div class="widget_greenwallet list-group text-center green-wallet-status <?php echo ($this->gts_greenwallet->is_registered() == "")?'':'active';?>">
    <img class="" title="Green Wallet" alt="Green Wallet" src="<?php echo current_theme_url();?>assets/images/icon-wallet.png" />
    <h4>Green Wallet</h4>
    <p>Saldo Aktif :<br/><?php echo $this->gts_greenwallet->get_tmp_current_amount_gw();?> IDR</p>
    <p>Saldo Pasif :<br/><?php echo $this->gts_greenwallet->get_current_amount_bidded();?> IDR</p>
    <p>Total Saldo :<br/><?php echo $this->gts_greenwallet->get_current_amount_gw();?> IDR</p>
    <!--p>Unavailable</p-->
  </div><!-- .panel-user -->
